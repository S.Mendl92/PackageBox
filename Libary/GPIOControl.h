/**
 * @file   GPIOControl.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse GPIOControl
 *
 * @section Definierte Methoden:
 * @li GPIOControl()
 * @li ~GPIOControl()
 * @li bool ControlDoor(const bool cbState,const bool cbWriteSystemLog)
 * @li bool ControlDoor(const unsigned int cuiTimeInSeconds,const bool cbWriteSystemLog)
 * @li bool ControlErrorLED(const bool cbState,const bool cbWriteSystemLog)
 * @li bool WaitTime(const unsigned int cuiTimeInSeconds)
 * @li void ContolAktorsByWebsite(const bool cbLockThread1,const bool cbLockThread2)
 * @li void SendStateToGPIOPort()
 * @li void WriteStateInFile(const string cstWholeTextfilePath,const bool cbActiveStatePackageBox)
 * @li bool get_DoorState() const
 * @li bool get_ErrorLEDState() const
 * @li bool get_ScannerButtonState()
 * @li bool get_ErrorButtonState()
 * @li void initialize()
 * @li void IncreaseClassCounter()
 * @li void SetInternalCreationalNumber()
 * @li bool ReadScannerButton()
 * @li bool ReadErrorButton()
 * @li string ConvertAktorState(const bool cbState) const
 * @li string ConvertSensorState(const bool cbState) const
 * @li string ConvertActiveState(const bool cbState) const
 * @li void GPIOPortSetup()
 * @li bool GetStateFromGpioPort(const unsigned int cuiPortNumber
 * @li void SetStateOfGpioPort(const unsigned int cuiPortNumber, const bool cbValue)
 */

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <thread>
#include <chrono>
#include <cassert>

#include "../Libary/Systemlog.h"
#include "../Libary/DatabaseWrapper.h"
#include "../Libary/Converter.h"

#ifndef GPIOCONTROL_H_INCLUDED
#define GPIOCONTROL_H_INCLUDED

/**
* @class GPIOControl
* @brief Steuert den GPIOPort des Rasberry Pi an.
*
* @section Aufgaben
* @li Ansteuern der Tür
* @li Anstuern der Fehler Led
* @li Modul zur dynamischen Pausenerzeugung
* @li Handelt die Verwaltung zur AKtoransteuerung von der Website
* @li Aktuellen Status aller Akoren und Sensoren abrufen
* @li Besitzt ein Modul, um eine Wartezeit zu erzeugen
* @li Name, Odnerpfad und komplette Adressierung aller verwendeten (externen) Datei
*     sollen aufrufbar sein
*/
class GPIOControl : protected DatabaseWrapper, protected Converter
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/

    GPIOControl();
    ~GPIOControl();

    bool ControlDoor(const bool cbState,const bool cbWriteSystemLog);
    bool ControlDoor(const unsigned int cuiTimeInSeconds,const bool cbWriteSystemLog);
    bool ControlErrorLED(const bool cbState,const bool cbWriteSystemLog);
    bool WaitTime(unsigned int uiTimeInSeconds);

    void ContolAktorsByWebsite(const bool cbLockThread1,const bool cbLockThread2);
    void WriteStateInFile(const std::string cstWholeTextfilePath,const bool cbActiveStatePackageBox);

    /****************************************************************
    *                      Get-Methoden                             *
    *****************************************************************/

    bool get_DoorState() const;
    bool get_ErrorLEDState() const;
    bool get_ScannerButtonState();
    bool get_ErrorButtonState();

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_GPIOControl;

private:
    /*****************************************************************
    *                    Private Methoden                           *
    *****************************************************************/

    void initialize();
    void GPIOPortSetup();
    void IncreaseClassCounter();
    void SetInternalCreationalNumber();
    bool ReadScannerButton();
    bool ReadErrorButton();

    bool GetStateFromGpioPort(const unsigned int cuiPortNumber);
    void SetStateOfGpioPort(const unsigned int cuiPortNumber, const bool cbValue);

    std::string ConvertAktorState(const bool cbWriteSystemLog) const;
    std::string ConvertSensorState(const bool cbWriteSystemLog) const;
    std::string ConvertActiveState(const bool cbWriteSystemLog) const;

    /*****************************************************************
    *                  Private Klassenvariablen                     *
    *****************************************************************/

    static bool _bDoorState;                             //! Aktueller Steuerwert der Tür
    static bool _bErrorLEDState;                         //! Aktueller Steuerwert der Fehler Led
    static bool _bScannerButtonState;                    //! Aktueller Stuerwert des Scanner Tasters
    static bool _bErrorButtonState;                      //! Aktueller Steuewert des Fehler Tasters
    static bool _bLastControlStateErrorLed;              //! Letzter Steuerwert der Fehler Led
    static bool _bLastControlStateDoor;                  //! Letzter Steuerwert der Tür
    static unsigned long long int _ulliCreationalNumber; //! Anzahl der erzeugten Instanzen


    const std::string _cstFolderGpioPort = "/sys/class/gpio/";                                                  //! Ordner zu virtuelle GPIO Abbildern
    const std::string _cstValueFile = "value";                                                                  //! virtueller File Name

    const unsigned int _cuiNumberScannerButtonState = DatabaseWrapper::INTERFACE::SCANNERBUTTONSTATE;            //! Datensatznummer simulierter Scanner Taster
    const unsigned int _cuiNumberErrorButtonState = DatabaseWrapper::INTERFACE::ERRORBUTTONSTATE;                //! Datensatzunmmer simulierter Feher Taster
    const unsigned int _cuiNumberDoorControlByWebsite = DatabaseWrapper::INTERFACE::HANDLEDOORBYWEBSITE;         //!Datensatznummer Türsteuerung per Website
    const unsigned int _cuiNumberControlErrorLedByWebsite = DatabaseWrapper::INTERFACE::HANDLEERRORLEDBYWEBSITE; //! Datensatznummer Fehler LED Steuerung per Website

    unsigned long long int _ulliNumberOfInstances;       //! Nummer der erzeugten Instanz
    bool _bTextfileIsLocked;                             //! Siganlisiert, dass das Textfile zum schreiben des Systemstatus gesperrt ist

    /*****************************************************************
    *                  Private Enum                                  *
    *****************************************************************/

    enum GPIOPORT
    {
        GREENLEDPORT = 16,
        REDLEDPORT = 20,
        SCANNERBUTTONPORT = 17,
        ERRORBUTTONPORT = 4
    };

    Systemlog Logger;
};
#endif // GPIOCONTROL_H_INCLUDED
