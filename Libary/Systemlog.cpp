/**
 * @file   Systemlog.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Dezember, 2016
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für Systemlog)
 *
 * @section Implementierte Methoden:
 * @li Systemlog()
 * @li ~Systemlog()
 * @li bool WriteSystemLogInDB(const unsigned int cuiErrorclass,const string cstLogComment)
 * @li string get_DatabaseName() const
 * @li string get_DatabasePath() const
 * @li string get_WholeDatabasePath() const
 * @li void initialize()
 * @li string SelectErrorClassCode(const unsigned int cuiErrorClassCode)
 */

#include "../Libary/Systemlog.h"

/**
* @brief Konstruktoraufruf
*/
Systemlog::Systemlog()
{
    initialize();
}

/**
* @brief Destruktoraufruf
*/
Systemlog::~Systemlog()
{

}

/**
* @brief Schreibt ein Kommentar mit zugehöriger Fehlerklasse in den Systemlog
* @param[in] cuiErrorClass:  Fehlercodeauswahl
*                           0 = Emergency
*                           1 = Alert
*                           2 = Critical
*                           3 = Error
*                           4 = Warning
*                           5 = Notice
*                           6 = Informational
*                           7 = Debug
* @param[in] cstLogComment: Systemlogkommentar
* @return true = Eintrag in Systemlog war erfolgreich
*/
/**
* @test Vorbedingung: Kein Systemlog ist aktiv \n
*       Funktion: Kompletten Wertebereich in DB schreiben \n
*       Nachbedinung: Daten wurden korrekt in DB eingetragen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool Systemlog::WriteSystemLogInDB(const unsigned int cuiErrorClass ,const std::string cstLogComment)
{
    //! Initialisiert die Datenbankabfrage
    sqlite3* SqliteDatabase;
    sqlite3_stmt* Statement;

    //! Beendet die Transaktion, wenn Datenbank nicht geöffnet werden kann
    if (sqlite3_open(_stWholeDatabasePath.c_str(), &SqliteDatabase) != SQLITE_OK)
    {
        assert(false);
        return false;
    }

    //! SQL Befehl
    std::stringstream Query;

    Query <<"INSERT INTO Systemlog ('Wochentag', 'Monat', 'Monatstag', 'Uhrzeit', 'Jahr' , 'Kommentar', 'Fehlerklasse') "
        << "VALUES ("
        << "'" << Time.get_wDay() << "',"
        << "'" << Time.get_month() << "',"
        <<        Time.get_mDay() << ","
        << "'" << Time.get_time() << "',"
        <<        Time.get_Year() << ","
        << "'" << cstLogComment << "',"
        << "'" << SelectErrorClassCode(cuiErrorClass) << "');";

    //! Bereitet die SQL Abfrage vor (c string)
    sqlite3_prepare(SqliteDatabase, Query.str().c_str(),-1, &Statement, nullptr);

    //! Schreibe in Datenbank
    sqlite3_step(Statement);

    //! Schließt die Sitzung ab
    sqlite3_finalize(Statement);
    sqlite3_close(SqliteDatabase);

    return true;
}
/**
* @brief Gibt den Datenbanknamen zurück
* @return Datenbankname
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string Systemlog::get_DatabaseName() const
{
    return _cstDatabaseName;
}

/**
* @brief Gibt den Speicherpfad der Datenbank zurück
* @return Speicherpfad der Datenbank
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string Systemlog::get_DatabasePath() const
{
    return _cstDatabasePath;
}

/**
* @brief Gibt den kompletten Pfad der Datenbank zurück
* @return kompletter Pfad der Datenbank
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string Systemlog::get_WholeDatabasePath() const
{
    return _stWholeDatabasePath;
}

/**
* @brief Initialisiert alle Klassenvariablen
* @return Kein Rückgabewert
*/
/**
* @test Kein Test erforderlich
*/
void Systemlog::initialize()
{
    //! Legt den Speicherpfade fest
    _stWholeDatabasePath = _cstDatabasePath + _cstDatabaseName;
}

/**
* @brief Wählt den zugehörigen Fehlerklassencode aus
* @param[in] uiValue: Fehlercodeauswahl
*                           0 = Emergency
*                           1 = Alert
*                           2 = Critical
*                           3 = Error
*                           4 = Warning
*                           5 = Notice
*                           6 = Informational
*                           7 = Debug
* @return Fehlercode als string
*/
/**
* @test Test wird mit der Methode WriteSystemLogInDB(unsigned int uiErrorClass , string stLogComment) abgedeckt
* @see WriteSystemLogInDB(unsigned int uiErrorClass , string stLogComment)
*/
std::string Systemlog::SelectErrorClassCode(const unsigned int cuiErrorClassCode)
{
    //! Fehlercode
    std::string stErrorCode = "";

    //! Fehlercodeauswahl
    switch (cuiErrorClassCode)
    {
    case Systemlog::COMMENT::EMERGENCY:
        stErrorCode = "Emergency";
        break;
    case Systemlog::COMMENT::ALERT:
        stErrorCode = "Alert";
        break;
    case Systemlog::COMMENT::CRITICAL:
        stErrorCode = "Critical";
        break;
    case Systemlog::COMMENT::ERROR:
        stErrorCode = "Error";
        break;
    case Systemlog::COMMENT::WARNING:
        stErrorCode = "Warning";
        break;
    case Systemlog::COMMENT::NOTICE:
        stErrorCode = "Notice";
        break;
    case Systemlog::COMMENT::INFORMATIONAL:
        stErrorCode = "Informational";
        break;
    case Systemlog::COMMENT::DEBUG:
        stErrorCode = "Debug";
        break;
    default:
        assert(false);
        stErrorCode = "---";
        break;
    }

    //! Gibt den Fehlercode zurück
    return stErrorCode;
}
