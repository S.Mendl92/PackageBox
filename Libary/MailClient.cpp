/**
 * @file   MailClient.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für MailClient)
 *
 * @section Implementierte Methoden:
 * @li MailClient()
 * @li ~MailClient()
 * @li bool SendMailOrderdPackageReceived(const Package PackageInformation, const Receiver ReceiverInformation)
 * @li bool SendMailWrongPackageReceived(const string cstBarcode)
 * @li bool SendMailPackageReceivedWithWrongSize(const Package PackageInformation, const Receiver ReceiverInformation)
 * @li bool SendMailActionDuringFalseWorkingState()
 * @li unsigned int get_ValueOfDailyMail() const
 * @li void initialize()
 * @li void PrepareMailTextForTerminal(string& stMailText)
 * @li void SendMail(const string cstPreparedMailText,const string cstReveierMailAdress)
 * @li void IncreaseDailyMailCounter()
 * @li void BufferInformationFromSettingsDatabase()
 * @li bool DailyMailCounter()
 */

#include "../Libary/MailClient.h"

//!< Initialisiert die static Klassenvariablen der Klasse MailClient
bool MailClient::_bSendMaxValueOfMailsReached = false;
unsigned int MailClient::MailClient::_uiDailyMails = 0;

/**
* @brief Konstruktoraufruf der Klasse Mailclient
*/
MailClient::MailClient()
{
    //! Initialisiert alle Klassenvariablen
    initialize();
}

/**
* @brief Destruktoraufruf der Klasse Mailclient
*/
MailClient::~MailClient()
{

}

/**
* @brief Sendend E-Mails an bis zu 5 Empfänger, das ein Paket ordnungsgemäß hinterlegt wurde
* @param[in] PackageInformation: Beinhaltet Informationen zum Paket
* @param[in] ReceiverInformation: Beinhaltet Empfänger Informationen
* @return true = E-Mail wurde erfolgreich versendet
*/
/**
* @test Vorbedingung: Es wurden noch keine 500 Mails am Testtag versendet  \n
*       Funktion: Methode mit unterschiedlichen Empfängern aufrufen \n
*       Nachbedinung: E-Mails müssen richtig ankommen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool MailClient::SendMailOrderdPackageReceived(const Package PackageInformation, const Receiver ReceiverInformation)
{
    //! Lädt die aktuellen Empfänger E-Mail Adressen
    BufferInformationFromSettingsDatabase();

    //! Überpüft, ob die maximale Anzahl an E-Mails schon versendet wurde und bricht ggf. den Vorgang ab
    if (!DailyMailCounter())
    {
        return false;
    }

    std::stringstream ssBodyText; //! Beinhaltet den Hauptteil der E-Mail

    ssBodyText  << "Der Lieferant " << PackageInformation.stDeliverer << " hat das Paket mit dem Barcode " << PackageInformation.stBarcode << std::endl
                << "um " << Time.get_time() << " Uhr geliefert." << std::endl << std::endl
                << "Das Paket ist addressiert an: " << ReceiverInformation.stName << " " << ReceiverInformation.stSureName << std::endl
                << "Im Paket befindet sich: " << PackageInformation.stContent << std::endl << std::endl
                << "Das Paket liegt nun abholbereit in der Paketbox." ;

    //! E-Mails werden an alle Empfänger versendet (max 5)
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {

        //!Bricht den Schleifendurchgang ab, falls kein Empfänger übermittelt wird
        if (_mapReceivers[uiLoop] == "")
        {
            continue;
        }

        //! Aktueller Empfänger wird hinzugefügt
        std::string stReceiver = _mapReceivers[uiLoop];

        //! Beinhaltet den Empfänger und Betreff
        std::stringstream ssHeadText;

        ssHeadText  << "To: " << stReceiver << std::endl
                    << "From: Paketbox<paketbox.dhbw@gmail.com>" << std::endl
                    << "Subject: Das Paket mit dem Barcode: " << PackageInformation.stBarcode << " wurde gerade geliefert" << std::endl << std::endl;

        //! Konvertiert die stringstreams in einen String
        std::string stBodyText = ssBodyText.str();

        //! Ersetzt alle Umlaute in dem String
        DeleteUmlauteToRegularLetters(stBodyText);
        DetecteInjectionSigns(stBodyText);

        std::string stMailText = ssHeadText.str() + stBodyText + _ssTailText.str();

        //! E-Mail wird für die Kommandozeile vorbereitet
        PrepareMailTextForTerminal(stMailText);

        //! E-Mail wird an Empfänger versendet
        SendMail(stMailText, stReceiver);

        //! Erhöht die Anzahl der versendetetn Mails um 1
        IncreaseDailyMailCounter();
    }

    WriteDailyMailsInSystemlog();
    return true;
}

/**
* @brief Sendend E-Mails an bis zu 5 Empfänger, das ein falsches Paket eingescannt wurde
* @param[in] stBarcode: Der Barcode des Paketes
* @return true = E-Mail wurde erfolgreich versendet
*/
/**
* @test Vorbedingung: Es wurden noch keine 500 Mails am Testtag versendet  \n
*       Funktion: Methode mit unterschiedlichen Empfängern aufrufen \n
*       Nachbedinung: E-Mail müssen richtig ankommen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool MailClient::SendMailWrongPackageReceived(const std::string cstBarcode)
{
    //! Lädt die aktuellen Empfänger E-Mail Adressen
    BufferInformationFromSettingsDatabase();

    //! Überpüft, ob die maximale Anzahl an E-Mails schon versendet wurde und bricht ggf. den Vorgang ab
    if (!DailyMailCounter())
    {
        return false;
    }

    std::stringstream ssBodyText; //! Beinhaltet den Haupteil der E-Mail

    ssBodyText  << "Der Lieferant hat ein Paket um" << std::endl
                << Time.get_time() << " Uhr eingescannt." << std::endl << std::endl
                << "Das Paket befindet sich nicht in der Datenbank der Paketbox." ;

    //! E-Mails werden an alle Empfänger versendet (max 5)
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        //!Bricht den Schleifendurchgang ab, falls kein Empfänger übermittelt wird
        if (_mapReceivers[uiLoop] == "")
        {
            continue;
        }

        //! Aktueller Empfänger wird hinzugefügt
        const std::string cstReceiver = _mapReceivers[uiLoop];

        //! Beinhaltet den Empfänger und Betreff
        std::stringstream ssHeadText;

        ssHeadText  << "To: " << cstReceiver << std::endl
                    << "From: Paketbox<paketbox.dhbw@gmail.com>" << std::endl
                    << "Subject: Das Paket mit dem Barcode: " << cstBarcode << " wurde gerade eingescannt, befindet sich"
                    << " aber nicht in der Datenbank" << std::endl << std::endl;

        //! Konvertiert die stringstreams in einen String
        std::string stBodyText = ssBodyText.str();

        //! Ersetzt alle Umlaute in dem String
        DeleteUmlauteToRegularLetters(stBodyText);
        DetecteInjectionSigns(stBodyText);

        //! Konvertiert die stringstreams in einen String
        std::string stMailText = ssHeadText.str() + stBodyText + _ssTailText.str();

        //! E-Mail wird für die Kommandozeile vorbereitet
        PrepareMailTextForTerminal(stMailText);

        //! E-Mail wird an Empfänger versendet
        SendMail(stMailText, cstReceiver);

        //! Erhöht die Anzahl der versendeten Mails um 1
        IncreaseDailyMailCounter();
    }

    WriteDailyMailsInSystemlog();
    return true;
}

/**
* @brief Sendend E-Mails an bis zu 5 Empfänger, dass ein Paket mit den falschen Abmaßen eingescannt wurde
* @param[in] PackageInformation: Beinhaltet Informationen zum Paket
* @param[in] ReceiverInformation: Beinhaltet Empfänger Informationen
* @return true = E-Mail wurde erfolgreich versendet
*/
/**
* @test Vorbedingung: Es wurden noch keine 500 Mails am Testtag versendet  \n
*       Funktion: Methode mit unterschiedlichen Empfängern aufrufen \n
*       Nachbedinung: E-Mail müssen richtig ankommen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool MailClient::SendMailPackageReceivedWithWrongSize(const Package PackageInformation,const Receiver ReceiverInformation)
{
    //! Lädt die aktuellen Empfänger E-Mail Adressen
    BufferInformationFromSettingsDatabase();

    //! Überpüft, ob die maximale Anzahl an E-Mails schon versendet wurde und bricht ggf. den Vorgang ab
    if (!DailyMailCounter())
    {
        return false;
    }

    std::stringstream ssBodyText; //! Beinhaltet den Hauptteil der E-Mail

    ssBodyText  << "Der Lieferant " << PackageInformation.stDeliverer << " hat das Paket mit dem Barcode " << PackageInformation.stBarcode << std::endl
                << "um " << Time.get_time() << " Uhr eingescannt." << std::endl << std::endl
                << "Das Paket ist addressiert an: " << ReceiverInformation.stName << " " << ReceiverInformation.stSureName << std::endl
                << "Im Paket befindet sich: " << PackageInformation.stContent << std::endl
                << "Das Paket hatte die falschen Abmessungen und konnte nicht" << std::endl
                << "in der Paketbox hinterlegt werden" ;

    //! E-Mails werden an alle Empfänger versendet (max 5)
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        //!Bricht den Schleifendurchgang ab, falls kein Empfänger übermittelt wird
        if (_mapReceivers[uiLoop] == "")
        {
            continue;
        }

        //! Aktueller Empfänger wird hinzugefügt
        const std::string cstReceiver = _mapReceivers[uiLoop];

        //! Beinhaltet den Empfänger und Betreff
        std::stringstream ssHeadText;

        ssHeadText  << "To: " << cstReceiver << std::endl
                    << "From: Paketbox<paketbox.dhbw@gmail.com>" << std::endl
                    << "Subject: Das Paket mit dem Barcode: " << PackageInformation.stBarcode << " wurde gerade eingescannt und konnte nicht "
                    << "hinterlegt werden" << std::endl << std::endl;

        //! Konvertiert die stringstreams in einen String
        std::string stBodyText = ssBodyText.str();

        //! Ersetzt alle Umlaute in dem String
        DeleteUmlauteToRegularLetters(stBodyText);
        DetecteInjectionSigns(stBodyText);

        //! Konvertiert die stringstreams in einen String
        std::string stMailText = ssHeadText.str() + stBodyText + _ssTailText.str();

        //! E-Mail wird für die Kommandozeile vorbereitet
        PrepareMailTextForTerminal(stMailText);

        //! E-Mail wird an Empfänger versendet
        SendMail(stMailText, cstReceiver);

        //! Erhöht die Anzahl der versendeten Mails um 1
        IncreaseDailyMailCounter();
    }

    WriteDailyMailsInSystemlog();
    return true;
}

/**
* @brief Sendend E-Mails an bis zu 5 Empfänger, dass ein Paketaktion stattfand, während die Paketbox inaktiv ist
* @return true = E-Mail wurde erfolgreich versendet
*/
/**
* @test Vorbedingung: Es wurden noch keine 500 Mails am Testtag versendet  \n
*       Funktion: Methode mit unterschiedlichen Empfängern aufrufen \n
*       Nachbedinung: E-Mail müssen richtig ankommen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool MailClient::SendMailActionDuringFalseWorkingState()
{
    //! Lädt die aktuellen Empfänger E-Mail Adressen
    BufferInformationFromSettingsDatabase();

    //! Überpüft, ob die maximale Anzahl an E-Mails schon versendet wurde und bricht ggf. den Vorgang ab
    if (!DailyMailCounter())
    {
        return false;
    }

    std::stringstream ssBodyText; //! Beinhaltet den Hauptteil der E-Mail

    ssBodyText  << "Der Paketlieferant hat um " << Time.get_time() << " versucht ein Paket" << std::endl
                << "einzuscannen. Zu diesem Zeitpunkt war die Paketbox aber inaktiv." << std::endl
                << "Das Paket konnte somit leider nicht entgegengenommen werden.";

    //! E-Mails werden an alle Empfänger versendet (max 5)
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        //!Bricht den Schleifendurchgang ab, falls kein Empfänger übermittelt wird
        if (_mapReceivers[uiLoop] == "")
        {
            continue;
        }

        //! Aktueller Empfänger wird hinzugefügt
        std::string stReceiver = _mapReceivers[uiLoop];

        //! Beinhaltet den Empfänger und Betreff
        std::stringstream ssHeadText;

        ssHeadText  << "To: " << stReceiver << std::endl
                    << "From: Paketbox<paketbox.dhbw@gmail.com>" << std::endl
                    << "Subject: Es wurde versucht ein Paket einzuscannen, waehrend die Paketbox inatkiv war" << std::endl << std::endl;

        //! Konvertiert die stringstreams in einen String
        std::string stBodyText = ssBodyText.str();

        //! Ersetzt alle Umlaute in dem String
        DeleteUmlauteToRegularLetters(stBodyText);
        DetecteInjectionSigns(stBodyText);


        //! Konvertiert die stringstreams in einen String
        std::string stMailText = ssHeadText.str() + stBodyText + _ssTailText.str();

        //! E-Mail wird für die Kommandozeile vorbereitet
        PrepareMailTextForTerminal(stMailText);

        //! E-Mail wird an Empfänger versendet
        SendMail(stMailText, stReceiver);

        //! Erhöht die Anzahl der versendeten Mails um 1
        IncreaseDailyMailCounter();
    }

    WriteDailyMailsInSystemlog();
    return true;
}

/**
* @brief Initialisiert alle Variablen der Klasse MailClient
* @return Kein Rückgabewert
*/
/**
* @test Kein Test notwendig
*/
void MailClient::initialize()
{
    //! Beinhaltet den Schlussteil jeder E-Mail (immer derselbe)
    _ssTailText << std::endl << std::endl
                << "Mit freundlichen Gruessen" << std::endl
                << "Ihr Paktebox Team" << std::endl << std::endl << std::endl << std::endl
                << "Diese E-Mail wurde automatisch generiert. Bitte " << std::endl
                << "Antworten Sie nicht darauf.";

    //! uiCurrentDay beinhaltet den aktuellen Tag
    _uiCurrentDay = Time.get_mDay();

    //! Initialisert alle E-Mail Empfänger mit "" (kein Empfänger)
    for (unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        _mapReceivers[uiLoop] = "";
    }

}

/**
* @brief Bereitet den Mailtext für die Konsole vor
* @param[in,out] stMailText: Kompletter E-Mail Text wird übergeben und nach dem Vorgang überschrieben
* @return Kein Rückgabewert
*/
/**
* @test Test wurde mit dem Versand einer Mail abgeschlossen
*/
void MailClient::PrepareMailTextForTerminal(std::string& stMailText)
{
    //!Variablen wird überschrieben
    stMailText = "echo \"" + stMailText + "\"";
}

/**
* @brief Sendet das Kommando an die Konsole um eine E-Mail zu versenden (wird in Systemlog vermerkt)
* @param[in] cstPreparedMailText: Der prebarierte Mailtext für die Konsole
* @param[in] cstReveierMailAdress: Die E-Mail Empfänger
* @return Kein Rückgabewert
*/
/**
* @test Test wurde mit dem Versand einer Mail abgeschlossen
*/
void MailClient::SendMail(const std::string cstPreparedMailText,const std::string cstReveierMailAdress)
{
    //! Bricht den Sendevorgang ab, falls die letzte E-Mail versendet wurde
    if(_bSendMaxValueOfMailsReached)
    {
        return;
    }

    //! Bereitet einen Text für die Konsole vor
    std::string stExtendendPreparedMailText = cstPreparedMailText + " | ssmtp " + cstReveierMailAdress + " &";
    int iTerminalState = system(stExtendendPreparedMailText.c_str());

    //! Vermerkt das Senden im Systemlog
    std::string stLog = "Empfänger " + cstReveierMailAdress + " wurde Benachrichtigt";
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::NOTICE, stLog.c_str());

    std::ignore = iTerminalState;
}

/**
* @brief Gibt die Anzahl der versendeten E-Mails zurück
* @return Die Anzahl der versendeten Mails (statische Variable)
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
unsigned int MailClient::get_ValueOfDailyMail() const
{
    return _uiDailyMails;
}

/**
* @brief Überprüft ob noch E-Mails versendet werden können (max 485 Stück pro Tag)
* @return true = Es können noch Emails versendet werden
*/
/**
* @test Vorbedingung: Es wurden noch keine 500 Mails am Testtag versendet  \n
*       Funktion: Methode mit unterschiedlichen Empfängern aufrufen \n
*       Nachbedinung: E-Mail müssen richtig ankommen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool MailClient::DailyMailCounter()
{
    //Es können bei Gmail maximal 500 Mails versendet werden
    //! Lädt alle Empfänger
    BufferInformationFromSettingsDatabase();

    //! Der Counter wird um 00:00 Uhr Reset (= nächster Tag)
    if (_uiCurrentDay != Time.get_mDay())
    {
        //!Setzt den neun Tag
        _uiCurrentDay = Time.get_mDay();

        //! Reset die gezählten Mails
        _uiDailyMails = 0;

        //! Setzt den Merker zurück
        _bSendMaxValueOfMailsReached = false;

        return true;
    }

    //! Beendet die Funktion solange der Wert < 485 ist (15 Mails sind Puffer)
    if (_uiDailyMails < 485)
    {
        return true;
    }

    //! Wenn die E-Mails schon gesendet wurden, wird die Funktion beendet
    if (_bSendMaxValueOfMailsReached)
    {
        return false;
    }

    //! Beinhaltet den Text für den Hauptteil der E-Mail
    std::stringstream ssBodyText;

    ssBodyText  << "Es wurde die maximal Anzahl an E-Mails versendet." << std::endl
                << "Fuer heute erhalten sie keine E-Mails mehr. Pakete" << std::endl
                << "werden jedoch noch angenommen.";

    //! Sendet an jeden Empfänger eine Nachricht
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        //! Bricht einen Zyklus der Schleife ab, wenn kein Empfänger deklariert wurde
        if (_mapReceivers[uiLoop] == "")
        {
            continue;
        }

        //! Empfänger wird zugewiesen
        const std::string cstReceiver = _mapReceivers[uiLoop];

        //! Beinhaltet den Empfänger und Betreff der E-Mail
        std::stringstream ssHeadText;

        ssHeadText  << "To: " << cstReceiver << std::endl
                    << "From: Paketbox<paketbox.dhbw@gmail.com>" << std::endl
                    << "Subject: Für heute ist kein E-Mailempfang mehr möglich!" << std::endl << std::endl;

        //! Bildet den Mailtext
        std::string stMailText = ssHeadText.str() + ssBodyText.str() + _ssTailText.str();

        //! Bereitet den Mailtext für die Konsole vor
        PrepareMailTextForTerminal(stMailText);

        //! Versendet die Mail
        SendMail(stMailText, cstReceiver);

        //! Erhöht den Zähler der täglichen E-Mails
        IncreaseDailyMailCounter();
    }

    //! Wird in Systemlog vermerkt
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, "Es werden bis zum nächsten Tag keine E-Mails mehr versendet.");

    //! Wird true, wenn die Status E-Mail versendet wird (so lange diese Variable true ist, können keine Mails mehr versendet werden)
    _bSendMaxValueOfMailsReached = true;

    return false;
}

/**
* @brief Erhöht die Anzahl der versendeten E-Mails um 1
* @return Kein Rückgabewert
*/
/**
* @test Methode wurde mit Mail (<500 Mails/Tag) überprüft
*/
void MailClient::IncreaseDailyMailCounter()
{
    _uiDailyMails += 1;
}

/**
* @brief Holt die aktuellen Datensätze aus der Datenbank (E-Mail Empfänger)
* @return Kein Rückgabewert
*/
/**
* @test Methode wurde mit Mail (<500 Mails/Tag) überprüft
*/
void MailClient::BufferInformationFromSettingsDatabase()
{
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        //! Initialisiert die Struktur der Rückgabewerte
        Settings PreBuffer;

        //! Holt Information aus der Datenbank und speichert diese ab
        ReadInformationFromDatabase(uiLoop,PreBuffer);

        std::string stEmailReceiver =  PreBuffer.stDatasetValue;

        DetecteInjectionSigns(stEmailReceiver);

        _mapReceivers[uiLoop] = stEmailReceiver;
    }
}

/**
* @brief Schreibt die Anzahl der gesendeten Mails in den Systemlog
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Es wurden noch keine Mail am Testtag versendet  \n
*       Funktion: Methode mit unterschiedlicher Empfängeranzahl aufrufen \n
*       Nachbedinung: Anzahl muss korrekt in Systemlog vermerkt werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
 void MailClient::WriteDailyMailsInSystemlog()
 {
     //! Bereitet den string vor und schreibt ihn in die Datenbank
     const std::string cstLog = "Anzahl der versendeten Mails für diesen Tag: " + ConvertUnsingendIntToString(_uiDailyMails) + " / 485";
     Logger.WriteSystemLogInDB(Systemlog::COMMENT::DEBUG, cstLog);
 }

