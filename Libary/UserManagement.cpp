/**
 * @file   Usermanagement.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für Usermanagement
 *                                                              )
 * @section Implementierte Methoden:
 * @li bool CheckUserLoginState()
 * @li bool CheckWorkingState()
 * @li bool HandleTargetSystemByWebsite()
 * @li void initialize()
 * @li string BufferLoginInformationFromSettingsDatabase(const unsigned int cuiDatasetNumber)
 */

#include "../Libary/UserManagement.h"

//!< Initialisert die static Klassenvariablen der Klasse UserManagement
bool UserManagement::_bLastLoginState = false;

/**
* @brief Konstruktoraufruf der Klasse Usermanagement
*/
UserManagement::UserManagement()
{
    initialize();
}

/**
* @brief Destruktoraufruf der Klasse Usermanagement
*/
UserManagement::~UserManagement()
{

}

/**
* @brief Überprüft ob ein Benutzer angemeldet ist oder nicht
* @param[in] mapDatasets: Das geladene XML File
* @return true = User ist angemeldet | false = User ist nicht angemeldet
*/
/**
* @test Vorbedingung: User ist nicht eingeloggt \n
*       Funktion: Loginstatus in der Datenbank ändern \n
*       Nachbedinung: Rückgabewert muss true sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
/**
* @test Vorbedingung: User ist eingeloggt \n
*       Funktion: Loginstatus in der Datenbank ändern \n
*       Nachbedinung: Rückgabewert muss false sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool UserManagement::CheckUserLoginState()
{
    //! Lädt den Datensatz aus der Datenbank und schreibt in Klassenvariable stDatasetValue
    const std::string cstDatasetValue = BufferLoginInformationFromSettingsDatabase(_cuiDatasetNumberLoginState);

    //! Userstatus der Datenbank-Filse: ausgeloggt, aber letzter Zyklus war noch eingeloggt
    if ((cstDatasetValue == "0") and _bLastLoginState)
    {
        //! Bereitet einen string für den Systemlog vor
        std::string stLog = "User hat sich abgemeldet";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

        //! Ändert den Merker auf false
        _bLastLoginState = false;
    }

    //! User war im letzten Zyklus nicht eingeloggt und ist nun eingeloggt
    if ((cstDatasetValue == "1") and !_bLastLoginState)
    {
        //! Bereitet einen String für den Systemlog vor
        std::string stLog = "User ist angemeldet";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

        //! Ändert den Merker
        _bLastLoginState = true;
    }

    //! User ist momentan eingeloggt
    return ((cstDatasetValue == "1") and _bLastLoginState);
}

/**
* @brief Überprüft, ob die Paketbox betriebsbereit ist
* @return true = betriebsbereit | false = nicht betriebsbereit
*/
/**
* @test Vorbedingung: Paketbox ist Betriebsbereit \n
*       Funktion: Parameter verändern, und Betriebsbereitstatus überprüfen \n
*       Nachbedinung: Bereitschaftsstatus muss richtig verarbeitet sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool UserManagement::CheckWorkingState()
{
    //! Initialisiert den Speicher, für die einzelnen Datenbankabfragen
    std::map<int, Settings> mapReceiveTimeSettings;
    bool bReturnState = true;

    //! Läd die Informationen aus der Datenbank
    for(unsigned int uiLoop = _cuiLowerDatasetNumberTimeHandler; uiLoop <= _cuiUpperDatasetNumberTimeHandler; uiLoop += 1)
    {
        //! Legt eine Pufferstruktur an
        Settings ReceiveTimeSettingsBuffer;

        //! Holt Informationen aus der Datenbank und speichert diese in der Map ab
        DatabaseWrapper::ReadInformationFromDatabase(uiLoop,ReceiveTimeSettingsBuffer);
        mapReceiveTimeSettings[uiLoop] = ReceiveTimeSettingsBuffer;
    }

    //! Überprüft ob die Paketbox für den aktuellen Tag freigeschalten ist
    for(unsigned int uiLoop = _cuiLowerDatasetNumberTimeHandler; uiLoop <= _cuiLowerDatasetNumberTimeHandler + 6; uiLoop += 1)
    {
        //! Legt eine Pufferstructur an und beschreibt diese mit dem aktuellen Datensatz
        Settings StateDay;
        StateDay = mapReceiveTimeSettings[uiLoop];

        //! Speichert den aktuellen Wochentag
        std::string stCurrentWDay = Timer.get_wDay();

        //! Vergleicht ob im aktuellen Wochentag eine false hinterlegt ist
        if((StateDay.stDatasetValue == "0") and _mapWDays[uiLoop] == stCurrentWDay)
        {
            return false;
        }
    }

    //! Speichert den Zustand ab, des 24h Bereitschaft ab
    Settings WholeDay;
    WholeDay = mapReceiveTimeSettings[_cuiUpperDatasetNumberTimeHandler];

    //! Nur wenn 24h Bereitschaft Stunden false ist wird die Uhrzeit überprüft
    if(WholeDay.stDatasetValue != "1")
    {
        //! Legt die Strukturen an und beschreibt diese
        Settings StartHour;
        Settings EndHour;
        StartHour = mapReceiveTimeSettings[_cuiLowerDatasetNumberTimeHandler + 7];
        EndHour = mapReceiveTimeSettings[_cuiLowerDatasetNumberTimeHandler + 8];

        //! Konvertiert den String als Integer
        unsigned int uiStartHour = ConvertStringToUnsingendInt(StartHour.stDatasetValue);
        unsigned int uiEndHour = ConvertStringToUnsingendInt(EndHour.stDatasetValue);

        //! Prüft ob die Parameter im richtigen Bereich liegen
        if((uiStartHour > 24) or (uiEndHour > 24))
        {
            return false;
        }

        //! Erweiter ggf. den Stundenbereich von 0 -> 24
        if(uiEndHour == 0)
        {
            uiEndHour = 24;
        }

        //! Fehlerhafte Eingabe wird abgefangen
        if(uiEndHour < uiStartHour)
        {
            return false;
        }

        //! Gibt den Zustand zurück
        //! (StartStunde < aktuelle Stunde < EndStunde) oder (StartStunde = EndStunde)
        bReturnState = ((uiStartHour < Timer.get_hour() and Timer.get_hour() < uiEndHour) or (uiStartHour == Timer.get_hour()));
    }

    //! Gibt den Status zurück
    return bReturnState;
}

/**
* @brief Überprüft, ob das Zielsystem beendet werden soll
* @return true = Zielsystem beenden | false = Zielsystem nícht beenden
*/
/**
* @test Vorbedingung: Paketbox ist betriebsbereit \n
*       Funktion: Methode mit verschiedenen Parametern [0..4] überprüfen \n
*       Nachbedinung: Zielsystem muss sich korrekt verhalten \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool UserManagement::HandleTargetSystemByWebsite()
{
    //! Lädt den Datensatz aus der Datenbank
    const std::string cstReceivedInformation = BufferLoginInformationFromSettingsDatabase(_cuiDatasetNumberHandleCode);
    const unsigned cuiControlCode = ConvertStringToUnsingendInt(cstReceivedInformation);

    //! Initialisiert die strings
    std::string stCommandLine = "";
    std::string stLog = "";

    //! Wählt anhand des Controllcodes die Aktion aus
    switch(cuiControlCode)
    {
    case TARGETCONTROL::DONOTHING:
    {
        return false;
    }
    case TARGETCONTROL::SHUTDOWN:
    {
        //! Fährt das System in einer Minute herunter
        stCommandLine = "shutdown -P 1";
        stLog = "Das Zielsystem wird in einer Minute heruntergefahren";
        break;
    }
    case TARGETCONTROL::RESTART:
    {
        //! Startet das System in 60 Sekunden neu
        stCommandLine = "shutdown -r 1";
        stLog = "Das Zielsystem wird in einer Minute heruntergefahren und anschließend neu gestartet";
        break;
    }
    default:
    {
        return false;
    }
    }

    //! Schreibe Systemlog
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::NOTICE, stLog);

    //! Setzt Datenbankeintrag zurück
    WriteInformationToDatabase(_cuiDatasetNumberHandleCode,0);

    //! Führt das Terminalkommando
    int iTerminalState = system(stCommandLine.c_str());

    std::ignore = iTerminalState;
    return true;
}

/**
* @brief Initialisiert alle Klassenvariablen
* @return Kein Rückgaberwert
*/
/**
* @test Wird mit Klassentest der Klasse CheckWorkingState() überprüft
* @see CheckWorkingState()
*/
void UserManagement::initialize()
{
    //! Speichert die Wochentage ab
    _mapWDays[_cuiLowerDatasetNumberTimeHandler] = "Sun";
    _mapWDays[_cuiLowerDatasetNumberTimeHandler + 1] = "Mon";
    _mapWDays[_cuiLowerDatasetNumberTimeHandler + 2] = "Tue";
    _mapWDays[_cuiLowerDatasetNumberTimeHandler + 3] = "Wed";
    _mapWDays[_cuiLowerDatasetNumberTimeHandler + 4] = "Thu";
    _mapWDays[_cuiLowerDatasetNumberTimeHandler + 5] = "Fri";
    _mapWDays[_cuiLowerDatasetNumberTimeHandler + 6] = "Sat";
}

/**
* @brief Holt sich den geforderten Datensatz aus der Datenbank und speichert diesen in stDatasetValue (Klassenvariable)
* @param[in] cuiDatasetNumber: Datensatznummer
* @return true = Schreibvorgang war erfolgreich
*/
/**
* @test Wird mit Klassentest der Klasse CheckWorkingState() überprüft
* @see CheckWorkingState()
*/
std::string UserManagement::BufferLoginInformationFromSettingsDatabase(const unsigned int cuiDatasetNumber)
{
    //! Initialisiert die Structur und beschreibt diese
    Settings PreBuffer;
    DatabaseWrapper::ReadInformationFromDatabase(cuiDatasetNumber, PreBuffer);

    //! Gibt den Datensatz zurück
    return PreBuffer.stDatasetValue;
}
