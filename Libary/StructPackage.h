/**
 * @file   StructSettings.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 1.0
 *
 * @brief enthält die Definition der Struktur Package
 */

#include <string>

#ifndef STRUCTPACKAGE_H_INCLUDED
#define STRUCTPACKAGE_H_INCLUDED

/**
* @struct Beinhaltet die Datenstruktur nach einer Datenbankabfrage für die Paketdatenbank (Packages)
*/
using Package = struct PackageTemplate //!Package
{
    std::string stDeliverer;         //!< Beinhaltet den Lieferanten Namen
    std::string stBarcode;           //!< Beinhaltet den Barcode (Primary Key)
    std::string stContent;           //!< Beinhaltet den Inhalt des Paketes
    std::string stDeliveryState;     //!< Beinhaltet den Lieferstatus
    std::string stDelivered;         //!< Beinhaltet den Zustellstatus

    ~PackageTemplate() {}       //!< Destruktor
};

#endif // STRUCTPACKAGE_H_INCLUDED
