/**
 * @file   StructSettings.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 1.0
 *
 * @brief enthält die Definition der Struktur Receiver
 */

 #include <string>

#ifndef STRUCTRECEIVER_H_INCLUDED
#define STRUCTRECEIVER_H_INCLUDED

/**
* @struct Beinhaltet die Datenstruktur nach einer Datenbankabfrage für die Paketdatenbank (Receiver)
*/
using Receiver = struct ReceiverTemplate//!Receiver
{
    std::string stName;          //!< Empfänger Name
    std::string stSureName;      //!< Empfänger Vorname
    std::string stMailAdress;    //!< Empfänger Mail Adresse
    std::string stBarcode;       //!< PaketBarcode (Primary Key)

    ~ReceiverTemplate() {}  //!< Destruktoraufruf

};
#endif // STRUCTRECEIVER_H_INCLUDED
