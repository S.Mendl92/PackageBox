/**
 * @file   ReceiveTime.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Dezember, 2016
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für ReceiveTime)
 *
 * @section Implementierte Methoden:
 * @li ReceiveTime()
 * @li ~ReceiveTime()
 * @li string get_time()
 * @li string get_wDay()
 * @li string get_month()
 * @li unsigned int get_mDay()
 * @li unsigned int get_Year()
 * @li unsigned int get_hour()
 * @li void initialize()
 * @li void getLocalTime()
 * @li bool lessThan10(const int ciValue) const
 */
#include "../Libary/ReceiveTime.h"

/**
* @brief Konstruktoraufruf der Klasse ReceiveTime
*/
ReceiveTime::ReceiveTime()
{

}

/**
* @brief Destruktoraufruf der Klasse ReceiveTime
*/
ReceiveTime::~ReceiveTime()
{

}


/**
* @brief Liefert die aktuelle Uhrzeit
* @return Gibt Uhrzeit im Format hh:mm:ss zurück
*/
/**
* @test Vorbedingung: Es wurde noch keine Zeit geholt \n
*       Funktion: get_time() ausführen \n
*       Nachbedinung: Aktuelle Uhrzeit muss ausgegeben werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string ReceiveTime::get_time()
{
    //! Empfängt die aktuelle Zeit
    getLocalTime();
    char chBuffer[_cuiBufferSize] = {0};

    //! Formartiert die Zeit
    strftime (chBuffer,_cuiBufferSize,"%H:%M:%S",_TimeStamp);

    return chBuffer;
}
/**
* @brief Liefert den aktuellen Monatstag als Zahl
* @return Gibt den aktuellen Monatstag zurück
*/
/**
* @test Vorbedingung: die aktuelle Zeit wurde noch nicht abgefragt \n
*       Funktion: get_mDay() ausführen\n
*       Nachbedinung: Aktueller Monatstag muss richtig sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
unsigned int  ReceiveTime::get_mDay()
{
    //! Empfängt die aktuelle Zeit
    getLocalTime();
    return _TimeStamp->tm_mday;
}

/**
* @brief Liefert den aktuellen Wochentag als Text zurück
* @return Gibt den aktuellen Wochentag zurück
*/
/**
* @test Vorbedingung: die aktuelle Zeit wurde noch nicht abgefragt \n
*       Funktion: get_wDay() ausführen\n
*       Nachbedinung: Aktueller Wochentag muss richtig sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string ReceiveTime::get_wDay()
{
    //! Empfängt die aktuelle Zeit
    getLocalTime();
    char chBuffer[_cuiBufferSize] = {0};

    //! Formartiert die Zeit
    strftime (chBuffer,_cuiBufferSize,"%a",_TimeStamp);

    return chBuffer;
}

/**
* @brief Liefert den aktuellen Monat als Text zurück
* @return Gibt den aktuellen Monat zurück
*/
/**
* @test Vorbedingung: die aktuelle Zeit wurde noch nicht abgefragt \n
*       Funktion: get_month() ausführen \n
*       Nachbedinung: Aktueller Monat muss richtig sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string ReceiveTime::get_month()
{
    //! Empfängt die aktuelle Zeit
    getLocalTime();
    char chBuffer[_cuiBufferSize] = {0};

    //! Formartiert die Zeit
    strftime (chBuffer,_cuiBufferSize,"%h",_TimeStamp);

    return chBuffer;
}
/**
* @brief Liefert das aktuelle Jahr
* @return Gibt das aktuelle Jahr zurück
*/
/**
* @test Vorbedingung: die aktuelle Zeit wurde noch nicht abgefragt \n
*       Funktion: get_Year() ausführen \n
*       Nachbedinung: Aktuelles Jahr muss richtig sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
unsigned int  ReceiveTime::get_Year()
{
    //! Empfängt die aktuelle Uhrzeit
    getLocalTime();

    //! Jahr seit 1900
    return (_TimeStamp->tm_year + 1900);
}
/**
* Liefert die aktuelle Zeit
* @return Keine, Parameter der Klasse werden überschrieben
*/
/**
* @test Wird von den Public Mehtoden überprüft, kein direkter Test notwendig
*/
void ReceiveTime::getLocalTime()
{
    //! Initialisiert die Zeitvariablen
    time_t t = time(nullptr);
    _TimeStamp = localtime( & t );
}

/**
* @brief Gibt die aktuelle Stunde zurück
* @return aktuelle Stunde
*/
/**
* @test Vorbedingung: die aktuelle Zeit wurde noch nicht abgefragt \n
*       Funktion: get_hour() ausführen \n
*       Nachbedinung: Aktuelle Stunde muss richtig sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/

unsigned int ReceiveTime::get_hour()
{
    //! Empfängt die aktuelle Uhrzeit
    getLocalTime();

    return (_TimeStamp->tm_hour);
}
