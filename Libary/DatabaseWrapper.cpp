/**
 * @file   DatasetWrapper.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für DatasetWrapper)
 *
 * @section Implementierte Methoden:
 * @li DatabaseWrapper()
 * @li ~DatabaseWrapper()
 * @li string get_DatabaseName() const
 * @li string get_DatabasePath() const
 * @li string get_WholeDatabasePath() const
 * @li bool ReadInformationFromDatabase(const unsigned int cuiDatasetNumber, Settings& Parameters)
 * @li void WriteInformationToDatabase(const unsigned int cuiDatasetNumber,const unsigned int cuiDatabaseValue)
 */


#include "../Libary/DatabaseWrapper.h"

/**
* @brief Konstruktoraufruf der Klasse DatabaseWrapper
*/
DatabaseWrapper::DatabaseWrapper()
{
    initialize();
}

/**
* @brief Destruktoraufruf der Klasse DatabaseWrapper
*/
DatabaseWrapper::~DatabaseWrapper()
{

}

/**
* @brief Übermittelt den Namen der verwendeten Datenbank
* @return Datenbankname
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string DatabaseWrapper::get_DatabaseName() const
{
    return _cstDatabaseName;
}

/**
* @brief Übermittelt den Ordner der Datenbank
* @return Ordnerpfad der Datenbank
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string DatabaseWrapper::get_DatabasePath() const
{
    return _cstDatabasePath;
}

/**
* @brief Übermittelt den kompletten Pfad zur Datenbank
* @return Pfad zur Datenbank
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string DatabaseWrapper::get_WholeDatabasePath() const
{
    return _stWholeDatabasePath;
}

/**
* @brief Liest die Informationen aus der Datenbank (Settings.db)
* @param[in] cuiDatasetNumber: Nummer des Datensatzes
* @param[in,out] Prameters: Referenzstruktur die nur zurückgegeben wird und die aktuellen Datensätze beinhaltet
* @return true = Informationen wurden erfolgreich übermittelt
*/
/**
* @test Vorbedingung: Datenbank muss Parameter besitzen  \n
*       Funktion: Methode mit verschiedenen Parametern aufrufen \n
*       Nachbedinung: Parameter müssen richtig übertragen sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool DatabaseWrapper::ReadInformationFromDatabase(const unsigned int cuiDatasetNumber, Settings& Parameters)
{
    //! Initialisiert die Datenbankabfrage
    sqlite3* SqliteDatabase;
    sqlite3_stmt* Statement;

    //! Beendet die Transaktion, wenn Datenbank nicht geöffnet werden kann
    if (sqlite3_open(_stWholeDatabasePath.c_str(), &SqliteDatabase) != SQLITE_OK)
    {
        assert(false);

        //! Bereitet einen string für den Systemlog vor
        std::string stLog = "Datenbank " + _cstDatabaseName + " konnte nicht geöffnet werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, stLog);

        return false;
    }
    //! SQL Befehl
    std::stringstream Query;
    Query << "SELECT * FROM Settings "
          << "WHERE "
          << "DatasetNumber = "
          << cuiDatasetNumber
          << ";";

    //! Bereitet die SQL Abfrage vor (c string)
    sqlite3_prepare(SqliteDatabase, Query.str().c_str(),-1, &Statement, nullptr);

    //! Datenbankabfrage
    sqlite3_step(Statement);

    //! Initialisiert die stringsreams als Puffervariablen
    std::stringstream ssDatasetNumber;
    std::stringstream ssDatasetType;
    std::stringstream ssDatasetValue;
    std::stringstream ssDatasetComment;

    //! Weist einzelne Werte zu
    ssDatasetNumber << sqlite3_column_text(Statement, 0);
    ssDatasetType << sqlite3_column_text(Statement, 1);
    ssDatasetValue << sqlite3_column_text(Statement, 2);
    ssDatasetComment << sqlite3_column_text(Statement, 3);

    //! Überträgt die Werte in die Struktur
    Parameters.stDatasetNumber = ssDatasetNumber.str();
    Parameters.stDatasetType = ssDatasetType.str();
    Parameters.stDatasetValue = ssDatasetValue.str();
    Parameters.stDatasetComment = ssDatasetComment.str();

    //! Schließt die Sitzung ab
    sqlite3_finalize(Statement);
    sqlite3_close(SqliteDatabase);

    //! Die Datensatznummer darf nicht leer sein (sonst war die Abfrage ausserhalb des Bereichs)
    return (ssDatasetNumber.str() != "");
}

/**
* @brief Schreibt Informationen in die Datenbank (Settings.db)
* @param[in] cuiDatasetNumber: Nummer des Datensatzes
* @param[in] cuiDatabaseValue: zu schreibender  Parameter
* @return true = Informationen wurden erfolgreich geschrieben
*/
/**
* @test Vorbedingung: Datenbank muss Parameter besitzen \n
*       Funktion: Methode mehrmals aufrufen und Datensätze verändern \n
*       Nachbedinung: Datensätze müssen korrekt geändert sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void DatabaseWrapper::WriteInformationToDatabase(const unsigned int cuiDatasetNumber,const unsigned int cuiDatabaseValue)
{
    //! Initialisiert die Datenbankabfrage
    sqlite3* SqliteDatabase;
    sqlite3_stmt* Statement;

    //! Beendet die Transaktion, wenn Datenbank nicht geöffnet werden kann
    if (sqlite3_open(_stWholeDatabasePath.c_str(), &SqliteDatabase) != SQLITE_OK)
    {
        assert(false);

        //! Bereitet einen string für den Systemlog vor
        std::string stLog = "Datenbank " + _cstDatabaseName + " konnte nicht geöffnet werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, stLog);

        return;
    }
    //! SQL Befehl
    std::stringstream Query;
    Query << "UPDATE Settings "
          << "SET DatasetValue = "
          << cuiDatabaseValue
          << " WHERE "
          << "DatasetNumber = "
          << cuiDatasetNumber
          << " ;";

    //! Bereitet die SQL Abfrage vor (c string)
    sqlite3_prepare(SqliteDatabase, Query.str().c_str(),-1, &Statement, nullptr);

    //! Datenbankabfrage
    sqlite3_step(Statement);

    //! Schließt die Sitzung ab
    sqlite3_finalize(Statement);
    sqlite3_close(SqliteDatabase);

    //! Schreibt einen Systemlog
    std::stringstream ssLog;
    ssLog << "Datesatznummer " << cuiDatasetNumber << " in Datenbank " << _cstDatabaseName << ": wurde auf " << cuiDatabaseValue
          << " geändert";

    Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL,ssLog.str());
}

/**
* Initialisiert alle Klassenvariablen
* @return Kein Rückgabewert
*/
void DatabaseWrapper::initialize()
{
    //! kompletter Datenpfad wird zugewiesen
    _stWholeDatabasePath = _cstDatabasePath + _cstDatabaseName;
}
