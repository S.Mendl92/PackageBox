/**
 * @file   GPIOControl.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für GPIOControl)
 *
 * @section Implementierte Methoden:hoden:
 * @li GPIOControl()
 * @li ~GPIOControl()
 * @li bool ControlDoor(const bool cbState,const bool cbWriteSystemLog)
 * @li bool ControlDoor(const unsigned int cuiTimeInSeconds,const bool cbWriteSystemLog)
 * @li bool ControlErrorLED(const bool cbState,const bool cbWriteSystemLog)
 * @li bool WaitTime(const unsigned int cuiTimeInSeconds)
 * @li void ContolAktorsByWebsite(const bool cbLockThread1,const bool cbLockThread2)
 * @li void SendStateToGPIOPort()
 * @li void WriteStateInFile(const string cstWholeTextfilePath,const bool cbActiveStatePackageBox)
 * @li bool get_DoorState() const
 * @li bool get_ErrorLEDState() const
 * @li bool get_ScannerButtonState()
 * @li bool get_ErrorButtonState()
 * @li void initialize()
 * @li void IncreaseClassCounter()
 * @li void SetInternalCreationalNumber()
 * @li bool ReadScannerButton()
 * @li bool ReadErrorButton()
 * @li string ConvertAktorState(const bool cbState) const
 * @li string ConvertSensorState(const bool cbState) const
 * @li string ConvertActiveState(const bool cbState) const
 * @li void GPIOPortSetup()
 * @li bool GetStateFromGpioPort(const unsigned int cuiPortNumber
 * @li void SetStateOfGpioPort(const unsigned int cuiPortNumber, const bool cbValue)
 */
#include "../Libary/GPIOControl.h"

//!< Initialisert die static Klassenvariablen der Klasse GPIOControl
bool GPIOControl::_bDoorState = false;
bool GPIOControl::_bErrorLEDState = false;
bool GPIOControl::_bScannerButtonState = false;
bool GPIOControl::_bErrorButtonState = false;
bool GPIOControl::_bLastControlStateErrorLed = false;
bool GPIOControl::_bLastControlStateDoor = false;
unsigned long long int GPIOControl::_ulliCreationalNumber = 0;

/**
* @brief Konstruktoraufruf der Klasse GPIOControl
*/
GPIOControl::GPIOControl()
{
    //! Alle Klassenvariablen werden initialisiert
    initialize();
}

/**
* @brief Destruktoraufruf der Klasse GPIOControl
*/
GPIOControl::~GPIOControl()
{

}

/**
* @brief Steuert die Tür so lange an, bis der Status bState geändert wird
* @param[in] cbState: Tür wird angesteuert (true = ansteueren, false = nicht ansteuern)
* @param[in] cbWriteSystemLog: true = Systemlog wird geschrieben | false = Systemlog wird nicht geschrieben
* @return aktueller Ansteuerung der Tür
*/
/**
* @test Vorbedingung: Türvariable ist nicht betätigt \n
*       Funktion: Methode ausführen mit verschiedenen Parametern \n
*       Nachbedinung: Tür muss korrekt angesteuert werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool GPIOControl::ControlDoor(const bool cbState,const bool cbWriteSystemLog)
{
    //! Übergibt den Steuerwert
    if(cbState)
    {
        //! Setzt die Static Variable bDoorState auf true
        _bDoorState = true;

        if(cbWriteSystemLog)
        {
            //! Systemlogausgabe
            std::string stLog = "Tür wurde gesteuert (Steuerwert = true)";
            Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);
        }
    }
    else
    {
        //! Setzt die Static Variable bDoorState auf false
        _bDoorState = false;

        if(cbWriteSystemLog)
        {
            //! Systemlogausgabe
            std::string stLog = "Tür wurde gesteuert (Steuerwert = false)";
            Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);
        }
    }

    //! Gibt den aktuellen Schaltstatus zurück
    SetStateOfGpioPort(GPIOPORT::GREENLEDPORT,_bDoorState);
    return _bDoorState;
}

/**
* @brief Steuert die Tür für die angelegte Zeit an
* @param[in] cuiTimeInSeconds: Ansteuerungszeit in Sekunden
* @param[in] cbWriteSystemLog: true = Systemlog wird geschrieben | false = Systemlog wird nicht geschrieben
* @return aktueller Ansteuerung der Tür
*/
/**
* @test Vorbedingung: Türvariable ist nicht betätigt \n
*       Funktion: Methode ausführen mit verschiedenen Parametern \n
*       Nachbedinung: Tür muss korrekt angesteuert werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool GPIOControl::ControlDoor(const unsigned int cuiTimeInSeconds,const bool cbWriteSystemLog)
{
    //! Steuert die Tür an, wartet Zeit ab und schaltet die Ansteuerung wieder aus
    ControlDoor(true, cbWriteSystemLog);
    WaitTime(cuiTimeInSeconds);
    return ControlDoor(false, cbWriteSystemLog);
}

/**
* @brief Empfängt die Kommandos von der Website und Steuert die Aktoren direkt an
* @param[in] cbLockThread1: Zustand des Thread1 (true = gesperrt | false = freigegeben)
* @param[in] cbLockThread2: Zustand des Thread2 (true = gesperrt | false = freigegeben)
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Variablen sind nicht gesetzt  \n
*       Funktion: Methode ausführen mit verschiedenen Parametern \n
*       Nachbedinung: Aktoren müssen korrekt angesteuert werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void GPIOControl::ContolAktorsByWebsite(const bool cbLockThread1,const bool cbLockThread2)
{
    //! Falls ein Thread aktiv ist, wird die Methode beendet
    if(cbLockThread1 or cbLockThread2)
    {
        return;
    }

    //! Initialisiert die Setting Struktur und übermittelt die Parameter aus der Datenbank
    Settings ReceiveDatasetDoorControl;
    Settings ReceiveDatasetErrorLEDControl;

    ReadInformationFromDatabase(_cuiNumberDoorControlByWebsite, ReceiveDatasetDoorControl);
    ReadInformationFromDatabase(_cuiNumberControlErrorLedByWebsite, ReceiveDatasetErrorLEDControl);

    //! Wertet die Datenbank Werte aus
    bool bControlDoor = (ReceiveDatasetDoorControl.stDatasetValue == "1");
    bool bControlErrorLed = (ReceiveDatasetErrorLEDControl.stDatasetValue == "1");

    //! Steuert die Aktoren an
    ControlDoor(bControlDoor, false);
    ControlErrorLED(bControlErrorLed, false);

    //! Falls es einen Zustandswechsel gibt, wird ein Systemlog ausgegeben
    if(bControlDoor and not _bLastControlStateDoor)
    {
        //! Schreibe Systemlog
        std::string stLog = "Tür wurde von Website gesteuert";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::NOTICE, stLog);
    }

    //! Falls es einen Zustandswechsel gibt, wird ein Systemlog ausgegeben
    if(bControlErrorLed and not _bLastControlStateErrorLed)
    {
        //! Schreibe Systemlog
        std::string stLog = "Fehler LED wurde von Website gesteuert";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::NOTICE, stLog);
    }

    //! Alter Schaltstatus wird überschrieben
    _bLastControlStateDoor = _bDoorState;
    _bLastControlStateErrorLed = _bErrorLEDState;
}

/**
* @brief Schreibt die Aktor und Sensor Schaltzustände in ein definiertes Textfile
* @param[in] cstWholeTextfilePath: Der komplette Pfad zum Soundtextfile
* @param[in] cbActiveStatePackageBox: Status der Paketbox
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Programm initialisieren \n
*       Funktion: Methode ausführen \n
*       Nachbedinung: File muss korrekt beschrieben werdens \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void GPIOControl::WriteStateInFile(const std::string cstWholeTextfilePath,const bool cbActiveStatePackageBox)
{
    //! Öffnet das Textfile und überprüft, ob es geöffnet ist
    std::ofstream file(cstWholeTextfilePath);

    //! Überprüft, ob das Textfile geöffnet werden kann und noch keine Fehlermeldung ausgegeben worden ist
    if(!file and !_bTextfileIsLocked)
    {
        assert(false);

        //! Sperrt die nächste Fehlermeldung
        _bTextfileIsLocked = true;

        //! Schreibt eine Systemlogmeldung
        std::string stLog = "Das Textfile: " + cstWholeTextfilePath + " konnte nicht geöffnet werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, stLog);
        return;
    }
    //! Überpüft, ob das Textfile nicht geöffnet werden konnte und schon gesperrt ist
    else if(!file and _bTextfileIsLocked)
    {
        return;
    }
    //! Gibt den Sperrstatus des Textfiles wieder frei
    else if(file and _bTextfileIsLocked)
    {
        _bTextfileIsLocked = false;
    }

    //! Schreibt die aktuellen Statuswerte in das Textfile und schließt es
    file.clear();
    file << "Folgende Aktoren sind momentan angesteuert:" << std::endl
         << "Die Tür ist " << ConvertAktorState(_bDoorState) << std::endl
         << "Die LED ist " << ConvertAktorState(_bErrorLEDState) << std::endl
         << "Der Scanner Taster ist " << ConvertSensorState(_bScannerButtonState)<< std::endl
         << "Der Fehler Taster ist " << ConvertSensorState(_bErrorButtonState) << std::endl
         << "Die Paketbox ist " << ConvertActiveState(cbActiveStatePackageBox) << std::endl;

    file.close();
}

/**
* @brief Steuert die Fehler LED so lange an, bis der Status bState geändert wird
* @param[in] cbState: LED wird angesteuert (true = an, false = aus)
* @param[in] cbWriteSystemLog: true = schreibe Systemlog
* @return aktueller Ansteuerung der Fehler LED
*/
/**
* @test Vorbedingung: Fehler LED darf nicht angesteuert sein \n
*       Funktion: Fehler LED ansteuern \n
*       Nachbedinung: LED muss richtig angesteuert werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool GPIOControl::ControlErrorLED(const bool cbState,const bool cbWriteSystemLog)
{
    if(cbState)
    {
        //! Ändert den Schaltzustand der LED auf true (statc Variable)
        _bErrorLEDState = true;

        if(cbWriteSystemLog)
        {
            //! Schreibe Log Meldung
            std::string stLog = "Fehler LED wurde gesteuert (Steuerwert = true)";
            Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);
        }

    }
    else
    {
        //! Ändert den Schaltzustand der LED auf false (statc Variable)
        _bErrorLEDState = false;

        if(cbWriteSystemLog)
        {
            //! Schreibe Log Meldung
            std::string stLog = "Fehler LED wurde gesteuert (Steuerwert = false)";
            Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);
        }
    }

    //! Aktueller Schaltwert wird zurückgegeben
    SetStateOfGpioPort(GPIOPORT::REDLEDPORT,_bErrorLEDState);
    return _bErrorLEDState;
}

/**
* @brief Gibt ein true nach der angelegten Zeit zurück
* @param[in] uiTime: zu wartende Zeit in Sekunden
* @return true = Zeit ist abgelaufen
*/
/**
* @test Vorbedingung: Klasse initalisieren \n
*       Funktion: Methode mit verschiedenen Werten aufrufen \n
*       Nachbedinung: Ergebnis muss wie erwartet ausgegeben werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool GPIOControl::WaitTime(unsigned int uiTimeInSeconds)
{
    //! initialisert die Zeit Variablen
    std::chrono::seconds Seconds(uiTimeInSeconds);
    std::this_thread::sleep_for(Seconds);

    //! beendet die Funktion
    return true;
}

/**
* @brief Überpüft den Schaltzustand der Tür
* @return Schaltzustand der Tür
*/
/**
* @test Vorbedingung: Schaltzustand der Tür überprüfen \n
*       Funktion: Schaltzustand ändern \n
*       Nachbedinung: Wert muss korrekt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool GPIOControl::get_DoorState() const
{
    return _bDoorState;
}

/**
* @brief Überpüft den Schaltzustand der Fehler LED
* @return Schaltzustand der Fehler LED
*/
/**
* @test Vorbedingung: Schaltzustand der LED überprüfen \n
*       Funktion: Schaltzustand ändern \n
*       Nachbedinung: Wert muss korrekt sein \n
*       Status: bestanden
*/
bool GPIOControl::get_ErrorLEDState() const
{
    return _bErrorLEDState;
}

/**
* @brief Überpüft den Schaltzustand der ScannerButtons
* @return Schaltzustand des ScannerButtons
*/
/**
* @test Vorbedingung: Schaltzustand des Scanner Tasters überprüfen \n
*       Funktion: Schaltzustand ändern \n
*       Nachbedinung: Wert muss korrekt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool GPIOControl::get_ScannerButtonState()
{
    return ReadScannerButton();
}

/**
* @brief Überpüft den Schaltzustand des Fehlertasters
* @return Schaltzustand des Fehlertaster
*/
/**
* @test Vorbedingung: Schaltzustand des Fehlertasters überprüfen \n
*       Funktion: Schaltzustand ändern \n
*       Nachbedinung: Wert muss korrekt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool GPIOControl::get_ErrorButtonState()
{
    return ReadErrorButton();
}

/**
* @brief Initialisiert alle Klassenvariablen
* @return Kein Rückgabewert
* @todo Muss noch für den Rasberry Pi vollständig implementiert werden
*/
/**
* @test Kein Test erforderlich
*/
void GPIOControl::initialize()
{
    //! Initialisiert die Klassenvariablen
    _bTextfileIsLocked = false;

    //! Anzahl der Instanzen wird erhöht und der Klassenvariable zugewiesen
    IncreaseClassCounter();
    SetInternalCreationalNumber();

    if(_ulliCreationalNumber == 1)
    {
        GPIOPortSetup();
    }
}

/**
* @brief Initialisiert den GPIOPort des Rasberry PI
* @return Kein Rückgabewert
*/
/**
* @test Kein Test erforderlich
*/
void GPIOControl::GPIOPortSetup()
{
    std::vector<std::string> TermialCommands;
    TermialCommands.clear();

    //!Portnummern werden festgelegt
    const std::string cstGreenLedPort = ConvertUnsingendIntToString(GPIOPORT::GREENLEDPORT);
    const std::string cstRedLedPort = ConvertUnsingendIntToString(GPIOPORT::REDLEDPORT);
    const std::string cstScannerButtonPort = ConvertUnsingendIntToString(GPIOPORT::SCANNERBUTTONPORT);
    const std::string cstErrorButtonPort = ConvertUnsingendIntToString(GPIOPORT::ERRORBUTTONPORT);

    //!Ordnerstruktur wird erstellt
    TermialCommands.push_back("echo \"" + cstGreenLedPort + "\" > /sys/class/gpio/export");
    TermialCommands.push_back("echo \"" + cstRedLedPort + "\" > /sys/class/gpio/export");
    TermialCommands.push_back("echo \"" + cstScannerButtonPort + "\" > /sys/class/gpio/export");
    TermialCommands.push_back("echo \"" + cstErrorButtonPort + "\" > /sys/class/gpio/export");

    TermialCommands.push_back("echo \"out\" > /sys/class/gpio/gpio"+ cstGreenLedPort +"/direction");
    TermialCommands.push_back("echo \"out\" > /sys/class/gpio/gpio"+ cstRedLedPort +"/direction");

    TermialCommands.push_back("echo \"in\" > /sys/class/gpio/gpio"+ cstScannerButtonPort +"/direction");
    TermialCommands.push_back("echo \"in\" > /sys/class/gpio/gpio"+ cstErrorButtonPort +"/direction");

    for (unsigned int uiLoop = 0; uiLoop < TermialCommands.size(); uiLoop += 1)
    {
        const std::string cstCommandline = TermialCommands[uiLoop];
        int iTerminalState = system(cstCommandline.c_str());

        std::ignore = iTerminalState;
    }

    //! Systemlog wird ausgegeben
    std::stringstream ssLog;
    ssLog << cstGreenLedPort << " und " << cstRedLedPort << ":GPIO-OUT | " << std::endl
          << cstScannerButtonPort << " und " << cstErrorButtonPort << ":GPIO-IN ";

    Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, ssLog.str().c_str());
}

/**
* @brief Überpüft ob der Scanner Button bestätigt ist
* @return Zustand des Scanner Buttons
*/
/**
* @test @see get_ScannerButtonState
*/
bool GPIOControl::ReadScannerButton()
{
    //! Legt die Structur mit den Parameter an
    Settings ReceivedDataset;

    //! Liest die Werte aus der Datenbank
    DatabaseWrapper::ReadInformationFromDatabase(_cuiNumberScannerButtonState, ReceivedDataset);

    //! Speichert den Betätigungsstatus des Scanners Tasters ab.
    bool bOldScannerButtonState = _bScannerButtonState;

    //! Weist den neuen Schaltwert zu
    const bool cbDatabaseState = (ReceivedDataset.stDatasetValue == "1");
    const bool cbVirtualValueState = GetStateFromGpioPort(GPIOPORT::SCANNERBUTTONPORT);

    _bScannerButtonState = (cbDatabaseState or cbVirtualValueState);

    //! vergleicht, ob der Taster momentan gedrückt ist und davor nicht gedrückt war.
    if(!bOldScannerButtonState and _bScannerButtonState)
    {
        //! Vemerkt im Systemlog, das der Taster gedrückt ist
        std::string stLog = "Scanner Taster ist betätigt (Steuerwert = true)";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);
    }

    return _bScannerButtonState;
}

/**
* @brief Überpüft ob der Fehler Button betätigt ist
* @return Zustand des Fehler Buttons
*/
/**
* @test @see get_ErrorButtonState
*/
bool GPIOControl::ReadErrorButton()
{
    //! Legt die Struktur mit den Parameter an
    Settings ReceivedDataset;

    //! Liest die Werte aus der Datenbank
    DatabaseWrapper::ReadInformationFromDatabase(_cuiNumberErrorButtonState, ReceivedDataset);

    //! Speichert den Betätigungsstatus des Scanners Tasters ab.
    bool bOldErrorButtonState = _bErrorButtonState;

    //! Weist den neuen Schaltwert zu
    const bool cbDatabaseState = (ReceivedDataset.stDatasetValue == "1");
    const bool cbVirtualValueState = GetStateFromGpioPort(GPIOPORT::ERRORBUTTONPORT);

    _bErrorButtonState = (cbDatabaseState or cbVirtualValueState);

    //! vergleicht, ob der Taster momentan gedrückt ist und davor nicht gedrückt war.
    if(!bOldErrorButtonState and _bErrorButtonState)
    {
        //! Vermerkt im Systemlog, dass der Taster gedrückt ist
        std::string stLog = "Fehler Taster ist betätigt (Steuerwert = true)";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);
    }

    return _bErrorButtonState;
}

/**
* @brief Erhöhte die Anzahl der erzeugten Instanzen um 1
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: keine Klasseninstanz wurde erzeugt \n
*       Funktion: 100 mal aufrufen \n
*       Nachbedinung: ulliNumberOfInstances muss 100 betragen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void GPIOControl::IncreaseClassCounter()
{
    _ulliCreationalNumber += 1;
}

/**
* @brief Weist die aktuelle Instanznummer zu
* @return Kein Rückgabewert
*/
/**
* @test @see IncreaseClassCounter()
*/
void GPIOControl::SetInternalCreationalNumber()
{
    _ulliNumberOfInstances = _ulliCreationalNumber;
}

/**
* @brief Convert einen boolschen Wert in ein String zu "angesteuert" oder "nicht angesteuert"
* @param[in] cbState: Das zu "konvertierende" Bool
* @return Das konvertierte Bool als String
*/
/**
* @test Vorbedingung: Methode darf momentan nicht aufgerufen werden \n
*       Funktion: Methode mit verschiedenen Werten aufrufen \n
*       Nachbedinung: Richtiger Kommentar muss ausgegeben werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string GPIOControl::ConvertAktorState(const bool cbState) const
{
    //! Initalisiert den String
    std::string stReturn = "";

    //! Wählt den Rückgabewert aus
    if(cbState)
    {
        stReturn = "angesteuert";
    }
    else
    {
        stReturn = "nicht angesteuert";
    }
    return stReturn;
}

/**
* @brief Convert einen boolschen Wert in ein String zu "betätigt" oder "nicht betätigt"
* @param[in] bState: Das zu "konvertierende" Bool
* @return Das konvertierte Bool als String
*/
/**
* @test Vorbedingung: Methode darf momentan nicht aufgerufen werden \n
*       Funktion: Methode mit verschiedenen Werten aufrufen \n
*       Nachbedinung: Richtiger Kommentar muss ausgegeben werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string GPIOControl::ConvertSensorState(const bool cbState) const
{
    //! Initalisiert den String
    std::string stReturn = "";

    //! Wählt den Rückgabewert aus
    if(cbState)
    {
        stReturn = "betätigt";
    }
    else
    {
        stReturn = "nicht betätigt";
    }
    return stReturn;
}

/**
* @brief Convert einen boolschen Wert in ein String zu "aktiv" oder "nicht aktiv"
* @param[in] cbState: Das zu "konvertierende" Bool
* @return Das konvertierte Bool als String
*/
/**
* @test Vorbedingung: Methode darf momentan nicht aufgerufen werden \n
*       Funktion: Methode mit verschiedenen Werten aufrufen \n
*       Nachbedinung: Richtiger Kommentar muss ausgegeben werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string GPIOControl::ConvertActiveState(const bool cbState) const
{
    //! Initalisiert den String
    std::string stReturn = "";

    //! Wählt den Rückgabewert aus
    if(cbState)
    {
        stReturn = "aktiv";
    }
    else
    {
        stReturn = "nicht aktiv";
    }
    return stReturn;
}

/**
* @brief Frägt den Status eines GPIO Eingangs ab
* @param[in] cuiPortNumber: Die gewünschte Portnummer
* @return Wert des Portes als boolean
*/
/**
* @test Vorbedingung: System muss initialiseriert sein \n
*       Funktion: Methode mit verschiedenen Werten aufrufen \n
*       Nachbedinung: Richtiger Boolscher Wert muss ausgegeben werden \n \n
*       Status: bestanden
*       Datum: März, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool GPIOControl::GetStateFromGpioPort(const unsigned int cuiPortNumber)
{
    //! Legt die Ordnerstruktur fest
    const std::string cstFolder = "gpio" + ConvertUnsingendIntToString(cuiPortNumber) + "/";
    const std::string cstWholeFilePath = _cstFolderGpioPort + cstFolder + _cstValueFile;

    //! Öffnet die Textdatei
    std::ifstream file(cstWholeFilePath.c_str());

    if(!file)
    {
        assert(false);
        //! Systemlog wird ausgegeben
        std::string stLog = "GPIO Portfile: " + cstWholeFilePath + " existiert, oder konnte nicht geöffnet werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);
        return false;
    }

    //! Empfängt die erste Zeile aus dem Textfile
    std::string stReceivedInformation = "";
    getline(file,stReceivedInformation);

    //! Schließt das Textfile
    file.close();

    //! Wandelt den erhalten Wert um und überprüft, ob ein High Signal (= true) anliegt oder nicht (=false)
    const unsigned int cuiVirtualGpioPortValue = ConvertStringToUnsingendInt(stReceivedInformation);
    return (cuiVirtualGpioPortValue == 1);
}

/**
* @brief Schreibt den Status eines GPIOPort Ausgangs
* @param[in] cuiPortNumber: Die gewünschte Portnummer
* @param[in] cbValue: Der gewünschte Wert am Ausgang
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: System muss initialiseriert sein \n
*       Funktion: Methode mit verschiedenen Werten aufrufen \n
*       Nachbedinung: Richtiger Boolscher Wert muss am Ausgang ausgegeben werden \n \n
*       Status: bestanden
*       Datum: März, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void GPIOControl::SetStateOfGpioPort(const unsigned int cuiPortNumber, const bool cbValue)
{
    //! Legt die Ordnerstruktur fest
    const std::string cstFolder = "gpio" + ConvertUnsingendIntToString(cuiPortNumber) + "/";
    const std::string cstWholeFilePath = _cstFolderGpioPort + cstFolder + _cstValueFile;

    //! Öffnet das Textfile und überprüft, ob es geöffnet ist
    std::ofstream file(cstWholeFilePath);

    if(!file)
    {
        assert(false);
        //! Schreibt eine Systemlogmeldung
        std::string stLog = "Das Textfile: " + cstWholeFilePath + " konnte nicht geöffnet werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, stLog);
        return;
    }

    //! Schreibt die aktuellen Statuswerte in das Textfile und schließt es
    file.clear();
    file << ConvertBoolToString(cbValue) << std::endl;
    file.close();
}
