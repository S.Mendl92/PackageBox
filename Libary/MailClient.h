/**
 * @file   MailClient.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse MailClient
 *
 * @section Definierte Methoden:
 * @li MailClient()
 * @li ~MailClient()
 * @li bool SendMailOrderdPackageReceived(const Package PackageInformation, const Receiver ReceiverInformation)
 * @li bool SendMailWrongPackageReceived(const string cstBarcode)
 * @li bool SendMailPackageReceivedWithWrongSize(const Package PackageInformation, const Receiver ReceiverInformation)
 * @li bool SendMailActionDuringFalseWorkingState()
 * @li unsigned int get_ValueOfDailyMail() const
 * @li void initialize()
 * @li void PrepareMailTextForTerminal(string& stMailText)
 * @li void SendMail(const string cstPreparedMailText,const string cstReveierMailAdress)
 * @li void IncreaseDailyMailCounter()
 * @li void BufferInformationFromSettingsDatabase()
 * @li bool DailyMailCounter()
 */

#include <iostream>
#include <string>
#include <map>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <stdio.h>
#include <cassert>

#include "../Libary/Systemlog.h"
#include "../Libary/ReceiveTime.h"
#include "../Libary/DatabaseWrapper.h"
#include "../Libary/Converter.h"

#include "../Libary/StructPackage.h"
#include "../Libary/StructReceiver.h"

#ifndef MAILCLIENT_H_INCLUDED
#define MAILCLIENT_H_INCLUDED

/**
* @class MailClient
* @brief Der MailClient kommuniziert über das Programm ssmtp mit den eingestellten Anwendern
*
* @section Aufgaben
* @li Sendet eine E-Mail, das ein Paket ordnungsgemäß angekommen ist
* @li Sendet eine E-Mail, das ein Paket mit den falschen Maßen angekommen ist
* @li Sendet eine E-Mail, das ein nicht gespeichertes Paket eingescannt wird
* @li Lädt die die E-Mail Empfänger aus der Datenbank (max. 5 Stück)
* @li Benachrichtig die Benutzer, wenn keine E-Mails mehr versendet werden können
*     Mit gmail können über ssmtp nur 500 Mails pro Tag versendet werden
* @li Gibt die Anzahl der versendeten Mails zurück
* @li Name, Odnerpfad und komplette Adressierung aller verwendeten (externen) Datei
*     sollen aufrufbar sein
*/
class MailClient : protected DatabaseWrapper,  protected Converter
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/

    MailClient();
    ~MailClient();

    bool SendMailOrderdPackageReceived(const Package PackageInformation,const Receiver ReceiverInformation);
    bool SendMailWrongPackageReceived(const std::string stBarcode);
    bool SendMailPackageReceivedWithWrongSize(const Package PackageInformation,const Receiver ReceiverInformation);
    bool SendMailActionDuringFalseWorkingState();

    /****************************************************************
    *                      Get-Methoden                             *
    *****************************************************************/

    unsigned int get_ValueOfDailyMail() const;

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_MailClient;

private:
    /*****************************************************************
    *                    Private Methoden                           *
    *****************************************************************/

    void initialize();
    void PrepareMailTextForTerminal(std::string& stMailText);
    void SendMail(const std::string cstPreparedMailText, const std::string cstReveierMailAdress);
    void IncreaseDailyMailCounter();
    void BufferInformationFromSettingsDatabase();
    void WriteDailyMailsInSystemlog();

    bool DailyMailCounter();

    /*****************************************************************
    *                  Private Klassenvariablen                     *
    *****************************************************************/

    static unsigned int _uiDailyMails;           //! Anzahl der versendeten E-Mails (static)
    static bool _bSendMaxValueOfMailsReached;    //! true = letze E-Mail ist versendet (static)

    const unsigned int _cuiMaxReceiver = (DatabaseWrapper::INTERFACE::MAILRECEIVER5 - DatabaseWrapper::INTERFACE::MAILRECEIVER1+1); //! Anzahl der Maximalen E-Mail Empfänger
    const unsigned int _cuiLowerDatasetNumber = DatabaseWrapper::INTERFACE::MAILRECEIVER1; //! Untere Grenze der Datensatznummer
    const unsigned int _cuiUpperDatasetNumber = DatabaseWrapper::INTERFACE::MAILRECEIVER5; //! Obere Grenze der Datensatznummer

    unsigned int _uiCurrentDay;                  //! Beinhaltet den aktuellen Tag

    std::stringstream _ssTailText;                    //! Beinhaltet den Schlußteil jeder E-Mail
    std::map<int, std::string> _mapReceivers;         //! Beinhaltet alle E-Mail Empfänger

    Systemlog Logger;                           //! Wird für den Systemlog benötigt
    ReceiveTime Time;                           //! Wird zum Abfragen der Systemzeit benötigt
};
#endif // MAILCLIENT_H_INCLUDED
