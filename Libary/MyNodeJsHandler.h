/**
 * @file   MyNodeJsHandler.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse ZbarCam
 *
 * @section Definierte Methoden:
 * @li MyNodeJsHandler()
 * @li ~MyNodeJsHandler()
 * @li bool StartNodeJsServer()
 * @li bool StopNodeJsServer()
 * @li bool CheckStateOfNodeJsServer()
 * @li string get_ServerName() const
 * @li string get_ServerPath() const
 * @li string get_WholeServerPath() const
 * @li string get_CurrentProcessNumber() const
 * @li string get_WholeTextfileName() const
 * @li void intialize()
 * @li void DeletAllCreatedFiles(const bool cbWriteSystemLog)
 * @li string FindProcessNumber()
 */

#include <string>
#include <chrono>
#include <thread>
#include <fstream>

#include "../Libary/Systemlog.h"
#include "../Libary/MyWebsocketProtocol.h"

#ifndef MYNODEJSHANDLER_H_INCLUDED
#define MYNODEJSHANDLER_H_INCLUDED

/**
* @class MyNodeJsHandler
* @brief Verwaltet die Ansteuerung zum NodeJs Server
*
* @section Aufgaben
* @li Startet den NodeJs Server
* @li Überprüft den Status des NodeJs Servers
* @li Stopt den NodeJsServer
* @li Name, Odnerpfad und komplette Adressierung aller verwendeten (externen) Datei
*     sollen aufrufbar sein
*/
class MyNodeJsHandler : public MyWebsocketProtocol
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/

    MyNodeJsHandler();
    ~MyNodeJsHandler();

    bool StartNodeJsServer();
    bool StopNodeJsServer();
    bool CheckStateOfNodeJsServer();

    /****************************************************************
    *                      Get-Methoden                             *
    *****************************************************************/

    std::string get_ServerName() const;
    std::string get_ServerPath() const;
    std::string get_WholeServerPath() const;
    std::string get_CurrentProcessNumber() const;
    std::string get_WholeTextfileName() const;

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_MyNodeJsHandler;

private:
    /*****************************************************************
    *                    Private Methoden                           *
    *****************************************************************/

    void intialize();
    void DeletAllCreatedFiles(const bool cbWriteSystemLog);
    std::string FindProcessNumber();

    /*****************************************************************
    *                  Private Klassenvariablen                     *
    *****************************************************************/

    static std::string _stKillProcessNumber;                        //! Beinhaltet die Prozessnummer von node

    const std::string _cstServerName = "server.js";                 //! Beinhaltet den Server Name
    const std::string _cstServerPath = "./Weboberflaeche/";        //! Beinhaltet den Ordnerpfad des Servers
    const std::string _cstTextFilePath = "./Paketbox/99_temp/";    //! Beinhaltet den Pfad zum Temporären Ordner

    std::string _stWholeTextfileName;                               //! Beinhaltet den Pfad der temporären Textdateo
    std::string _stWholeServerPath;                                 //! Beinhaltet den kompletten Serverpfad

    Systemlog Logger;
};
#endif // MYNODEJSHANDLER_H_INCLUDED
