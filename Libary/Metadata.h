/**
 * @file   Metadata.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Februar, 2017
 * @version 1.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse Metadata
 *
 * @section Definierte Methoden:
 * @li ~Metadata()
 * @li void on_open(client * c, websocketpp::connection_hdl hdl)
 * @li void on_fail(client * c, websocketpp::connection_hdl hdl)
 * @li void on_close(client * c, websocketpp::connection_hdl hdl)
 * @li void on_message(websocketpp::connection_hdl, client::message_ptr msg)
 * @li websocketpp::connection_hdl get_hdl() const
 * @li int get_id() const
 * @li std::string get_status() const
 * @li void record_sent_message(std::string message)
 */

#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>

#include <websocketpp/common/thread.hpp>
#include <websocketpp/common/memory.hpp>

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <chrono>
#include <thread>


#ifndef METADATA_H_INCLUDED
#define METADATA_H_INCLUDED

typedef websocketpp::client<websocketpp::config::asio_client> client;

/**
* @class Metadata
* @brief Supportklasse für Websocketclient
*/
class Metadata
{
public:

    typedef websocketpp::lib::shared_ptr<Metadata> ptr;

    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/

    Metadata(int id, websocketpp::connection_hdl hdl, std::string uri);
    ~Metadata();

    void on_open(client * c, websocketpp::connection_hdl hdl);
    void on_fail(client * c, websocketpp::connection_hdl hdl);
    void on_close(client * c, websocketpp::connection_hdl hdl);
    void on_message(websocketpp::connection_hdl, client::message_ptr msg);

    /****************************************************************
    *                      Get-Methoden                             *
    *****************************************************************/


    websocketpp::connection_hdl get_hdl() const;
    int get_id() const;
    std::string get_status() const;
    void record_sent_message(std::string message);

    /****************************************************************
    *                Überladene Operatoren                          *
    *****************************************************************/

    friend std::ostream & operator<< (std::ostream & out, Metadata const & data)
    {
        out << "> URI: " << data.m_uri << "\n"
            << "> Status: " << data.m_status << "\n"
            << "> Remote Server: " << (data.m_server.empty() ? "None Specified" : data.m_server) << "\n"
            << "> Error/close reason: " << (data.m_error_reason.empty() ? "N/A" : data.m_error_reason) << "\n";
        out << "> Messages Processed: (" << data.m_messages.size() << ") \n";

        std::vector<std::string>::const_iterator it;
        for (it = data.m_messages.begin(); it != data.m_messages.end(); ++it)
        {
            out << *it << "\n";
        }

        return out;
    }

private:
    /*****************************************************************
    *                  Private Klassenvariablen                     *
    *****************************************************************/

    int m_id;
    websocketpp::connection_hdl m_hdl;
    std::string m_status;
    std::string m_uri;
    std::string m_server;
    std::string m_error_reason;
    std::vector<std::string> m_messages;
};
#endif // METADATA_H_INCLUDED
