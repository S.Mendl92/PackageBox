/**
 * @file   MyNodeJsHandler.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für MyNodeJsHandler)
 *
 * @section Implementierte Methoden:
 * @li MyNodeJsHandler()
 * @li ~MyNodeJsHandler()
 * @li bool StartNodeJsServer()
 * @li bool StopNodeJsServer()
 * @li bool CheckStateOfNodeJsServer()
 * @li string get_ServerName() const
 * @li string get_ServerPath() const
 * @li string get_WholeServerPath() const
 * @li string get_CurrentProcessNumber() const
 * @li string get_WholeTextfileName() const
 * @li void intialize()
 * @li void DeletAllCreatedFiles(const bool cbWriteSystemLog)
 * @li string FindProcessNumber()
 */

#include "../Libary/MyNodeJsHandler.h"

//!< Initialisert die static Klassenvariablen der Klasse MyNodeJsClient
std::string MyNodeJsHandler::_stKillProcessNumber = "";

/**
* @brief Konstruktoraufruf der Klasse: MyNodeJsHandler
*/
MyNodeJsHandler::MyNodeJsHandler()
{
    intialize();
}

/**
* @brief Destruktoraufruf der Klasse: MyNodeJsHandler
*/
MyNodeJsHandler::~MyNodeJsHandler()
{

}

/**
* @brief Startet den NodeJs Server
* @return true = Server wurde gestartet
*/
/**
* @test Vorbedingung: NodeJs Server läuft nicht \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Server muss laufen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool MyNodeJsHandler::StartNodeJsServer()
{
    //! Bereitet den String für die Komandozeile vor
    std::string stComment = "node " + _stWholeServerPath + " &";
    int iTerminalState = system(stComment.c_str());

    //! Hinterlegt die Aktion in der Datenbank
    std::string stLog = _cstServerName + " wurde gestartet";
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::NOTICE, stLog);

    FindProcessNumber();

    //! Gibt dem Server Zeit zum Starten (2 Sekunden)
    const std::chrono::seconds SleepTimeSeconds(2);
    std::this_thread::sleep_for(SleepTimeSeconds);

    std::ignore = iTerminalState;
    return true;
}

/**
* @brief Stopt den NodeJs Server anhand seiner Task Nummer
* @return true = Server wurde erfolgreich beendet
*/
/**
* @test Vorbedingung: Laufende Paketbox beenden \n
*       Funktion: Mehtode wird aufgerufen \n
*       Nachbedinung: NodeJs Server muss beendet werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool MyNodeJsHandler::StopNodeJsServer()
{
    //! Falls es keine Tasknummer gibt wird eine die aktuelle Nummer gesucht
    if(_stKillProcessNumber == "")
    {
        FindProcessNumber();
    }

    //! Server wurde anderweitig beendet
    if(_stKillProcessNumber == "")
    {
        assert(false);
        return false;
    }

    //! Bereitet den String für die Komandozeile vor
    const std::string cstComment = "kill " + _stKillProcessNumber;
    int iTerminalState = system(cstComment.c_str());

    //! Hinterlegt die Aktion in der Datenbank
    std::string stLog = _cstServerName + " wurde gestoppt";
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::NOTICE, stLog);

    std::ignore = iTerminalState;
    return true;
}

/**
* @brief Überprüft, ob der Server aktuell läuft
* @return true = Server ist in Betrieb | false = Server ist nicht betriebsbereit
*/
/**
* @test Vorbedingung: Paketbox ist initialisiert \n
*       Funktion: NodeJS Server beenden \n
*       Nachbedinung: NodeJS Server muss erneut starten \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool MyNodeJsHandler::CheckStateOfNodeJsServer()
{
    //! Bereitet den String für die Komandozeile (Sucht nach dem Prozess node)
    const std::string cstComment = "ps -a | grep node > " + _stWholeTextfileName;
    int iTerminalState = system(cstComment.c_str());

    //! Öffnet die Textdatei
    std::ifstream file(_stWholeTextfileName.c_str());

    if(!file)
    {
        assert(false);
        //! Systemlog wird ausgegeben
        std::string stLog = "Textfile: " + _stWholeTextfileName + " existiert, oder konnte nicht angelegt werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);
        return "";
    }

    //! Empfängt die erste Zeile aus dem Textfile
    std::string stReceivedInformation = "";
    getline(file,stReceivedInformation);

    //! Schließt das Textfile und löscht es
    file.close();
    DeletAllCreatedFiles(false);

    //! Wenn Textfile keine Informationen enthält
    if(stReceivedInformation == "")
    {
        assert(false);
        std::string stLog = "Der Server " + _cstServerName + " ist nicht aktiv";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);

        return false;
    }

    //! true = Textfile enthält Informationen
    std::ignore = iTerminalState;
    return (stReceivedInformation != "");
}

/**
* @brief Übergibt den Namen des NodeJs Servers
* @return Name des NodeJs Servers
*/
std::string MyNodeJsHandler::get_ServerName() const
{
    return _cstServerName;
}

/**
* @brief Übergibt den Ordnerpfades zum NodeJs Server
* @return Ordnerpfad
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string MyNodeJsHandler::get_ServerPath() const
{
    return _cstServerPath;
}

/**
* @brief Übergibt den kompletten Datenpfad zum NodeJs Server
* @return kompletter Datenpfad
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string MyNodeJsHandler::get_WholeServerPath() const
{
    return _stWholeServerPath;
}

/**
* @brief Übergibt die aktuelle Prozessnummer des NodeJs Servers
* @return Aktuelle Process Nummer
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string MyNodeJsHandler::get_CurrentProcessNumber() const
{
    return _stKillProcessNumber;
}

/**
* @brief Übergibt den Namen der Temporären Datei
* @return Name der Temporären Datei
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n
*       Status:
*/
std::string MyNodeJsHandler::get_WholeTextfileName() const
{
    return _stWholeTextfileName;
}

/**
* Initialisiert alle Klassenvariablen
* @return Kein Rückgabewert
*/
void MyNodeJsHandler::intialize()
{
    //! Weist den kompletten Datenpfad zu
    _stWholeServerPath = _cstServerPath + _cstServerName;
    _stWholeTextfileName = _cstTextFilePath + _cstServerName;
}

/**
* @brief Findet die Task Nummer des laufenden NodeJs Servers
* @return Task Nummer des NodeJs Servers
*/
/**
* @test @see StopNodeJsServer()
*/
std::string MyNodeJsHandler::FindProcessNumber()
{
//! Bereitet den String für die Kommandozeile (sucht nach dem Prozess node)
    std::string stComment = "ps -a | grep node > " + _stWholeTextfileName;
    int iTerminalState = system(stComment.c_str());

    //! Öffnet die Textdatei
    std::ifstream file (_stWholeTextfileName.c_str());

    if(!file)
    {
        assert(false);

        //! Systemlog wird ausgegeben
        std::string stLog = "Textfile: " + _stWholeTextfileName + " existiert, oder konnte nicht angelegt werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);
        return "";
    }

    //! Überträgt die Informationen auf dem string
    std::string stReceivedInformation = "";
    getline(file,stReceivedInformation);

    //! Falls der string leer ist
    if (stReceivedInformation == "")
    {
        assert(false);

        //! Schreibe Systemlog
        std::string stLog = "Textfile: " + _stWholeTextfileName + " hat keinen Inhalt, der Prozess wurde anderweitig beendet";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);

        //! Löscht die erstellten Files
        DeletAllCreatedFiles(true);
        return "";
    }

    file.close();

    //! sucht nach der ersten Leerstelle
    //! Beispielhafte Ausgabe: 16800 pts/0    10:00:49 node
    const unsigned int cuiFindPlace = stReceivedInformation.find(" ",2);

    //! Weist die Prozessnummer zu
    _stKillProcessNumber = stReceivedInformation.substr(0, cuiFindPlace);

    //! Löscht die erstellten Files
    DeletAllCreatedFiles(true);

    std::ignore = iTerminalState;
    return _stKillProcessNumber;
}

/**
* @brief Löscht das erstellte Textfile
* @param[in] bWriteSystemLog: true = Systemlog wird geschrieben | false = Systemlog wird nicht geschrieben
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Paketbox muss betriebsbereit sein  \n
*       Funktion: Methode wird aufgerufen \n
*       Nachbedinung: Es dürfen keine Files mehr im 99_Tempordner sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void MyNodeJsHandler::DeletAllCreatedFiles(const bool cbWriteSystemLog)
{
    //! Initialisiert den string fürs Terminal
    std::string stCommandline = "rm " + _stWholeTextfileName;
    int iTerminalState = system(stCommandline.c_str());

    //! Schreibt die Log Meldung falls true
    if(cbWriteSystemLog)
    {
        //! Schreibe Systemlog
        std::string stLog = "Datei: " + _stWholeTextfileName + " wurde geloescht";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);
    }

    std::ignore = iTerminalState;
}
