/**
 * @file   Database.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für Database)
 *
 * @section Implementierte Methoden:
 * @li Database()
 * @li ~Database()
 * @li bool CheckIfBarcodeIsInDatabase(const string cstBarcode, Package& DeliverdPackage, Receiver& DeliverdReceiver)
 * @li bool ChangeDeliveryState(const string cstBarcode)
 * @li string get_DatabaseName() const
 * @li string get_DatabaseFolder() const
 * @li string get_WholeDatabasePath() const
 * @li void initialize()
 */

#include "../Libary/Database.h"

/**
* @brief Konstruktoraufruf
*/
Database::Database()
{
    //! Initialisiert alle Variablen
    initialize();
}

/**
* @brief Destruktoraufruf
*/
Database::~Database()
{

}

/**
* @brief Überpüft ob sich ein Barcode in der Datenbank befindet
* @param[in] cstBarcode: Der zu überprüfende Barcode
* @param[in,out] DeliverdPackage: Referenz Rückgabe der Paketinformationen
* @param[in,out] DeliverdReceiver: Referenz Rückgabe der Empfängerinformationen
* @return true = Barcode ist in der Datenbank vorhanden
*/
/**
* @test Vorbedingung: Datenbank muss mit verschiedenen Paketen angelegt sein \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung:  Barcode muss korrekt ausgewertet werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool Database::CheckIfBarcodeIsInDatabase(const std::string cstBarcode, Package& DeliverdPackage, Receiver& DeliverdReceiver)
{
    //! Initialisiert die Datenbankabfrage
    sqlite3* SqliteDatabase;
    sqlite3_stmt* Statement;

    //! Beendet die Transaktion, wenn Datenbank nicht geöffnet werden kann
    if (sqlite3_open(_stWholeDatabasePath.c_str(), &SqliteDatabase) != SQLITE_OK)
    {
        assert(false);

        //! Bereitet einen string für den Systemlog vor
        std::string stLog = "Datenbank " + _cstDatabaseName + " konnte nicht geöffnet werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, stLog);

        return false;
    }

    //! SQL Befehl
    std::stringstream Query;
    Query << "SELECT * FROM Pakete NATURAL JOIN Empfaenger "
        << "WHERE "
        << "Barcode ="
        << cstBarcode
        << ";";

    //! Bereitet die SQL Abfrage vor (c string) und beendet das Programm falls nicht erfolgreich
    sqlite3_prepare(SqliteDatabase, Query.str().c_str(),-1, &Statement, nullptr);

    //! Datenbankabfrage
    sqlite3_step(Statement);

    //! Legt den ReturnValue fest
    bool bReturnValue = false;

    std::stringstream ssBarcode;
    ssBarcode << sqlite3_column_text(Statement, 1);

    //! Wenn ein Barcode gefunden ist, wird der Rückgabewert true (NULL kann kein Barcode sein);
    if (ssBarcode.str() != "")
    {
        //! Initialist alle streams
        std::stringstream ssDelieverer;
        std::stringstream ssContent;
        std::stringstream ssDelieveryState;
        std::stringstream ssDelieverd;
        std::stringstream ssName;
        std::stringstream ssSureName;
        std::stringstream ssMailAdress;

        //! Schreibt die Datenbankabfrage in die Streams
        ssDelieverer << sqlite3_column_text(Statement, 0);
        ssContent << sqlite3_column_text(Statement, 2);
        ssDelieveryState << sqlite3_column_text(Statement, 3);
        ssDelieverd << sqlite3_column_text(Statement, 4);
        ssName << sqlite3_column_text(Statement, 5);
        ssSureName << sqlite3_column_text(Statement, 6);
        ssMailAdress << sqlite3_column_text(Statement, 7);

        //!Weist die Strukturvariablen zu
        DeliverdPackage.stDeliverer = ssDelieverer.str();
        DeliverdPackage.stBarcode = ssBarcode.str();
        DeliverdPackage.stContent = ssContent.str();
        DeliverdPackage.stDeliveryState = ssDelieveryState.str();
        DeliverdPackage.stDelivered = ssDelieverd.str();

        DeliverdReceiver.stBarcode = ssBarcode.str();
        DeliverdReceiver.stName = ssName.str();
        DeliverdReceiver.stSureName = ssSureName.str();
        DeliverdReceiver.stMailAdress = ssMailAdress.str();

        //! Bereitet einen string für den Systemlog vor
        std::string stLog = "Barcode: " + cstBarcode + " wurde in der Datenbank gefunden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

        bReturnValue = true;
    }
    else
    {
        //! Bereitet einen string für den Systemlog vor
        std::string stLog = "Barcode: " + cstBarcode + " wurde nicht in der Datenbank gefunden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

        bReturnValue = false;
    }

    //! Schließt die Sitzung ab
    sqlite3_finalize(Statement);
    sqlite3_close(SqliteDatabase);

    return bReturnValue;
}

/**
* @brief Ändert den Lieferstatus, wenn ein Paket angekommen ist
* @param[in] cstBarcode: Der Barcode des empfangenen Paketes
* @return true = Änderung war erfolgreich
*/
/**
* @test Vorbedingung: Datenbank muss mit nicht gelieferten Paketen angelegt sein \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Paketstatus muss sich geändert haben \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool Database::ChangeDeliveryState(const std::string cstBarcode)
{
    //! Initialisiert die Datenbankabfrage
    sqlite3* SqliteDatabase;
    sqlite3_stmt* Statement1;
    sqlite3_stmt* Statement2;

    //! Beendet die Transaktion, wenn Datenbank nicht geöffnet werden kann
    if (sqlite3_open(_stWholeDatabasePath.c_str(), &SqliteDatabase) != SQLITE_OK)
    {
        assert(false);

        //! Bereitet einen string für den Systemlog vor
        std::string stLog = "Datenbank " + _cstDatabaseName + " konnte nicht geöffnet werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, stLog);

        return false;
    }

    //! SQL Befehl
    std::stringstream Query1;
    Query1 << "UPDATE Pakete SET Lieferstatus = '"
          << "Geliefert am: "
          << Timer.get_mDay() << ". " << Timer.get_month() << " " << Timer.get_Year() << " um " << Timer.get_time() << " Uhr' "
          << "WHERE Barcode = '"
          << cstBarcode << "';";

    //! Bereitet die SQL Abfrage vor (c string) und beendet das Programm falls nicht erfolgreich
    sqlite3_prepare(SqliteDatabase, Query1.str().c_str(),-1, &Statement1, nullptr);

    //! Datenbankabfrage
    sqlite3_step(Statement1);

    std::stringstream Query2;
    Query2 << "UPDATE Pakete SET Zugestellt = '"
          << "1' "
          << "WHERE Barcode = '"
          << cstBarcode << "';";

    //! Bereitet die SQL Abfrage vor (c string) und beendet das Programm falls nicht erfolgreich
    sqlite3_prepare(SqliteDatabase, Query2.str().c_str(),-1, &Statement2, nullptr);

    //! Datenbankabfrage
    sqlite3_step(Statement2);

    //! Schließt die Sitzung ab
    sqlite3_finalize(Statement1);
    sqlite3_finalize(Statement2);
    sqlite3_close(SqliteDatabase);

    //! Bereitet einen String für den Systemlog vor
    std::string stLog = "Lieferstatus des Paketes:" + cstBarcode + " wurde auf geliefert geändert";
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

    return true;
}
/**
* @brief Liefert den Datenbanknamen
* @return gibt den Datenbanknamen zurück
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string Database::get_DatabaseName() const
{
    return _cstDatabaseName;
}

/**
* @brief Liefert den Ordner (Speicherort) der Datenbank
* @return gibt den Speicherort der Datenbank zurück
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string Database::get_DatabaseFolder() const
{
    return _cstDatabaseFolder;
}

/**
* @brief Liefert den kompletten Datenpfad der Datenbank
* @return Datenpfad (mit Datei)
*/
/**
* @test Vorbedingung: Methode ist nicht aufgerufen \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Korrekter Datensatz muss übermittelt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string Database::get_WholeDatabasePath() const
{
    return _stWholeDatabasePath;
}

/**
* @brief Initialisiert alle Klassenvariablen
* @return Kein Rückgabewert
*/
/**
* @test Kein Test notwendig
*/
void Database::initialize()
{
    //! Initialisert den kompletten Datenbankpfad
    _stWholeDatabasePath = _cstDatabaseFolder + _cstDatabaseName;
}
