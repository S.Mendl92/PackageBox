/**
 * @file   Websocketclient.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Februar, 2017
 * @version 1.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse Websocketclient
 *
 * @section Definierte Methoden:
 * @li WebsocketClient();
 * @li ~WebsocketClient();
 * @li int connect(std::string const & uri);
 * @li void close(int id, websocketpp::close::status::value code, std::string reason);
 * @li void send(int id, std::string message);
 * @li Metadata::ptr get_metadata(int id) const;
 */

#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>

#include <websocketpp/common/thread.hpp>
#include <websocketpp/common/memory.hpp>

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <chrono>
#include <thread>

#include "../Libary/Metadata.h"

#ifndef MYWEBSOCKETCLIENT_H_INCLUDED
#define MYWEBSOCKETCLIENT_H_INCLUDED

/**
* @class Websocketclient
* @brief Verwaltet die Websocketkommunikation
*/
class WebsocketClient
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/

    WebsocketClient();
    ~WebsocketClient();

    int connect(std::string const & uri);

    void close(int id, websocketpp::close::status::value code, std::string reason);
    void send(int id, std::string message);

    /****************************************************************
    *                      Get-Methoden                             *
    *****************************************************************/

    Metadata::ptr get_metadata(int id) const;

private:

    typedef std::map<int,Metadata::ptr> con_list;

    /*****************************************************************
    *                  Private Klassenvariablen                     *
    *****************************************************************/

    client WebsocketppClient;
    websocketpp::lib::shared_ptr<websocketpp::lib::thread> m_thread;

    con_list m_connection_list;
    int m_next_id;
};

#endif // MYWEBSOCKETCLIENT_H_INCLUDED
