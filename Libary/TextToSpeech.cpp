/**
 * @file   TextToSpeech.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für TextToSpeech)
 *
 * @section Implementierte Methode:
 * @li TextToSpeech()
 * @li ~TextToSpeech()
 * @li bool CompareDatasets(const bool cbPlayDirectly)
 * @li bool PlayOutputFile(const unsigned int cuiSoundFile)
 * @li string get_Language() const
 * @li string get_ProgramName() const
 * @li string get_GPIOTextFilePath() const
 * @li void get_CreatedFileName(map<int, string>& mapCopiedFileName)
 * @li void get_CreatedFilePath(map<int, string>& mapCopiedFilePath)
 * @li void get_WholeFilePath(map<int, string>& mapCopiedWholeFileName)
 * @li void set_Language(const unsigned int cuiLanguage)
 * @li void initialize()
 * @li void CreateOutputFile(const string cstOutputString)
 * @li void BufferInformationFromSettingsDatabase()
 * @li void IncreaseClassCounter()
 * @li void DeleteAllCreatedFiles()
 * @li void CreateSpeachOutputFile(const bool cbPlayDirectly)
 * @li bool DeleteCreatedSoundFile(const unsigned int cuiSoundFile)
 * @li bool CheckIfSoundFileAreCreated(const unsigned int cuiSoundFile)
 * @li bool CheckUserParameter(const unsigned int cuiSoundFile,const unsigned int cuiLowerBoader,const unsigned int cuiUpperBoader)
 */

#include "../Libary/TextToSpeech.h"

//!< Initialisert die static Klassenvariablen der Klasse TextToSpeech
unsigned long long int TextToSpeech::_ulliCreationalNumber = 0;

/**
* @brief Konstruktoraufruf der Klasse: TextToSpeech
*/
TextToSpeech::TextToSpeech()
{
    //! Initialsiert alle Klassenvariablen
    initialize();
}

/**
* @brief Destruktoraufruf der Klasse: TextToSpeech
*/
TextToSpeech::~TextToSpeech()
{

}

/**
* @brief Vergleicht die aktuellen Datensätze mit den vorhandenen Datensätzen
* @param[in] cbPlayDirectly: true = Soundfile direkt abspielen
* @return true = Keine Änderungen | false = Änderungen, Soundfiles wurden neu generiert
*/
/**
* @test Vorbedingung: Klassenobjekt anlegen (Soundfiles werden erstellt) \n
*       Funktion: Datensatz in Datenbank ändern \n
*       Nachbedinung: Sounfiles müssen neu generiert werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool TextToSpeech::CompareDatasets(const bool cbPlayDirectly)
{
    //! Kopiert die alten Datensätze
    std::map<int, std::string> mapOldDatasets = _mapReceivedInformation;

    //! Holt die neuen Datensätze
    BufferInformationFromSettingsDatabase();

    //! Überprüft alle Datensätze
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        //! Datensätze sind unterschiedlich
        if(mapOldDatasets[uiLoop] != _mapReceivedInformation[uiLoop])
        {
            //! Soundfiles werden neu generiert
            CreateSpeachOutputFile(cbPlayDirectly);
            return false;
        }
    }
    return true;
}

/**
* @brief Erstellt aus den empfangenen Daten der Datenbank die Sprachausgaben
* @param[in] mapDatasets: Empfängt Daten aus der Datenbank
* @param[in] cbPlayDirectly: Play All generated Soundfiles Diretcly
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Klassenobjekt anlegen (Soundfiles werden erstellt) \n
*       Nachbedinung: Sounfiles müssen neu generiert werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/

void TextToSpeech::CreateSpeachOutputFile(const bool cbPlayDirectly)
{
    //! Löscht alle bis dahin erstellten Soundfiles
    DeleteAllCreatedFiles();

    //! Die ersten 7 Soundfiles werden direkt aus der Datenbankgeneriert [36..43]
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber - 2; uiLoop += 1)
    {
        //! Setzt die Progammparameter
        std::string stProgrammControl = _cstProgramName + " " + _stLanguage + " -w " + _mapWholeFileName[uiLoop] + " ";

        //! String beinhaltet den Preperierten Text fürs Terminal
        DetecteInjectionSigns(_mapReceivedInformation[uiLoop]);

        std::string stPreparedText = '\"' + _mapReceivedInformation[uiLoop] + '\"';

        //! Befehl wird ans Terminal weitergeleitet
        std::string stCommandLine = stProgrammControl + stPreparedText;
        int iTerminalState = system(stCommandLine.c_str());

        //! Systemlog wird erstellt
        std::string stLog = "Soundfile " + _mapWholeFileName[uiLoop] + " wurde erstellt";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::NOTICE, stLog);

        std::ignore = iTerminalState;
    }

    //! Die letzten zwei Parameter sind die Zielpfade für Dateien (Es können längere Texte ausgegeben werden)
    for(unsigned int uiLoop = _cuiLowerDatasetNumber + 8; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        //! Hinterlegt den Speicherpfad der Datei
        std::string stWholeSoundFilePath = _mapReceivedInformation[uiLoop];

        std::ifstream file(stWholeSoundFilePath.c_str());            //!öffnet die Datei

        if (!file)
        {
            assert(false);

            //! Bereitet den Kommentar für die Systemlogmeldung vor
            std::string stLog = "Datei: " + stWholeSoundFilePath + " konnte nicht erstellt werden";
            Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, stLog);
            break;
        }

        //! Initialisiert die Variablen
        std::string stReadFile = "";
        std::string stFileText = "";

        //! Lädt jede Zeile aus der ausgewählten TextDatei
        while(getline(file,stReadFile) and (stFileText.size() <= stFileText.max_size()))
        {
            //! Textdatei wird in String geladen
            stFileText = stFileText + stReadFile + " ";
        }

        //! Schließt die Textdatei
        file.close();

        DetecteInjectionSigns(stFileText);

        //! Bereitet die Variablen für den Terminal aufruf vor
        std::string stPreparedFileText = '\"' + stFileText + '\"';
        std::string stProgrammControl = _cstProgramName + " " + _stLanguage + " -w " + _mapWholeFileName[uiLoop] + " ";

        //! Sendet das Kommando ans Terminal
        std::string stCommandLine = stProgrammControl + stPreparedFileText;
        int iTerminalState = system(stCommandLine.c_str());

        //! Bereitet den Kommentar für die Systemlogmeldung vor
        std::string stLog = "Soundfile " + _mapWholeFileName[uiLoop] + " wurde aus Textdatei erstellt";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::NOTICE,stLog);

        std::ignore = iTerminalState;
    }

    //! Spielt die erzeugten Soundfiles ab
    if (cbPlayDirectly)
    {
        for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
        {
            PlayOutputFile(uiLoop);
        }
    }

}

/**
* @brief Spielt eines der gewählten Soundfiles ab
* @param[in] cuiSoundfile: Wählt das abzuspielende Soundfile aus [46-45]
* @return Soundfile wurde erfolgreich abgespielt
*/
/**
* @test Vorbedingung: Klassenobjekt muss angelegt werden \n
*       Funktion: Jedes Soundfile wiedergeben \n
*       Nachbedinung: Soundfiles müssen abgespielt werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool TextToSpeech::PlayOutputFile(const unsigned int cuiSoundFile)
{
    //! Überprüft ob die Benutzereingaben korrekt sind
    if(!CheckUserParameter(cuiSoundFile, _cuiLowerDatasetNumber, _cuiUpperDatasetNumber))
    {
        assert(false);
        return false;
    }

    //! Überprüft ob ein Soundfile schon existiert
    if(!CheckIfSoundFileAreCreated(cuiSoundFile))
    {
        //! Systemlog wird schon in der Methode geschrieben
        assert(false);
        return false;
    }

    //!Initialisiert den String für den Systemlog
    std::string stLog = "";
    std::string stCommandLine = "";

    //! Generiert einen c_string der das Soundfile abspielt
    switch(cuiSoundFile)
    {
    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH1:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH1];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH1] + "wurde abgespielt";
        break;
    }

    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH2:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH2];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH2];
        break;
    }

    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH3:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH3];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH3] + "wurde abgespielt";
        break;
    }

    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH4:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH4];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH4] + "wurde abgespielt";
        break;
    }

    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH5:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH5];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH5] + "wurde abgespielt";
        break;
    }

    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH6:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH6];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH6] + "wurde abgespielt";
        break;
    }

    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH7:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH7];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH7] + "wurde abgespielt";
        break;
    }

    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH8:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH8];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH8] + "wurde abgespielt";
        break;
    }

    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH9:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH9];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH9] + "wurde abgespielt";
        break;
    }

    case DatabaseWrapper::INTERFACE::TEXTTOTPEECH10:
    {
        stCommandLine = "play " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH10];

        //! Systemlogmeldung
        stLog = "Soundfile: " + _mapWholeFileName[DatabaseWrapper::INTERFACE::TEXTTOTPEECH10] + "wurde abgespielt";
        break;
    }

    default:
    {
        assert(false);

        //! Systemlogmeldung
        stLog = "Nicht vorhandenes Soundfile wurde angewählt";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, stLog);
        return false;
    }
    }

    int iTerminalState = system(stCommandLine.c_str());

    Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

    std::ignore = iTerminalState;
    return true;
}

/**
* @brief Löscht alle erzeugten Soundfiles
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Soundfiles müssen erzeugt sein \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Soundfiles müssen gelöscht werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void TextToSpeech::DeleteAllCreatedFiles()
{
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        DeleteCreatedSoundFile(uiLoop);
    }
}

/**
* @brief Gibt die eingestellte Sprachauswahl zurück
* @return die eingestellte Sprachauswahl
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert\n
*       Funktion: vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode\n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string TextToSpeech::get_Language() const
{
    return _stLanguage;
}

/**
* @brief Gibt den verwendeten Programmnamen zurück
* @return der verwendete Programmname
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert \n
*       Funktion: Vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode \n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string TextToSpeech::get_ProgramName() const
{
    return _cstProgramName;
}

/**
* @brief Gibt den verwendeten Pfad zum Textfile zurück [Datensatz 44]
* @return der verwendete Programmname
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert \n
*       Funktion: Vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode \n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string TextToSpeech::get_GPIOTextFilePath() const
{
    return (_mapReceivedInformation.find(DatabaseWrapper::INTERFACE::TEXTTOTPEECH9)->second);
}

/**
* @brief Gibt eine map mit allen Soundfile Namen wird zurück
* @param[in,out] mapCopiedFileName: map mit Dateinamen wird zurückgegeben
* @return Kein Rückgabewert
*/
/**
* @test Testablauf: wurde mit cout auf der Konsole überprüft \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void TextToSpeech::get_CreatedFileName(std::map<int, std::string>& mapCopiedFileName)
{
    mapCopiedFileName = _mapCreatedFileName;
}

/**
* @brief Gibt eine map mit Speicherpfaden zurück
* @param[in,out] mapCopiedFilePath: map mit Speicherpfaden wird zurückgegeben
* @return Kein Rückgabewert
*/
/**
* @test Testablauf: wurde mit cout auf der Konsole überprüft \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void TextToSpeech::get_CreatedFilePath(std::map<int, std::string>& mapCopiedFilePath)
{
    mapCopiedFilePath = _mapCreatedFilePath;
}

/**
* @brief Gibt eine map mit allen kompletten Soundpfaden zurück
* @param[in,out] mapCopiedWholeFilename: map mit Soundpfaden wird zurückgegeben
* @return Kein Rückgabewert
*/
/**
* @test Testablauf: wurde mit cout auf der Konsole überprüft
*       Status: bestanden
*/
void TextToSpeech::get_WholeFilePath(std::map<int, std::string>& mapCopiedWholeFileName)
{
    mapCopiedWholeFileName = _mapWholeFileName;
}

/**
* @brief Setzt die Ausgangssprache des TTS Moduls
* @param[in] cuiLanguage: Sprachauswahl [0..4]
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Klassenobjekt wurde erstellt \n
*       Funktion: Methodenauruf mit alle switch - Case Parameter [0..7] und Soundfile generieren + abspielen \n
*       Nachbedinung: Sprachausgabe muss sich verändern \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void TextToSpeech::set_Language(const unsigned int cuiLanguage)
{
    //! Wählt anhand des unsingend Integers die Ausgangsspräche aus
    //! Default ist Deutsch
    switch(cuiLanguage)
    {
    case LANGUAGE::ENGLISH:
        _stLanguage = "--lang en-EN";
        break;
    case LANGUAGE::FRENCH:
        _stLanguage = "--lang fr-FR";
        break;
    case LANGUAGE::GERMAN:
        _stLanguage = "--lang de-DE";
        break;
    case LANGUAGE::ITALIAN:
        _stLanguage = "--lang it_IT";
        break;
    case LANGUAGE::SPANISH:
        _stLanguage = "--lang sp-SP";
        break;
    default:
        assert(false);
        _stLanguage = "--lang de-DE";
        break;
    }

}

/**
* @brief Initialisiert alle Variablen der Klasse TextToSpeech
* @return Kein Rückgabewert
*/
/**
* @test Test wurde durch Get-Mehtoden erledigt
*/
void TextToSpeech::initialize()
{
    //! Setzt die Ausgangssprache
    set_Language(LANGUAGE::GERMAN);

    //! Schreibt alle Relevanten Daten in die maps. Parameter läuft von 36..45
    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        //!Übergibt die Anzahl der Schleifendurchläuf: Bsp: 45-45+1 = 1 (erster Durchlauf)
        unsigned int uiFileCounter = uiLoop - _cuiLowerDatasetNumber + 1;

        //! Beinhaltet den Namen des zu erstellenden Soundfils
        std::stringstream ssFileNameBuffer;
        ssFileNameBuffer << "SpeachOutputFile" << uiFileCounter << ".wav";

        //!Beinhaltet den Speicherpfad + die komplette Adressierung der Sound Dateien
        std::string stWholeFilePathBuffer = _cstFilePathBuffer + ssFileNameBuffer.str();

        //!Übergibt die Parameter an die einzelnen maps
        _mapCreatedFileName[uiLoop] = ssFileNameBuffer.str();
        _mapCreatedFilePath[uiLoop] = _cstFilePathBuffer;
        _mapWholeFileName[uiLoop] = stWholeFilePathBuffer;
    }

    //! Beim ersten Klassenaufruf werden die Soundfiles erstellt
    if (_ulliCreationalNumber == 0)
    {
        BufferInformationFromSettingsDatabase();
        CreateSpeachOutputFile(false);
    }

    //! Erhöht die static Variable: ulliCreationalNumber um 1
    IncreaseClassCounter();
}

/**
* @brief Löscht eines der erstellten Soundfiles
* @param[in] cuiSoundfile: Wählt das zu löschende Soundfile aus [41-45]
* @return true = löschen des Soundfiles war erfolgreich
*/
/**
* @test Vorbedingung: Es läuft momentan kein Soundfiel \n
*       Funktion: Methode aufrufen \n
*       Nachbedinung: Sound Ordner enthält keine Dateien \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool TextToSpeech::DeleteCreatedSoundFile(const unsigned int cuiSoundFile)
{
    //! Soundfiles existieren nicht
    if(!CheckUserParameter(cuiSoundFile, _cuiLowerDatasetNumber, _cuiUpperDatasetNumber))
    {
        return false;
    }

    if(!CheckIfSoundFileAreCreated(cuiSoundFile))
    {
        //! Bereitet den Kommentar für den Systemlog vor
        std::string stLog = "Soundfile " + _mapWholeFileName[cuiSoundFile] + " existiert nicht";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);

        return false;
    }

    //! stCommandLine beinhaltet den string für das Terminal
    std::string stCommandLine = "rm " + _mapWholeFileName[cuiSoundFile];
    int iTerminalState = system(stCommandLine.c_str());

    //! Bereitet den Kommentar für den Systemlog vor
    std::string stLog = "File: " + _mapWholeFileName[cuiSoundFile] + " wurde erfolgreich gelöscht";
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

    std::ignore = iTerminalState;
    return true;
}

/**
* @brief Überprüft ob ein Soundfile existiert
* @param[in] cuiSoundfile: Wählt das zu Soundfile aus [36-45]
* @return true = Soundfiles existieren
*/
/**
* @test Vorbedingung: Soundfiles müssen erstellt sein \n
*       Funktion: Mehtode aufrufen \n
*       Nachbedinung: Rückgabewert muss true sein und Soundfiles dürfen sich nicht verändern \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
/**
* @test Vorbedingung: Soundfiles müssen gelöscht sein \n
*       Funktion: Methodenaufruf \n
*       Nachbedinung: Rückgabewert ist false und Soundfiles dürfen nicht erstellt werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool TextToSpeech::CheckIfSoundFileAreCreated(const unsigned int cuiSoundFile)
{
    //!Übergiebt den gespeicherten Datenpfad an die interne Variable
    const std::string cstSoundFilePath = _mapWholeFileName[cuiSoundFile];

    //!Öffnet das Soundfile
    FILE* SoundFile = fopen(cstSoundFilePath.c_str(),"rb");

    //!Überprüft ob Soundfile vorhanden
    return (SoundFile);
}

/**
* @brief Überprüft ob ein Parameter innerhalb der Intervallgrenzen liegt
* @param[in] cuiSoundfile: Der zu überprüfende Parameter
* @param[in] cuiLowerBoader: untere Intervallgrenze (einschließlich)
* @param[in] cuiUpperBoader: obere Intervallgrenze (einschließlich)
* @return true = Benutzereingaben waren korrekt
*/
/**
* @test Vorbedingung: Paramerter übergeben (richtige Grenzen) \n
*       Funktion: Methode Aufrufen \n
*       Nachbedinung: Rückgabewert = true \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
/**
* @test Vorbedingung: Paramerter übergeben (falsche Grenzen) \n
*       Funktion: Methode Aufrufen \n
*       Nachbedinung: Rückgabewert = false \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool TextToSpeech::CheckUserParameter(const unsigned int cuiSoundFile, const unsigned int cuiLowerBoader,
                                       const unsigned int cuiUpperBoader)
{
    //!Es wurde ein Falscher Userparameter übergeben < uiLowerBoader oder > uiUpperBoader
    if(((cuiSoundFile < cuiLowerBoader) or (cuiSoundFile > cuiUpperBoader)))
    {
        assert(false);

        std::string stLog = "Falsche Benutzerparameter wurde übergeben Klasse: TextToSpecech Methode: CheckUserParameter";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);

        return false;
    }

    return true;
}

/**
* @brief Liest die Daten aus der Settings Datenbank und speichert sie in der mapReceivedInformation ab
* @return Kein Rückgabewert
*/
/**
* @test Wird mit Klassentest der Klasse CompareDatasets() überprüft
* @see CompareDatasets()
*/
void TextToSpeech::BufferInformationFromSettingsDatabase()
{

    for(unsigned int uiLoop = _cuiLowerDatasetNumber; uiLoop <= _cuiUpperDatasetNumber; uiLoop += 1)
    {
        //! Struktur zum zwischenspeichern, der Empfangen Daten
        Settings PreBuffer;

        //! Empfängt Informationen von der Datenbank
        DatabaseWrapper::ReadInformationFromDatabase(uiLoop,PreBuffer);

        //! Leitet diese weiter an die Klassenvariable
        _mapReceivedInformation[uiLoop] = PreBuffer.stDatasetValue;
    }
}

/**
* @brief Erhöht die Anzahl der erzeugten Klasseninstanzen um 1
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Es wurde keine Klasse erzeugt \n
*       Funktion: 100 Klassenobjekte anlegen \n
*       Nachbedinung: Wert ulliCreationalNumber = 100 muss erfüllt sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void TextToSpeech::IncreaseClassCounter()
{
    _ulliCreationalNumber += 1;
}
