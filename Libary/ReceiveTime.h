/**
 * @file   ReceiveTime.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Dezember, 2016
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse ReceiveTime
 *
 * @section Definierte Methoden:
 * @li ReceiveTime()
 * @li ~ReceiveTime()
 * @li string get_time()
 * @li string get_wDay()
 * @li string get_month()
 * @li unsigned int get_mDay()
 * @li unsigned int get_Year()
 * @li unsigned int get_hour()
 * @li void initialize()
 * @li void getLocalTime()
 * @li bool lessThan10(const int ciValue) const
 */

#include <string>
#include <sstream>
#include <ctime>
#include <cassert>

#ifndef RECEIVETIME_H_INCLUDED
#define RECEIVETIME_H_INCLUDED

/**
* @class ReceiveTime
* @brief Liest die aktuelle Systemzeit aus und bereitet diese für darauffolgende Prozesse vor
* @see http://www.cplusplus.com/reference/ctime/tm/
* @see http://www.cplusplus.com/reference/ctime/strftime/
*
* @section Aufgaben
* @li Bei jedem public-Methodenaufruf wird die Systemzeit neu ausgelesen und aufbereitet
* @li Es wird die aktuelle Uhrzeit im Format 24:60:60 ausgegeben (string)
* @li Es wird der Wochentag ausgegeben
* @li Es wird der Monat ausgegebn
* @li Es wird der Monatstag ausgegen
* @li Es wird das Jahr ausgegeb
* @li Es wird die aktuelle Stunde ausgegebn
*/

class ReceiveTime
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    ****************************************************************/

    ReceiveTime();
    ~ReceiveTime();

    std::string get_time();
    std::string get_wDay();
    std::string get_month();

    unsigned int get_mDay();
    unsigned int get_Year();
    unsigned int get_hour();

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_ReceiveTime;

private:
    /*****************************************************************
    *                    Private Methoden                           *
    *****************************************************************/

    void getLocalTime();

    /*****************************************************************
    *                  Private Klassenvariablen                     *
    ****************************************************************/

    const unsigned int _cuiBufferSize = 35;
    struct tm* _TimeStamp;
};
#endif // RECEIVETIME_H_INCLUDED
