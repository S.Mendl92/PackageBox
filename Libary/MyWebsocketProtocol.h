/**
 * @file   MyWebsocketProtocol.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse MyWebsocketProtocol
 *
 * @section Definierte Methoden:
 * @li MyWebsocketProtocol()
 * @li ~MyWebsocketProtocol()
 * @li string ConvertForWebsocket(const unsigned int cuiProtocolCode,const bool cbSendState)
 * @li string SelectProtocolCode(const unsigned int cuiProtocolCode)
 */

#include <string>
#include <sstream>
#include <cassert>

#include "../Libary/Converter.h"

#ifndef MYWEBSOCKETPROTOCOL_H_INCLUDED
#define MYWEBSOCKETPROTOCOL_H_INCLUDED

/**
* @class MyWebsocketProtocol
* @brief Bereitet einen Status (true / false) für die Websocketkommunikation vor
*
* @section Aufgaben
* @li Wählt anhand der MYWEBSOCKETPROTOCOL_XXX Nummer die Zeichenergänzung aus und
*     und fügt sie zum gesendeten Status hinzu.
* @li Der Protokollcode wird in der Klasse SelectProtocolCode(unsigned int uiProtocolCode)
*     ausführlich Beschrieben, besteht aber immer aus 2 elementaren Teilen: \n
*     XXXX_S -> Aufbau des Kodierten Signals \n
*     XXXX ist der übermittelte Code zur Bestimmung des Datensatzes \n
*     S Stellt den Status dar 1 = true | 0 = false
* @see SelectProtocolCode(const unsigned int cuiProtocolCode)
*/
class MyWebsocketProtocol : protected Converter
{
public:
    /*****************************************************************
    *                      Public-Methoden                           *
    *****************************************************************/

    MyWebsocketProtocol();
    ~MyWebsocketProtocol();

    std::string ConvertForWebsocket(const unsigned int cuiProtocolCode,const bool cbSendState);

    /*****************************************************************
    *                      enum - Deklaration                        *
    *****************************************************************/

    //! @enum definiert den Übertragungscode
    enum PROTOCOLCODE
    {
        DOOR = 1,              //!< Übertragungsprotokolcode: 0001
        ERRORLED = 2,          //!< Übertragungsprotokolcode: 0010
        SCANNERBUTTON = 3,     //!< Übertragungsprotokolcode: 0011
        ERRORBUTTON = 4,       //!< Übertragungsprotokolcode: 0100
        WORKINGSTATE = 5      //!< Übertragungsprotokolcode: 0101
    };

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_MyWebsocketProtocol;

private:
    /*****************************************************************
    *                    Private Methoden                           *
    *****************************************************************/

    std::string SelectProtocolCode(unsigned int uiProtocolCode);
};
#endif // MYWEBSOCKETPROTOCOL_H_INCLUDED
