/**
 * @file   TextToSpeech.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse TextToSpeech
 *
 * @section Definierte Methoden:
 * @li TextToSpeech()
 * @li ~TextToSpeech()
 * @li bool CompareDatasets(const bool cbPlayDirectly)
 * @li bool PlayOutputFile(const unsigned int cuiSoundFile)
 * @li string get_Language() const
 * @li string get_ProgramName() const
 * @li string get_GPIOTextFilePath() const
 * @li void get_CreatedFileName(map<int, string>& mapCopiedFileName)
 * @li void get_CreatedFilePath(map<int, string>& mapCopiedFilePath)
 * @li void get_WholeFilePath(map<int, string>& mapCopiedWholeFileName)
 * @li void set_Language(const unsigned int cuiLanguage)
 * @li void initialize()
 * @li void CreateOutputFile(const string cstOutputString)
 * @li void BufferInformationFromSettingsDatabase()
 * @li void IncreaseClassCounter()
 * @li void DeleteAllCreatedFiles()
 * @li void CreateSpeachOutputFile(const bool cbPlayDirectly)
 * @li bool DeleteCreatedSoundFile(const unsigned int cuiSoundFile)
 * @li bool CheckIfSoundFileAreCreated(const unsigned int cuiSoundFile)
 * @li bool CheckUserParameter(const unsigned int cuiSoundFile,const unsigned int cuiLowerBoader,const unsigned int cuiUpperBoader)
 */

#include <string>
#include <map>
#include <sstream>
#include <fstream>
#include <cstdlib>

#include "../Libary/Systemlog.h"
#include "../Libary/Converter.h"
#include "../Libary/StructSettings.h"
#include "../Libary/DatabaseWrapper.h"

#ifndef TEXTTOSPEECH_H_INCLUDED
#define TEXTTOSPEECH_H_INCLUDED

/**
* @class TextToSpeech
* @brief Verwaltet die Interaktion mit dem Sound Ausgabe Modul
*
* @section Aufgaben
* @li Bei der ersten Instanzierung werden automtisch die Soundfiles erzeugt (Datenbankabfrage)
*     Jede weiter Instanzierung muss zuvor überprüfen, ob ein Benutzer eingeloggt ist und
*     erzeugt anschließend die Soundfiles (falls es Änderungen gab)
* @li Es kann überprüft werden, ob sich die Datensätze in der Datenbank geändert haben.
* @li Die Sprache kann geändert werden (auf die vorinstallierten Sprachen des Soundmoduls)
* @li Die erzeugten Soundfiles können abgespielt werden
*     Darauf hin werden die Soundfiles neu generiert (Zykluszeit der main dauert dadurch länger)
* @li Name, Odnerpfad und komplette Adressierung aller verwendeten (externen) Datei
*     sollen aufrufbar sein
*/
class TextToSpeech : protected DatabaseWrapper , protected Converter
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/
    TextToSpeech();
    ~TextToSpeech();

    bool CompareDatasets(const bool cbPlayDirectly);
    bool PlayOutputFile(const unsigned int cuiSoundFile);

    /****************************************************************
    *                      Get-Methoden                             *
    *****************************************************************/

    std::string get_Language() const;
    std::string get_ProgramName() const;
    std::string get_GPIOTextFilePath() const;

    void get_CreatedFileName(std::map<int, std::string>& mapCopiedFileName);
    void get_CreatedFilePath(std::map<int, std::string>& mapCopiedFilePath);
    void get_WholeFilePath(std::map<int, std::string>& mapCopiedWholeFileName);

    /****************************************************************
    *                      Set-Methoden                             *
    *****************************************************************/
    void set_Language(const unsigned int cuiLanguage);

    /*****************************************************************
    *                      enum - Deklaration                        *
    *****************************************************************/

    //! @enum Definiert die Sprachen
    enum LANGUAGE {ENGLISH = 0,   //!< Englisch
                   FRENCH = 1,    //!< Französisch
                   GERMAN = 2,    //!< Deutsch
                   ITALIAN = 3,   //!< Italienisch
                   SPANISH = 4
                  };  //!< Spanisch

    //! @enum Definiert die einzelnen Soundfiles
    enum SOUNDFILE {WELCOME = DatabaseWrapper::INTERFACE::TEXTTOTPEECH1,
                    PACKAGERECEIVEDREGULARE = DatabaseWrapper::INTERFACE::TEXTTOTPEECH2,
                    FALSEBARCODE = DatabaseWrapper::INTERFACE::TEXTTOTPEECH3,
                    BARCODEISNOTSCANNABLE = DatabaseWrapper::INTERFACE::TEXTTOTPEECH4,
                    ERRORMODE = DatabaseWrapper::INTERFACE::TEXTTOTPEECH5,
                    ERRORMODEFALSEBARCODE = DatabaseWrapper::INTERFACE::TEXTTOTPEECH6,
                    DELIVERYPLACE = DatabaseWrapper::INTERFACE::TEXTTOTPEECH7,
                    FALSEWORKINGMODE = DatabaseWrapper::INTERFACE::TEXTTOTPEECH8,
                    OWNSOUNDFILE9 = DatabaseWrapper::INTERFACE::TEXTTOTPEECH9,
                    OWNSOUNDFILE10 = DatabaseWrapper::INTERFACE::TEXTTOTPEECH10
                   };

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_TextToSpeech;

private:
    /*****************************************************************
    *                    Private Methoden                           *
    *****************************************************************/

    void initialize();
    void BufferInformationFromSettingsDatabase();
    void IncreaseClassCounter();
    void DeleteAllCreatedFiles();
    void CreateSpeachOutputFile(const bool cbPlayDirectly);

    bool DeleteCreatedSoundFile(const unsigned int cuiSoundFile);
    bool CheckIfSoundFileAreCreated(const unsigned int cuiSoundFile);
    bool CheckUserParameter(const unsigned int cuiSoundFile,const unsigned int cuiLowerBoader,
                            const unsigned int cuiUpperBoader);

    /*****************************************************************
    *                  Private Klassenvariablen                     *
    *****************************************************************/

    static unsigned long long int _ulliCreationalNumber;                                    //! Anzahl der erzeugten Instanzen

    const std::string _cstProgramName = "pico2wave";                                        //! Name des verwendeten Programms
    const std::string _cstFilePathBuffer = "./Paketbox/02_Sound/";                         //! Kompletter FilePfad

    const unsigned int _cuiLowerDatasetNumber = DatabaseWrapper::INTERFACE::TEXTTOTPEECH1; //! Untere Grenze der Datenbankabfrage
    const unsigned int _cuiUpperDatasetNumber = DatabaseWrapper::INTERFACE::TEXTTOTPEECH10;//! Obere Grenze der Datenbankabfrage

    std::string _stLanguage;                                                                //! Beinhaltet die aktuelle Sprache

    std::map<int, std::string> _mapReceivedInformation;
    std::map<int, std::string> _mapCreatedFileName;
    std::map<int, std::string> _mapCreatedFilePath;
    std::map<int, std::string> _mapWholeFileName;

    Systemlog Logger;
};
#endif // TEXTTOSPEECH_H_INCLUDED
