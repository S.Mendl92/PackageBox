/**
 * @file   Systemlog.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Dezember, 2016
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse Systemlog
 *
 * @section Definierte Methoden:
 * @li Systemlog()
 * @li ~Systemlog()
 * @li bool WriteSystemLogInDB(const unsigned int cuiErrorclass,const string cstLogComment)
 * @li string get_DatabaseName() const
 * @li string get_DatabasePath() const
 * @li string get_WholeDatabasePath() const
 * @li void initialize()
 * @li string SelectErrorClassCode(const unsigned int cuiErrorClassCode)
 */

#include <string>
#include <sstream>
#include <cassert>
#include <sqlite3.h>

#include "../Libary/ReceiveTime.h"

#ifndef SYSTEMLOG_H_INCLUDED
#define SYSTEMLOG_H_INCLUDED

/**
* @class Systemlog
* @brief Schreibt die Systemlog Meldungen in die Datenbank
*
* @section Aufgaben
* @li Schreiben der Systemlogmeldungen
* @li Name, Odnerpfad und komplette Adressierung aller verwendeten (externen) Datei
*     sollen aufrufbar sein
*/
class Systemlog
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/

    Systemlog();
    ~Systemlog();

    bool WriteSystemLogInDB(const unsigned int cuiErrorclass,const std::string cstLogComment);

    /****************************************************************
    *                      Get-Methoden                             *
    ****************************************************************/

    std::string get_DatabaseName() const;
    std::string get_DatabasePath() const;
    std::string get_WholeDatabasePath() const;

    /*****************************************************************
    *                      enum - Deklaration                        *
    *****************************************************************/

    //! @enum Definiert die Art der Fehlermeldung
    enum COMMENT {EMERGENCY = 0,       //!< Signalisiert, das ein Notfall eingetreten ist
                  ALERT = 1,           //!< Signalisiert, das ein Alarm eingetreten ist
                  CRITICAL = 2,        //!< Signalisiert, das ein kritisches Eregnis eingetreten ist
                  ERROR = 3,           //!< Signalisiert, das ein Fehler eingetreten ist
                  WARNING = 4,         //!< Signalisiert, das eine Warnung eingetreten ist
                  NOTICE = 5,          //!< Signalisiert die Notiz
                  INFORMATIONAL = 6,   //!< Allgemeine Informationen
                  DEBUG = 7            //!< Meldungen während des Debuggings
                 };

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_Systemlog;

private:
    /*****************************************************************
    *                    Private Methoden                           *
    *****************************************************************/

    void initialize();
    std::string SelectErrorClassCode(const unsigned int cuiErrorClassCode);

    /*****************************************************************
    *                  Private Klassenvariablen                     *
    *****************************************************************/

    const std::string _cstDatabaseName = "Systemlog.db";                 //! Name der verwendeten Datenbank
    const std::string _cstDatabasePath = "./Paketbox/00_Databases/";    //! Ordnerpfad

    std::string _stWholeDatabasePath;                                   //! kompletter Datepfad

    ReceiveTime Time;
};
#endif // SYSTEMLOG_H_INCLUDED
