/**
 * @file   Websocketclient.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Februar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden der Klasse Websocketclient
 *
 * @section Definierte Methoden:
 * @li WebsocketClient();
 * @li ~WebsocketClient();
 * @li int connect(std::string const & uri);
 * @li void close(int id, websocketpp::close::status::value code, std::string reason);
 * @li void send(int id, std::string message);
 * @li Metadata::ptr get_metadata(int id) const;
*/

#include "../Libary/WebsocketClient.h"

/**
* @brief Konstruktoraufruf
*/
WebsocketClient::WebsocketClient()
{
    m_next_id = 0;

    WebsocketppClient.clear_access_channels(websocketpp::log::alevel::all);
    WebsocketppClient.clear_error_channels(websocketpp::log::elevel::all);

    WebsocketppClient.init_asio();
    WebsocketppClient.start_perpetual();

    m_thread = websocketpp::lib::make_shared<websocketpp::lib::thread>(&client::run, &WebsocketppClient);
}

/**
* @brief Destruktoraufruf
*/
WebsocketClient::~WebsocketClient()
{
    WebsocketppClient.stop_perpetual();

    for (con_list::const_iterator it = m_connection_list.begin(); it != m_connection_list.end(); ++it)
    {
        if (it->second->get_status() != "Open")
        {
            continue;
        }

        std::cout << "> Closing connection " << it->second->get_id() << std::endl;

        websocketpp::lib::error_code ec;
        WebsocketppClient.close(it->second->get_hdl(), websocketpp::close::status::going_away, "", ec);
        if (ec)
        {
            std::cout << "> Error closing connection " << it->second->get_id() << ": "
                      << ec.message() << std::endl;
        }
    }

    m_thread->join();
}

/**
* @brief Stellt eine Verbindung zum Websocketserver her
* @param[in] uri: URL zum Websocketserver
* @return Verbindungs ID
*/
int WebsocketClient::connect(std::string const & uri)
{
    websocketpp::lib::error_code ec;

    client::connection_ptr con = WebsocketppClient.get_connection(uri, ec);

    if (ec)
    {
        std::cout << "> Connect initialization error: " << ec.message() << std::endl;
        return -1;
    }

    int new_id = m_next_id++;
    Metadata::ptr metadata_ptr = websocketpp::lib::make_shared<Metadata>(new_id, con->get_handle(), uri);
    m_connection_list[new_id] = metadata_ptr;

    con->set_open_handler(websocketpp::lib::bind(
                              &Metadata::on_open,
                              metadata_ptr,
                              &WebsocketppClient,
                              websocketpp::lib::placeholders::_1
                          ));
    con->set_fail_handler(websocketpp::lib::bind(
                              &Metadata::on_fail,
                              metadata_ptr,
                              &WebsocketppClient,
                              websocketpp::lib::placeholders::_1
                          ));
    con->set_close_handler(websocketpp::lib::bind(
                               &Metadata::on_close,
                               metadata_ptr,
                               &WebsocketppClient,
                               websocketpp::lib::placeholders::_1
                           ));
    con->set_message_handler(websocketpp::lib::bind(
                                 &Metadata::on_message,
                                 metadata_ptr,
                                 websocketpp::lib::placeholders::_1,
                                 websocketpp::lib::placeholders::_2
                             ));

    WebsocketppClient.connect(con);

    std::chrono::seconds WaitTime(5);
    std::this_thread::sleep_for(WaitTime);

    return new_id;
}

/**
* @brief Schließt eine Verbindung zum Websocketserver
* @param[in] id: Verbindungsid
* @param[in] code: Schlusscode
* @param[in] reason: Grund
* @return Kein Rückgabewert
*/
void WebsocketClient::close(int id, websocketpp::close::status::value code, std::string reason)
{
    websocketpp::lib::error_code ec;

    con_list::iterator metadata_it = m_connection_list.find(id);
    if (metadata_it == m_connection_list.end())
    {
        std::cout << "> No connection found with id " << id << std::endl;
        return;
    }

    WebsocketppClient.close(metadata_it->second->get_hdl(), code, reason, ec);
    if (ec)
    {
        std::cout << "> Error initiating close: " << ec.message() << std::endl;
    }
}

/**
* @brief Sendet eine Nachricht zum Websocketserver
* @param[in] id: Verbindungsid
* @param[in] message: Die zu übermittelnde Nachricht
* @return Kein Rückgabewert
*/
void WebsocketClient::send(int id, std::string message)
{
    websocketpp::lib::error_code ec;

    con_list::iterator metadata_it = m_connection_list.find(id);
    if (metadata_it == m_connection_list.end())
    {
        std::cout << "> No connection found with id " << id << std::endl;
        return;
    }

    WebsocketppClient.send(metadata_it->second->get_hdl(), message, websocketpp::frame::opcode::text, ec);
    if (ec)
    {
        std::cout << "> Error sending message: " << ec.message() << std::endl;
        return;
    }

    metadata_it->second->record_sent_message(message);
}

/**
* @brief Empfängt Metadatapointer
* @param[in] id: Verbindungsid
* @return Metadatapointer
*/
Metadata::ptr WebsocketClient::get_metadata(int id) const
{
    con_list::const_iterator metadata_it = m_connection_list.find(id);
    if (metadata_it == m_connection_list.end())
    {
        return Metadata::ptr();
    }
    else
    {
        return metadata_it->second;
    }
}


