/**
 * @file   MyWebsocketProtocol.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für MyWebsocketProtocol)
 *
 * @section Implementierte Methoden:
 * @li MyWebsocketProtocol()
 * @li ~MyWebsocketProtocol()
 * @li string ConvertForWebsocket(const unsigned int cuiProtocolCode,const bool cbSendState)
 * @li string SelectProtocolCode(const unsigned int cuiProtocolCode)
 */

#include "../Libary/MyWebsocketProtocol.h"

/**
* @brief Konstruktoraufruf der Klasse MyWebsocketProtocol
*/
MyWebsocketProtocol::MyWebsocketProtocol()
{

}

/**
* @brief Destruktoraufruf der Klasse MyWebsocketProtocol
*/
MyWebsocketProtocol::~MyWebsocketProtocol()
{

}

/**
* @brief Bereite einen Bool für den Websockettransfer (senden) vor
* @param[in] cuiProtocolCode: Die zu übermittelnde Bytesequenz (MYWEBSOCKETPROTOCOL_... nutzen)
*                            0 = 0000 (Nicht Definiert)
*                            1 = 0001 (Spezifiziert die Tür)
*                            2 = 0010 (Spezifiziert die Fehler Led)
*                            3 = 0011 (Spezifiziert den Scanner Taster)
*                            4 = 0100 (Spezifieziert den Fehler Taster)
*                            5 = 0101 (Spezifiziert den Zustand der Paketbox)
* @param[in] cbSend: Der zu übermittlende Status
* @return Der Protokollformatierte String
*/
/**
* @test Vorbedingung: Paketbox initialisieren \n
*       Funktion: Daten an Websocketserver schicken \n
*       Nachbedinung: Daten müssen korrekt angekommen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string MyWebsocketProtocol::ConvertForWebsocket(const unsigned int cuiProtocolCode, const bool cbSendState)
{
    //! Stringstream zu konvertieren wird initialisiert
    const std::string cstSend = ConvertBoolToString(cbSendState);

    //! Bereitet den Rückgabestring vor
    const std::string cstReturn = SelectProtocolCode(cuiProtocolCode) + cstSend;
    return cstReturn;
}

/**
* @brief Wählt eine Bytesequenz aus
* @param[in] cuiProtocolType: Die auszuwählende Bytesequenze
* @return Der ausgewählte Protcolcode
*/
/**
* @test @see ConvertForWebsocket(unsigned int uiProtocolCode,  bool bSend)
*/
std::string MyWebsocketProtocol::SelectProtocolCode(const unsigned int cuiProtocolType)
{
    //! Initialisiert den string für den Rückgabecode
    std::string stProtocolCode = "";

    //! Wählt den Bytecode aus
    switch (cuiProtocolType)
    {
    case MyWebsocketProtocol::PROTOCOLCODE::DOOR:
        stProtocolCode = "0001";
        break;
    case MyWebsocketProtocol::PROTOCOLCODE::ERRORLED:
        stProtocolCode = "0010";
        break;
    case MyWebsocketProtocol::PROTOCOLCODE::SCANNERBUTTON:
        stProtocolCode = "0011";
        break;
    case MyWebsocketProtocol::PROTOCOLCODE::ERRORBUTTON:
        stProtocolCode = "0100";
        break;
    case MyWebsocketProtocol::PROTOCOLCODE::WORKINGSTATE:
        stProtocolCode = "0101";
        break;
    default:
        assert(false);
        stProtocolCode = "0000";
        break;
    }

    //! Gibt den Bytecode mit _ zurück
    return stProtocolCode + "_";
}
