/**
 * @file   StructSettings.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 1.0
 *
 * @struct enthält die Definition der Struktur Settings
 */

#include <string>

#ifndef STRUCTSETTINGS_H_INCLUDED
#define STRUCTSETTINGS_H_INCLUDED

/**
* @brief Beinhaltet die Datenstruktur nach einer Datenbankabfrage für die Einstellungsdatenbank
*/
using Settings = struct SettingsTemplate//!Settings
{
    std::string stDatasetNumber;     //!< Datensatz Nummer
    std::string stDatasetType;       //! <Datensatz Typ
    std::string stDatasetValue;      //!< Hinterlegter Wert
    std::string stDatasetComment;    //!< Kommentar des Datensatzes

    ~SettingsTemplate() {}      //!< Destruktoraufruf

};
#endif // STRUCTSETTINGS_H_INCLUDED
