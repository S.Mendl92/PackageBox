/**
 * @file   Usermanagement.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse UserManagement
 * @section Definierte Methoden:
 * @li bool CheckUserLoginState()
 * @li bool CheckWorkingState()
 * @li bool HandleTargetSystemByWebsite()
 * @li void initialize()
 * @li string BufferLoginInformationFromSettingsDatabase(const unsigned int cuiDatasetNumber)
 */

#include <string>
#include <map>

#include "../Libary/StructSettings.h"

#include "../Libary/Systemlog.h"
#include "../Libary/DatabaseWrapper.h"
#include "../Libary/Converter.h"
#include "../Libary/ReceiveTime.h"

#ifndef USERMANAGEMENT_H_INCLUDED
#define USERMANAGEMENT_H_INCLUDED

/**
* @class UserManagement
* @brief Verwaltet die Userinteraktion
*
* @section Aufgaben
* @li Überprüft ob der User angemeldet (Datenbankabfrage)
* @li Überprüft den Bereitschaftsmodus der Paketbox (Datenbankabfrage)
* @li Fährt das Zielsystem herunter
* @li Veranlasst das Zielsystem zum Neustart
* @li Name, Odnerpfad und komplette Adressierung aller verwendeten (externen) Datei
*     sollen aufrufbar sein
*/
class UserManagement : protected DatabaseWrapper , protected Converter
{
public:
    /*****************************************************************
     *                      Public-Methoden                          *
     *****************************************************************/

    UserManagement();
    ~UserManagement();

    bool CheckUserLoginState();
    bool CheckWorkingState();
    bool HandleTargetSystemByWebsite();

    /*****************************************************************
    *                      enum - Deklaration                        *
    *****************************************************************/

    //! @enum Enthält die Kommandos für das Zielsystem
    enum TARGETCONTROL { DONOTHING = 0,
                         SHUTDOWN = 1,
                         RESTART = 2
                       };

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_UserManagement;

private:
    /*****************************************************************
     *                    Private Methoden                           *
     *****************************************************************/
    void initialize();
    std::string BufferLoginInformationFromSettingsDatabase(const unsigned int cuiDatasetNumber);

    /****************************************************************
    *                  Private Klassenvariablen                     *
    *****************************************************************/

    static bool _bLastLoginState;                                                                   //! Merker für den Loginstatus des letzten Zykluses
    const unsigned int _cuiDatasetNumberHandleCode = DatabaseWrapper::INTERFACE::HANDLECODE;        //! Beinhaltet den Controllcode für den Rasberry PI
    const unsigned int _cuiDatasetNumberLoginState = DatabaseWrapper::INTERFACE::USERLOGINSTATE;    //! Beinhaltet den verwendeten Datensatz(Userlogin)
    const unsigned int _cuiLowerDatasetNumberTimeHandler = DatabaseWrapper::INTERFACE::STATESUNDAY; //! AnfangsDatensatz
    const unsigned int _cuiUpperDatasetNumberTimeHandler = DatabaseWrapper::INTERFACE::HOURS24;     //! EndDatensatz

    std::map<int, std::string> _mapWDays;  //! Hinterlegt jeden Wochentag

    Systemlog Logger;
    ReceiveTime Timer;
};
#endif // USERMANAGEMENT_H_INCLUDED
