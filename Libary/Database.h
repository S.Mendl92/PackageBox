/**
 * @file   Database.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse Database
 *
 * @section Definierte Methoden:
 * @li Database()
 * @li ~Database()
 * @li bool CheckIfBarcodeIsInDatabase(const string cstBarcode, Package& DeliverdPackage, Receiver& DeliverdReceiver)
 * @li bool ChangeDeliveryState(const string cstBarcode)
 * @li string get_DatabaseName() const
 * @li string get_DatabaseFolder() const
 * @li string get_WholeDatabasePath() const
 * @li void initialize()
 */

#include <string>
#include <sstream>
#include <sqlite3.h>
#include <cassert>

#include "../Libary/Systemlog.h"
#include "../Libary/StructPackage.h"
#include "../Libary/StructReceiver.h"

#ifndef DATABASE_H_INCLUDED
#define DATABASE_H_INCLUDED

/**
* @class Database
* @brief Dient zur Kommunikation mit der Paketdatenbank
*
* @section Aufgaben
* @li Überprüft ob sich ein Barcode in der Datenbank befindet
* @li Ändert den Status eines Paketes auf geliefert
* @li Name, Odnerpfad und komplette Adressierung aller verwendeten (externen) Datei
*     sollen aufrufbar sein
*/
class Database
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/

    Database();
    ~Database();

    bool CheckIfBarcodeIsInDatabase(const std::string cstBarcode, Package& DeliverdPackage, Receiver& DeliverdReceiver);
    bool ChangeDeliveryState(const std::string cstBarcode);

    /****************************************************************
    *                      Get-Methoden                             *
    *****************************************************************/

    std::string get_DatabaseName() const;
    std::string get_DatabaseFolder() const;
    std::string get_WholeDatabasePath() const;

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_Database;

private:
    /*****************************************************************
    *                    Private Methoden                           *
    *****************************************************************/

    void initialize();

    /*****************************************************************
     *                  Private Klassenvariablen                     *
     *****************************************************************/

    const std::string _cstDatabaseName = "PaketDB.db";                   //! Datenbankname
    const std::string _cstDatabaseFolder  = "./Paketbox/00_Databases/"; //! Datenbank Ordner

    std::string _stWholeDatabasePath;                                   //! Kompletter DatenbankPfad

    Package _PackageInformation;                                        //! Struktur mit Paketinformationen
    Receiver _ReceiverInformation;                                      //! Struktur mit Empfaenger Informationen

    Systemlog Logger;
    ReceiveTime Timer;
};
#endif // DATABASE_H_INCLUDED
