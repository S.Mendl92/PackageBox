/**
 * @file   Converter.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für Converter)
 *
 * @section Implementierte Methoden:
 * @li Converter()
 * @li ~Converter()
 * @li string ConvertUnsingendIntToString(const unsigned int cuiUnsingendIntToString) const
 * @li string ConvertBoolToString(const bool cbBoolToString) const
 * @li unsigned int ConvertStringToUnsingendInt(const string cstStringToUnsingendInt) const
 * @li void DeleteUmlauteToRegularLetters(std::string& stText)
 * @li void DetecteInjectionSigns(std::string& stText)
 */
#include "../Libary/Converter.h"

/**
* Konstruktoraufruf der Klasse Converter
*/
Converter::Converter()
{

}

/**
* Destruktoraufruf der Klasse Converter
*/
Converter::~Converter()
{

}

/**
* @brief Konvertiert einen String in ein Unsingend Int
* @param[in] cstStringToUnsingendInt: String mit Zahlen als Inhalt
* @return Konvertierter String als unsingend int
*/
/**
* @test Vorbedingung: String mit Zahlen vorbereiten \n
*       Funktion: Ausführen \n
*       Nachbedinung: String muss als unsingend integer konvertiert sein \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
unsigned int Converter::ConvertStringToUnsingendInt(const std::string cstStringToUnsingendInt) const
{
    //! Initialisiert den Konverter und beschreibt diesen
    std::stringstream ssConverter;
    ssConverter << cstStringToUnsingendInt;

    //! Konvertiert den string
    unsigned int uiReturnValue = 0;
    ssConverter >> uiReturnValue;

    //! Gibt den Wert zurück
    return uiReturnValue;
}

/**
* @brief Konvertiert ein Unsingend Int in ein string
* @param[in] cuiUnsingendIntToString: unsingend Int
* @return Konvertierter unsingend int String
*/
/**
* @test Vorbedingung: ein initialisiertes unsingend int \n
*       Funktion: Ausführen \n
*       Nachbedinung: String muss ausgegeben werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string Converter::ConvertUnsingendIntToString(const unsigned int cuiUnsingendIntToString) const
{
    //! Initialisiert die Systemvariablen und überträgt das unsingend int in den Stream
    std::stringstream ssConverter;
    ssConverter << cuiUnsingendIntToString;

    //! Gibt den extrahierten String zurück
    return ssConverter.str();
}

/**
* @brief Konvertiert ein Bool  in ein string
* @param[in] cbBoolToString: zu konvertierendes bool
* @return Konvertierter String
*/
/**
* @test Vorbedingung: ein initialisiertes bool \n
*       Funktion: Ausführen \n
*       Nachbedinung: String muss ausgegeben werden \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string Converter::ConvertBoolToString(const bool cbBoolToString) const
{
    //! Initalisiert den Konverter
    std::stringstream ssConverter;
    ssConverter << cbBoolToString;

    //! gibt das Ergebnis zurück
    return ssConverter.str();
}

/**
* @brief Löscht alle Umlaute aus einem Text und ersetzt sie durch gebräuchliche Ausdrücke (ä -> ae, ö -> oe, ...)
* @param[in,out] stText: Der zu analysierende String (wird als Referenzparameter übergeben)
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: String mit Umlauten vorbereiten \n
*       Funktion: Ausführen \n
*       Nachbedinung: String muss Umlaute ersetzen \n \n
*       Status: bestanden
*       Datum: März, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void Converter::DeleteUmlauteToRegularLetters(std::string& stText)
{
    std::map<std::string,std::string> Umlaute;

    //! Schreibt alle Umlaute in die Map
    //! first -> Umlaut
    //! seconde -> zu ersetzender Ausdruck
    Umlaute["ä"] = "ae";
    Umlaute["ö"] = "oe";
    Umlaute["ü"] = "ue";
    Umlaute["Ä"] = "Ae";
    Umlaute["Ö"] = "Oe";
    Umlaute["Ü"] = "Ue";

    //! Initialisiert die Variable
    int iPosition = 0;

    for (auto it = Umlaute.begin(); it != Umlaute.end(); it++)
    {
        std::string stFirst = it->first;        //! Beinhaltet den Umlaut
        std::string stSecond = it->second;      //! Benihaltet den Ersatztext

        //! Sucht nach dem ersten Umlaut
        iPosition = stText.find(stFirst);


        const int iWrongPos = -1;               //! Definiert die Abbruchbedingung

        //! Ersetzt den Text solange, bis kein Zeichen mehr gefunden wurde
        while (iPosition != iWrongPos)
        {
            //! Ersetzt den Text
            stText.replace(iPosition, 2, stSecond, 0, stSecond.length());
            iPosition = stText.find(stFirst, iPosition);
        }
    }
}

/**
* @brief Durchsucht einen String auf verdächtige Zeichen (Injection signs
* @param[in,out] stText: Der zu analysierende String (wird im FehlerFall mit einem nicht kritischen Text überschrieben)
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: String mit verbotenen Zeichen vorbereiten \n
*       Funktion: Ausführen \n
*       Nachbedinung: String muss ersetzt werden \n \n
*       Status: bestanden
*       Datum: März, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void Converter::DetecteInjectionSigns(std::string& stText)
{
    std::vector<std::string> InjectionSigns;
    InjectionSigns.clear();

    //! Beinhaltet alle verdächtigen Zeichen
    InjectionSigns.push_back("/");
    InjectionSigns.push_back("<");
    InjectionSigns.push_back(">");
    InjectionSigns.push_back("§");
    InjectionSigns.push_back("$");
    InjectionSigns.push_back("\"");
    InjectionSigns.push_back("%");
    InjectionSigns.push_back("&");
    InjectionSigns.push_back("/");
    InjectionSigns.push_back("=");
    InjectionSigns.push_back("?");
    InjectionSigns.push_back("#");
    InjectionSigns.push_back("'");
    InjectionSigns.push_back("|");
    InjectionSigns.push_back("~");
    InjectionSigns.push_back("+");
    InjectionSigns.push_back("-");
    InjectionSigns.push_back("_");

    //! Überprüft, ob in string ein Zeichen vorhanden ist
    for (auto it = InjectionSigns.begin(); it != InjectionSigns.end(); it++)
    {
        const int iWrongPos = -1;

        if (stText.find(it->back()) != iWrongPos)
        {
            stText = "EnjectionSignsDetected";
            break;
        }
    }
}
