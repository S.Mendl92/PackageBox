/**
 * @file   ZbarCam.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Dezember, 2016
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden (für ZbarCam)
 *
 * @section Implementierte Methoden:
 * @li ZbarCam()
 * @li ~ZbarCam()
 * @li bool RececiveBarcode(string& stBarcode)
 * @li string get_DataName() const
 * @li string get_DataPath() const
 * @li string get_TerminalTextFileName() const
 * @li string get_LastBarcode() const
 * @li string get_LastBarcodeType() const
 * @li string get_WholeTextFileName() const
 * @li string get_stWholeTerminalTextFileName() const
 * @li void initializer()
 * @li void SpiltReceivedBarcode(const string cstReceivedBarcode)
 * @li void DeleteAllCreatedFiles()
 * @li bool ReceiveInfoermationFromVideo(string& stDetectedBarcode)
 */

#include "../Libary/ZbarCam.h"

/**
* @brief Definiert den Konstruktor
*/
ZbarCam::ZbarCam()
{
    initializer();
}

/**
* @brief Definiert den Destruktor
*/
ZbarCam::~ZbarCam()
{

}

/**
* @brief Ermittelt den Barcode aus einem Video
* @param[in,out] stBarcode: Referenzparameter auf ermittelten Barcode
* @return Liefert true zurück, wenn Barcode erkannt
*/
/**
* @test Vorbedingung: Es wurde kein Barcode eingescannt \n
*       Funktion: Barcode einscannen \n
*       Nachbedinung: Barcode ist in Klassenvariable hinterlegt und es wird ein true zurückgegeben \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
/**
* @test Vorbedingung: Es wurde kein Barcode eingescannt \n
*       Funktion: Barcode nicht einscannen und abwarten \n
*       Nachbedinung: Es ist kein Barcode hinterlegt und wird nach ~25s ein false zurückgegeben \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool ZbarCam::RececiveBarcode(std::string& stBarcode)
{
    //! Speichert den Barcode von der Kamera
    std::string stReveivedBarcode = "";

    //! Falls ein Fehler auftrat, wird die Routine beendet
    if(!ReceiveInfoermationFromVideo(stReveivedBarcode))
    {
        //! Schreibt einen SystemLog
        std::string stLog = "Es konnte kein Barcode aus dem Videostream ermittelt werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ERROR, stLog);

        DeleteAllCreatedFiles();
        return false;
    }

    //! Extrahiert alle Daten aus dem Empfangenen string
    SpiltReceivedBarcode(stReveivedBarcode);

    //! Direkte Zuweisung (Klassenvariablen)
    stBarcode = _stLastBarcode;

    //! Schreibt einen Systemlog
    std::string stLog = "Barcode " + _stLastBarcode + " wurde erkannt";
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

    DeleteAllCreatedFiles();

    return true;
}

/**
* @brief Liefert den Dateinnamen
* @return Dateinamen
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert \n
*       Funktion: Vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode \n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string ZbarCam::get_DataName() const
{
    return _cstTextFileName;
}

/**
* @brief Liefert den Dateienpfad
* @return Dateienpfad
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert \n
*       Funktion: Vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode \n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string ZbarCam::get_DataPath() const
{
    return _cstDataPath;
}

/**
* @brief Liefert den Dateinamen des TerminalTextFiles
* @return Dateiname des TerminalTextFiles
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert \n
*       Funktion: Vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode \n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string ZbarCam::get_TerminalTextFileName() const
{
    return _cstTerminalTextFile;
}

/**
* @brief Liefert den letzten erkannten Barcode
* @return letzter erkannter Barcode
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert \n
*       Funktion: Vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode \n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string ZbarCam::get_LastBarcode() const
{
    return _stLastBarcode;
}

/**
* @brief Liefert den letzten erkannten Barcodetypen
* @return letzter erkannter Barcodetyp
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert \n
*       Funktion: Vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode \n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string ZbarCam::get_LastBarcodeType() const
{
    return _stLastBarcodeType;
}

/**
* @brief Liefert den komplleten Textfilenamen
* @return kompletter Textfilename
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert \n
*       Funktion: Vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode \n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
std::string ZbarCam::get_WholeTextFileName() const
{
    return _stWholeTextFileName;
}

/**
* @brief Liefert den kompletten TerminalTextFile Namen
* @return kompletter TerminalTextFile Namen
*/
/**
* @test Vorbedingung: Hinterlegter Datensatz wird seperat in string gespeichert \n
*       Funktion: Vergleich zwischen hinterlegtem Datensatz und dem Rückgabewert der Methode \n
*       Nachbedinung: Beide strings müssen den gleichen Inhalt besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*       Status: bestanden
*/
std::string ZbarCam::get_stWholeTerminalTextFileName() const
{
    return _stWholeTerminalTextFile;
}


/**
* @brief Initialisiert alle Klassenvariablen
* @return Kein Rückgabewert
*/
/**
* @test Wird durch Get-Methodentest bestätigt
*/
void ZbarCam::initializer()
{
    //! Definiert die Ordnerpfade
    _stWholeTextFileName = _cstDataPath + _cstTextFileName;
    _stWholeTerminalTextFile = _cstDataPath + _cstTerminalTextFile;

    //! Letzter Barcode wird initialisiert
    _stLastBarcode = "";
    _stLastBarcodeType = "";
}

/**
* @brief Zerlegt denn String von der Webcam in einzelne Strings
* @param[in] cstReceivedBarcode: zu zerlegender String
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Es ist kein Barcode eingescannt \n
*       Funktion: Barcode einscannen \n
*       Nachbedinung: Klassenvariablen stLastBarcodeType und stLastBarcode müssen die Werte des Barcodes besitzen \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
/**
* @test Vorbedingung: Es ist kein Barcode eingescannt \n
*       Funktion: Barcode Scannvorgang starten, aber nichts einscannen \n
*       Nachbedinung:  Klassenvariablen stLastBarcodeType und stLastBarcode müssen leer bleiben \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void ZbarCam::SpiltReceivedBarcode(const std::string cstReceivedBarcode)
{
    //! sucht die Stelle mit dem ":" um an dieser zu Splitten
    const unsigned int cuiPlace = cstReceivedBarcode.find(":");

    //! Der Barcode wird aufgeteilt
    _stLastBarcodeType = cstReceivedBarcode.substr(0,cuiPlace-1);
    _stLastBarcode = cstReceivedBarcode.substr(cuiPlace+1);
}

/**
* @brief Extrahiert den Barcode aus einem Video
* @param[in,out] stDetectedBarcode: Der ermittelte Barcode wird als Referenzparameter ausgegeben
* @return true= Es wurde ein Barcode ermittelt
*/
/**
* @test Vorbedingung: Es wurde kein Scannvorgang gestartet \n
*       Funktion:  Scannvorgang starten \n
*       Nachbedinung:  Barcode wurde übergeben und temporäre Dateien wurden gelöscht \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
bool ZbarCam::ReceiveInfoermationFromVideo(std::string& stDetectedBarcode)
{
    //! Generiert den String fürs Terminal, der seperat abgekoppelt wird
    std::string stCommandLine = "zbarcam --nodisplay > " +  _stWholeTextFileName + " &";
    int iTerminalState = system(stCommandLine.c_str());

    //! Initialisiert die Puffervariable
    std::string stReceiveInformation = "";

    //! bricht die Schleife ab, sobald etwas erkannt wurde oder cuiMaxLoop durläufe vorbei sind
    for(unsigned int uiLoop = 0; ((uiLoop <= _cuiMaxLoop) && (stReceiveInformation == "")); uiLoop += 1)
    {
        //! Pausiert die Schleife
        const std::chrono::milliseconds Milliseconds(500);
        std::this_thread::sleep_for(Milliseconds);

        //! Öffnet das Textfile
        std::ifstream file1(_stWholeTextFileName.c_str());

        //! Prüft ob Textfile existiert
        if (!file1)
        {
            assert(false);

            //! Schreibt einen Systemlog
            std::string stLog = "Textfile: " + _stWholeTextFileName + " existiert, oder konnte nicht angelegt werden";
            Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);
            return false;
        }

        //! Liest die erste Zeile ein
        getline(file1,stReceiveInformation);

        //!Schliesst das Textfile
        file1.close();
    }

    //! Ermittelter Barcode wird zugewiesen
    stDetectedBarcode = stReceiveInformation;

    //! sucht nach dem Prozess
    stCommandLine = "ps -a | grep zbarcam > " + _stWholeTerminalTextFile;
    iTerminalState = system(stCommandLine.c_str());

    //! Öffnet ein zweites Textfile mit dem Prozess
    std::ifstream file2(_stWholeTerminalTextFile.c_str());

    //! Prüft ob Textfile existiert
    if (!file2)
    {
        assert(false);

        //! Systemlog wird ausgegeben
        std::string stLog = "Textfile: " + _stWholeTerminalTextFile + " existiert, oder konnte nicht angelegt werden";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);
        return false;
    }

    //! Empfängt die erste Zeile des Textfiles
    std::string stReceiveFromTerminalTextfile = "";
    getline(file2, stReceiveFromTerminalTextfile);

    //! wenn Prozess nicht existiert
    if (stReceiveFromTerminalTextfile == "")
    {
        assert(false);

        std::string stLog = "Textfile: " + _stWholeTerminalTextFile + " hat keinen Inhalt, der Prozess wurde anderweitig beendet";
        Logger.WriteSystemLogInDB(Systemlog::COMMENT::ALERT, stLog);

        DeleteAllCreatedFiles();
        return false;
    }

    file2.close();

    //! sucht nach der ersten Leerstelle
    //! Beispielhafte Ausgabe: 16800 pts/0    00:00:09 zbarcam
    unsigned int uiFindPlace = stReceiveFromTerminalTextfile.find(" ",2);

    //! Beendet den Prozess
    stCommandLine = "kill " + stReceiveFromTerminalTextfile.substr(0, uiFindPlace);
    iTerminalState = system(stCommandLine.c_str());

    //! Löscht alle erstellten Files
    DeleteAllCreatedFiles();

    //! Falls keine Information ermittelt wurde wird ein false zurückgegeben
    std::ignore = iTerminalState;
    return (stReceiveInformation != "");
}

/**
* @brief Löscht die erstellten Dateien image und ZbarCam aus dem Ordner Exchangefiles
* @return Kein Rückgabewert
*/
/**
* @test Vorbedingung: Es läuft momentan kein Scannvorgang \n
*       Funktion: Scannvorgang starten \n
*       Nachbedinung: Temporärer Ordner enthält keine Dateien \n \n
*       Status: bestanden
*       Datum: Januar, 2017
*       Tester: Sebastian Mendl (Sebastian.Mendl92@gmx.de)
*/
void ZbarCam::DeleteAllCreatedFiles()
{
    //! Löscht alle Dateien aus dem Temporären Ordner
    const std::string cstCommandLine = "rm " + _stWholeTextFileName + " " + _stWholeTerminalTextFile;
    int iTerminalState = system(cstCommandLine.c_str());

    //! Initialisiert den String für den Systemlog
    std::string stLog = _stWholeTextFileName + " und " + _stWholeTerminalTextFile + " wurden gelöscht";
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

    std::ignore = iTerminalState;
}
