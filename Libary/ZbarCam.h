/**
 * @file   ZbarCam.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Dezember, 2016
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse ZbarCam
 *
 * @section Definierte Methoden:
 * @li ZbarCam()
 * @li ~ZbarCam()
 * @li bool RececiveBarcode(string& stBarcode)
 * @li string get_DataName() const
 * @li string get_DataPath() const
 * @li string get_TerminalTextFileName() const
 * @li string get_LastBarcode() const
 * @li string get_LastBarcodeType() const
 * @li string get_WholeTextFileName() const
 * @li string get_stWholeTerminalTextFileName() const
 * @li void initializer()
 * @li void SpiltReceivedBarcode(const string cstReceivedBarcode)
 * @li void DeleteAllCreatedFiles()
 * @li bool ReceiveInfoermationFromVideo(string& stDetectedBarcode)
 */

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <thread>
#include <chrono>
#include <cassert>

#include "../Libary/Systemlog.h"

#ifndef ZBARCAM_H_INCLUDED
#define ZBARCAM_H_INCLUDED


/**
* @class ZbarCam
* @brief ZbarCam übernimmt die Ansteuerung der Webcam
*
* @section Aufgaben
* @li Barcode ermitteln und als Referenzparameter übergeben
* @li Name, Odnerpfad und komplette Adressierung aller verwendeten (externen) Datei
*     sollen aufrufbar sein
*/
class ZbarCam
{
public:
    /*****************************************************************
     *                      Public-Methoden                          *
     *****************************************************************/

    ZbarCam();
    ~ZbarCam();

    bool RececiveBarcode(std::string& stBarcode);

    /****************************************************************
    *                      Get-Methoden                             *
    *****************************************************************/

    std::string get_DataName() const;
    std::string get_DataPath() const;
    std::string get_TerminalTextFileName() const;
    std::string get_LastBarcode() const;
    std::string get_LastBarcodeType() const;
    std::string get_WholeTextFileName() const;
    std::string get_stWholeTerminalTextFileName() const;

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_ZbarCam;

private:
    /*****************************************************************
     *                    Private Methoden                           *
     *****************************************************************/

    void initializer();
    void SpiltReceivedBarcode(const std::string cstReceivedBarcode);
    void DeleteAllCreatedFiles();
    bool ReceiveInfoermationFromVideo(std::string& stDetectedBarcode);

    /*****************************************************************
     *                  Private Klassenvariablen                     *
     *****************************************************************/

    const std::string _cstTextFileName = "Barcode.txt";              //! Beinhaltet den Dateinamen
    const std::string _cstDataPath = "./Paketbox/99_temp/";         //! Beinhaltet den Speicherort der Datei
    const std::string _cstTerminalTextFile = "ZbarCamProzess.txt"; //! Beinhaltet den Speicherort des Temporären Textfiles

    std::string _stWholeTextFileName;         //! Kompletter Textfile Name
    std::string _stWholeTerminalTextFile;     //! Kompletter TextfilePfad
    std::string _stLastBarcode;               //! letzter ermittelter Barcode
    std::string _stLastBarcodeType;           //! letzter Barcodetyp

    const unsigned int _cuiMaxLoop = 25; //! Anzahl der Maximalen Durchläufe zum Scannen eines Videos (~ 25 Sekunden)

    Systemlog Logger;
};

#endif // ZBARCAM_H_INCLUDED
