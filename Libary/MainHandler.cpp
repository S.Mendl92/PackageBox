/**
 * @file   MainHandler.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Februar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Klassenmethoden der Klasse MainHandler
 *
 * @section Definierte Methoden:
 * @li MainHandler()
 * @li ~MainHandler()
 * @li initilize()
 * @li ConnectToWebserver()
 * @li ReconnectToWebserver()
 * @li HandleButtonState()
 * @li ThreadHandling()
 * @li LoginJobs()
 * @li WebsocketKommuncation()
 * @li task1(bool& bLockThread1)
 * @li task2(bool& bLockThread2)
 * @li task3(bool& bLockThread3)
 * @li ExtendMainCycleTime(const unsigned int cuiMaxCycleTime, const std::chrono::system_clock::time_point StartCycleTime,
                           const std::chrono::system_clock::time_point EndCycleTime)
 * @li GeneratePositiveFlag(const bool cbPositiveEdge, bool& bAuxPoitiveEdge)
 */

#include "../Libary/MainHandler.h"

/**
*@brief Konstruktoraufruf
*/
MainHandler::MainHandler()
{
    initilize();
}

/**
*@brief Destruktoraufruf
*/
MainHandler::~MainHandler()
{
    //! Beendet die Websocketverbindung und schließt den Server
    Client.close(_WebsocketConnectionId, websocketpp::close::status::normal, "");
    NodeJsHandler.StopNodeJsServer();
}

/**
* @brief Verbindet sich zum Webserver und zum Websocketserver
* @return Kein Rückgabewert
*/
void MainHandler::ConnectToWebserver()
{
    //! Startet den NodeJs Server
    NodeJsHandler.StartNodeJsServer();

    //! Initialisiert den WebsocketClient
    _WebsocketConnectionId = Client.connect(_cstWebsocketUrl);

}

/**
* @brief Verbindet sich erneut zum Webserver und zum Websocketserver
* @return Kein Rückgabewert
*/
void MainHandler::ReconnectToWebserver()
{
    //! Schließt eine andere NodeJs Server Instanz
    NodeJsHandler.StopNodeJsServer();

    //! Verbindet sich neu zum Server
    NodeJsHandler.StartNodeJsServer();
    _WebsocketConnectionId = Client.connect(_cstWebsocketUrl);
}

/**
* @brief Initialisiert alle Klassenvariablen
* @return Kein Rückgabewert
*/
void MainHandler::initilize()
{
    //! Initialisiert die Variablen für die Flankenbildung
    _bPositiveEdgeScannerButton = false;
    _bPositiveEdgeErrorButton = false;

    _bAuxPositiveEdgeScannerButton = false;
    _bAuxPositiveEdgeErrorButton = false;

    _bScannerButtonState = false;
    _bErrorButtonState = false;


    //! Initialisiert die Variablen zum Sperren der einzelnen Threads
    _bLockThread1 = false;
    _bLockThread2 = false;
    _bLockThread3 = false;
    _bShudtdownTargetSystem = false;

    _WebsocketConnectionId = 0;

    //! Erstellt den Systemlogkommentar
    const std::string cstLog = "Initialisierung ist abgeschlossen, Paketbox ist Betriebsbereit";
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL,cstLog);
}

/**
* @brief Speichert die Aktuellen Zustande der Aktivierungstaster und bildet Flanken
* @return Kein Rückgabewert
*/
void MainHandler::HandleButtonState()
{
    //! Speichert den Status des Taster
    _bScannerButtonState = GPIOPort.get_ScannerButtonState();
    _bErrorButtonState = GPIOPort.get_ErrorButtonState();

    //! Bildet Positive Flanken
    _bPositiveEdgeScannerButton = GeneratePositiveFlag(_bScannerButtonState, _bAuxPositiveEdgeScannerButton);
    _bPositiveEdgeErrorButton = GeneratePositiveFlag(_bErrorButtonState, _bAuxPositiveEdgeErrorButton);
}

/**
* @brief Verwaltet die Ansteuerung der einzelnen Threads
* @return Kein Rückgabewert
*/
void MainHandler::ThreadHandling()
{
    //! Überprüft ob Paketbox in Zeitparameter liegt (Arbeitsbereit ist)
    _bLockPackageBox = WebsiteHandler.CheckWorkingState();

    //! Überprüft ob die Paketbox Betriebsbereit ist
    if(_bLockPackageBox)
    {
        //! Start den Thread task1 falls er noch nicht gestartet wurde
        if(_bPositiveEdgeScannerButton and not _bLockThread1 and not _bLockThread2 and not _bLockThread3)
        {
            //! Sperrt die Freigabe des Threads 1 und wird per Referenz an den Task übergeben
            _bLockThread1 = true;
            std::thread {&MainHandler::task1,this,std::ref(_bLockThread1)} .detach();
        }

        //! Startet den Thread task2 falls er noch nicht gestartet wurde
        if(_bPositiveEdgeErrorButton and not _bLockThread1 and not _bLockThread2 and not _bLockThread3)
        {
            //! Sperrt die Freigabe des Threads 2 und wird per Referenz an den Task übergeben
            _bLockThread2 = true;
            std::thread {&MainHandler::task2,this,std::ref(_bLockThread2)} .detach();
        }
    }
    else
    {
        //! Gibt eine Meldung aus, wenn Paketbox nicht Betriebsbereit aber Knopf gedrückt wird
        if((_bPositiveEdgeScannerButton or _bPositiveEdgeErrorButton) and not _bLockThread3 )
        {
            //! Sperrt die Freigabe des Threads 3 und wird per Referenz an den Task übergeben
            _bLockThread3 = true;
            std::thread {&MainHandler::task3,this,std::ref(_bLockThread3)} .detach();
        }
    }
}

/**
* @brief Führt Tatigkeiten aus, wenn der Benutzer eingeloggt ist
* @return Kein Rückgabewert
*/
void MainHandler::LoginJobs()
{
    //! Vergleicht die Datenstäze des Soundmoduls und spielt keinen Sound auf dem Rasberry Pi direkt ab
    TTS.CompareDatasets(false);
    GPIOPort.ContolAktorsByWebsite(_bLockThread1, _bLockThread2);

    //! Überprüft, ob die Paketbox beendet werden soll
    _bShudtdownTargetSystem = WebsiteHandler.HandleTargetSystemByWebsite();
}

/**
* @brief Versendet die Websocketnachrichten
* @return Kein Rückgabewert
*/
void MainHandler::WebsocketKommuncation()
{
    std::list<std::string> SendState;

    //! Schreibt die aktuellen Aktor und Sensorzustände in die Liste
    SendState.push_back(NodeJsHandler.ConvertForWebsocket(MyWebsocketProtocol::PROTOCOLCODE::DOOR, GPIOPort.get_DoorState()));
    SendState.push_back(NodeJsHandler.ConvertForWebsocket(MyWebsocketProtocol::PROTOCOLCODE::ERRORLED, GPIOPort.get_ErrorLEDState()));
    SendState.push_back(NodeJsHandler.ConvertForWebsocket(MyWebsocketProtocol::PROTOCOLCODE::SCANNERBUTTON, _bScannerButtonState));
    SendState.push_back(NodeJsHandler.ConvertForWebsocket(MyWebsocketProtocol::PROTOCOLCODE::ERRORBUTTON, _bErrorButtonState));
    SendState.push_back(NodeJsHandler.ConvertForWebsocket(MyWebsocketProtocol::PROTOCOLCODE::WORKINGSTATE, _bLockPackageBox));

    //! Versendet Websocket Nachrichten
    for(auto it = SendState.begin(); it != SendState.end(); it++)
    {
        const std::string cstConvertedState = (*it);
        Client.send(_WebsocketConnectionId, cstConvertedState);
    }
}

/**
* @brief Verwaltet den zyklischen Ablauf der Paketbox
* @return Kein Rückgabewert
*/
void MainHandler::RunPackageBox()
{
    ConnectToWebserver();

    while(not _bShudtdownTargetSystem)
    {
        auto StartCycleTime = std::chrono::system_clock::now();

        //! Verarbeitet Tasterwerte
        HandleButtonState();
        ThreadHandling();

        //! Startet den Server erneut, falls beendet und stellt die WebsocketVerbindung wieder her
        if(!NodeJsHandler.CheckStateOfNodeJsServer())
        {
            ReconnectToWebserver();
        }

        //! Wird nur ausgeführt, wenn der Benutzer eingelogt ist
        if(WebsiteHandler.CheckUserLoginState())
        {
            LoginJobs();
        }

        //! Statuswerte an GPIOPort übertragen
        GPIOPort.WriteStateInFile(TTS.get_GPIOTextFilePath(), _bLockPackageBox);

        //! Websocket Kommunikation: Sendet Parameter an Webserver
        WebsocketKommuncation();

        auto EndCycleTime = std::chrono::system_clock::now();

        //! Berechnet die Pause für jeden Zyklus, um den Schleifendurchlauf anzupassen und pausiert die main (Zykluszeitgenau)
        ExtendMainCycleTime(_cuiMaxCycleTimeMilliseconds, StartCycleTime, EndCycleTime);
    }
}

/**
* @brief Bildet eine positive Flanke
* @param[in] cbPositiveEdge: Eingang aus dem eine Flanke gebildet wird
* @param[in,out] bAuxPoitiveEdge: Hilfsmerker
* @return true = Flanke | false = keine Flanke
*/
bool MainHandler::GeneratePositiveFlag(const bool cbPositiveEdge, bool& bAuxPoitiveEdge)
{
    const bool cbFlag = (cbPositiveEdge and not bAuxPoitiveEdge);
    bAuxPoitiveEdge = cbPositiveEdge;
    return cbFlag;
}

/**
* @brief Wird gestartet sobald der Scanner Taster gedrückt wird und leitet die Service Routine ein
* @param[in,out] bLockThread1: wird am Ende Threads wieder freigeben (Steuerwert = false)
* @return Kein Rückgabewert
*/
void MainHandler::task1(bool& bLockThread1)
{
    //! Initialisierung der einzelnen Klassenvariablen
    ZbarCam Cam;
    Database CheckDatabase;
    MailClient Mail;
    GPIOControl Aktor;

    //! Begrüßungsansage wird ausgegeben
    TTS.PlayOutputFile(TextToSpeech::SOUNDFILE::WELCOME);

    //! Initialisiert die einzelnen string Variablen
    std::string stBarcode= "";

    //! Empfägt den Barcode von der Webcam
    bool bStateBarcode = Cam.RececiveBarcode(stBarcode);

    if(!bStateBarcode)
    {
        TTS.PlayOutputFile(TextToSpeech::SOUNDFILE::FALSEBARCODE);

        //! Gibt den Thread wieder frei
        bLockThread1 = false;
        return;
    }

    //! Definiert die einzelnen Strukturen
    Package DeliverdPackage;
    Receiver ReceiverInformation;

    //! Überprüft ob sich ein Paket in der Datenbank befindet
    bool bPackage = CheckDatabase.CheckIfBarcodeIsInDatabase(stBarcode, DeliverdPackage, ReceiverInformation);

    if(bPackage)
    {
        //! Sendet eine Mail, das ein Paket empfangen worden ist
        Mail.SendMailOrderdPackageReceived(DeliverdPackage,ReceiverInformation);
        TTS.PlayOutputFile(TextToSpeech::SOUNDFILE::PACKAGERECEIVEDREGULARE);

        //! Steuert den Tür Aktor an
        const unsigned int cuiActiveTimeInSeconds = 5;
        Aktor.ControlDoor(cuiActiveTimeInSeconds, true);

        //! Ändert den Lieferstatus des Paketes
        CheckDatabase.ChangeDeliveryState(stBarcode);
    }
    else
    {
        //! Informiert, das ein Falsches Paket eingescannt wurde
        Mail.SendMailWrongPackageReceived(stBarcode);
        TTS.PlayOutputFile(TextToSpeech::SOUNDFILE::BARCODEISNOTSCANNABLE);
    }

    //! Gibt den Thread wieder frei
    bLockThread1 = false;
}

/**
* @brief Wird gestartet sobald der Fehler Taster gedrückt wird und leitet die Service Routine ein
* @param[in,out] bLockThread2: wird am Ende Threads wieder freigeben (Steuerwert = false)
* @return Kein Rückgabewert
*/
void MainHandler::task2(bool& bLockThread2)
{
    //! Initialisiert alle Klassenvariablen
    ZbarCam Cam;
    Database CheckDatabase;
    UserManagement Login;
    MailClient Mail;
    GPIOControl Aktor;

    //! Steuert die Fehler LED an
    Aktor.ControlErrorLED(true, true);

    //! Spielt die Begrüßungsansage ab
    TTS.PlayOutputFile(TextToSpeech::SOUNDFILE::ERRORMODE);

    //! Initialisiert die Variablen für die Barcode Erkennung
    std::string stBarcode= "";

    //! Empfängt den Barcode
    bool bStateBarcode = Cam.RececiveBarcode(stBarcode);

    //! Wenn Barcode nicht gefunden wurde
    if(!bStateBarcode)
    {
        //! Spiele Ansage ab
        TTS.PlayOutputFile(TextToSpeech::SOUNDFILE::ERRORMODEFALSEBARCODE);

        //! Gibt den Thread wieder frei
        bLockThread2 = false;

        //! Led ausschalten
        Aktor.ControlErrorLED(false, true);
        return;
    }

    //! Initialisiert die Structurvariablen
    Package DeliverdPackage;
    Receiver ReceiverInformation;

    //! Überpüft sich der Barcode in der Datenbank befindet
    bool bPackage = CheckDatabase.CheckIfBarcodeIsInDatabase(stBarcode, DeliverdPackage, ReceiverInformation);

    //! Wenn der Barcode sich in der Datenbank befindet
    if(bPackage)
    {
        //! Empfängerliste wird benachrichtigt, dass ein richtiges Paket eingescannt wurde
        Mail.SendMailPackageReceivedWithWrongSize(DeliverdPackage, ReceiverInformation);

        //! Ansage wird abgespielt
        TTS.PlayOutputFile(TextToSpeech::SOUNDFILE::DELIVERYPLACE);

        //! Datenbankstatus wird auf geliefert geändert
        CheckDatabase.ChangeDeliveryState(stBarcode);
    }
    else
    {
        //! Empfängerliste wird benachrichtigt, dass ein fehlerhaftes Paket eingescannt wurde
        Mail.SendMailWrongPackageReceived(stBarcode);

        //! Ansage wird abgespielt
        TTS.PlayOutputFile(TextToSpeech::SOUNDFILE::ERRORMODEFALSEBARCODE);
    }

    //! LED wird deaktiviert und Thread wieder freigeben
    Aktor.ControlErrorLED(false, true);
    bLockThread2 = false;
}

/**
* @brief Wird gestartet sobald ein Taster gedrückt wird und Paketbox nicht in Betriebsbereitschaft ist
* @param[in,out] bLockThread3: wird am Ende Threads wieder freigeben (Steuerwert = false)
* @return Kein Rückgabewert
*/
void MainHandler::task3(bool& bLockThread3)
{
    //! Initialisiert die verwendeten Objekte
    MailClient Mail;

    //! Informiert den Benutzer über das Soundfile
    TTS.PlayOutputFile(TextToSpeech::SOUNDFILE::FALSEWORKINGMODE);

    //! Schreibt einen SystemLog
    std::string stLog = "Die Paketbox war nicht Betriebsbereit und Paketlieferant war vor Ort";
    Logger.WriteSystemLogInDB(Systemlog::COMMENT::INFORMATIONAL, stLog);

    Mail.SendMailActionDuringFalseWorkingState();

    //! Gibt den Thread wieder frei
    bLockThread3 = false;
}

/**
* @brief Berechnet die zu erweiternde Zykluszeit und pausiert die while Schleife innerhalb der Main
* @param[in] uiMaxCycleTime: Die Maximale Zykluszeit in ms
* @param[in] StartCycleTime: Zeitmessung bei beginn der while Schleife
* @param[in] EndCycleTime: Zeitmessung am ende der while Schleife
* @return Kein Rückgabewert
*/
void MainHandler::ExtendMainCycleTime(const unsigned int cuiMaxCycleTime, const std::chrono::system_clock::time_point StartCycleTime,
                                      const std::chrono::system_clock::time_point EndCycleTime)
{
    //! Reserviert den Speicher für die static Variable (wird bei der ersten Initialisierung automatisch mit 0 initialisiert)
    static unsigned long long int ulliArithmeticCycleValue;
    static unsigned long long int ulliCurrentCycle;
    static unsigned long long int ulliAktiveTimeInHours;

    //! Initialisert die eigene Startzeit
    const auto OwnStartTime = std::chrono::system_clock::now();

    //! Konvertiert die Parameter
    const unsigned int cuiMainCycleTime = std::chrono::duration_cast<std::chrono::milliseconds>(EndCycleTime - StartCycleTime).count();

    //! Überprüft im ersten Zyklus, ob die Maximale Zykluszeit richtig eingetragen wurde
    unsigned int uiFixedCycleTime = cuiMaxCycleTime;
    const unsigned int cuiFixeTime = 750;

    if((cuiMaxCycleTime <= cuiFixeTime))
    {
        //! Korrigiert die Cycluszeit
        uiFixedCycleTime = cuiFixeTime;
    }

    //! Definiert, nach wie vielen Zyklen eine Systemlogmeldung verfasst werden soll
    //! @todo 10 muss auf 1000 vor der Auslieferung geändert werden
    const unsigned int cuiWirteSystemLogAtCycle = (10*uiFixedCycleTime);

    //! Berechnet die Anzahl der Durchläufe pro Stunde (1000 ms / 200 ms) * (60 s/min) * (60 min/h)
    const unsigned long long int ulliCyclesPerHour = ((1000/uiFixedCycleTime) * 60 * 60);

    //! Frägt den aktuellen Zyklus ab
    if(ulliCyclesPerHour*(ulliAktiveTimeInHours+1) == ulliCurrentCycle)
    {
        //! Erhöht den Betriebstundenzähler
        ulliAktiveTimeInHours += 1;
    }

    //! Handelt die static Parameter
    ulliCurrentCycle += 1;
    ulliArithmeticCycleValue += cuiMainCycleTime;

    if(not (ulliCurrentCycle % cuiWirteSystemLogAtCycle))
    {
        //! Berechnet die genaue Zykluszeit (arithmetisches Mittel)
        const double cdCalculatedArithmeticCycleTime = (double)ulliArithmeticCycleValue/(double)cuiWirteSystemLogAtCycle;

        //! Bereitet den stringstream für die Systemlogmeldung vor und schreibt einen Logmeldung
        std::stringstream ssLog;
        ssLog << "Die aktuelle arithmetische Durchlaufzykluszeit beträgt: " << cdCalculatedArithmeticCycleTime << " ms" << std::endl
              << "Die eingestellte Gesammtzykluszeit beträgt: " << uiFixedCycleTime << " ms" << std::endl
              << "Anzahl der Zyklen: " << ulliCurrentCycle << " (seit dem Start der Paketbox)"<< std::endl
              << "Die aktive Zeit der Paketbox beträgt: " << ulliAktiveTimeInHours << " h";

        Logger.WriteSystemLogInDB(Systemlog::COMMENT::DEBUG, ssLog.str());

        //! Setzt die Variable ulliArithmeticCycleValue zurück
        ulliArithmeticCycleValue = 0;
    }

    //! Ende der eigenen Zeitmessung
    const auto OwnEndTime = std::chrono::system_clock::now();

    //! Berechnet die zu pausierende zeit und pausiert die while Schleife
    const unsigned int cuiOwnCycleTime = std::chrono::duration_cast<std::chrono::milliseconds>(OwnEndTime - OwnStartTime).count();
    const std::chrono::milliseconds SleepCycleTime(uiFixedCycleTime - (cuiMainCycleTime + cuiOwnCycleTime));

    //! Pausiert den Thread
    std::this_thread::sleep_for(SleepCycleTime);
}
