/**
 * @file   MainHandler.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Februar, 2017
 * @version 1.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse MainHandler
 *
 * @section Definierte Methoden:
 * @li MainHandler()
 * @li ~MainHandler()
 * @li initilize()
 * @li ConnectToWebserver()
 * @li ReconnectToWebserver()
 * @li HandleButtonState()
 * @li ThreadHandling()
 * @li LoginJobs()
 * @li WebsocketKommuncation()
 * @li task1(bool& bLockThread1)
 * @li task2(bool& bLockThread2)
 * @li task3(bool& bLockThread3)
 * @li ExtendMainCycleTime(const unsigned int cuiMaxCycleTime, const std::chrono::system_clock::time_point StartCycleTime,
                           const std::chrono::system_clock::time_point EndCycleTime)
 * @li GeneratePositiveFlag(const bool cbPositiveEdge, bool& bAuxPoitiveEdge)
 */

 #include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <list>

#include "../Libary/ZbarCam.h"
#include "../Libary/Systemlog.h"
#include "../Libary/Database.h"
#include "../Libary/GPIOControl.h"
#include "../Libary/MailClient.h"
#include "../Libary/TextToSpeech.h"
#include "../Libary/UserManagement.h"
#include "../Libary/ReceiveTime.h"
#include "../Libary/DatabaseWrapper.h"
#include "../Libary/MyWebsocketProtocol.h"
#include "../Libary/MyNodeJsHandler.h"

#include "../Libary/WebsocketClient.h"

#ifndef MAINHANDLER_H_INCLUDED
#define MAINHANDLER_H_INCLUDED
/**
* @class MainHandler
* @brief Der MainHandler verwaltet den Kompletten Ablauf der Paketbox
*
* @section Aufgaben
* @li Stellt Grundzusatand her
* @li Verbindet sich zum Server
* @li Stellt die Websocketverbindung her
* @li Steuert das Threadhandling
* @li Steuert den Ablauf der Paketbox
*/
class MainHandler
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/
    MainHandler();
    ~MainHandler();

    void RunPackageBox();

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_MainHandler;

private:
    /*****************************************************************
    *                    Private Methoden                           *
    *****************************************************************/

    void initilize();
    void ConnectToWebserver();
    void ReconnectToWebserver();
    void HandleButtonState();
    void ThreadHandling();
    void LoginJobs();
    void WebsocketKommuncation();
    void task1(bool& bLockThread1);
    void task2(bool& bLockThread2);
    void task3(bool& bLockThread3);
    void ExtendMainCycleTime(const unsigned int cuiMaxCycleTime, const std::chrono::system_clock::time_point StartCycleTime,
                             const std::chrono::system_clock::time_point EndCycleTime);

    bool GeneratePositiveFlag(const bool cbPositiveEdge, bool& bAuxPoitiveEdge);

    /*****************************************************************
    *                  Private Klassenvariablen                     *
    *****************************************************************/

    bool _bPositiveEdgeScannerButton;       //! true = Positive Flanke des ScannerButtons wurde erkannt
    bool _bPositiveEdgeErrorButton;         //! true = Positive Flanke des Fehlerbuttons wurde erkannt
    bool _bAuxPositiveEdgeScannerButton;    //! Hilfsmerker
    bool _bAuxPositiveEdgeErrorButton;      //! Hilfsmerker
    bool _bLockThread1;                     //! true = thread 1 wurde gestartet
    bool _bLockThread2;                     //! true = thread 2 wurde gestartet
    bool _bLockThread3;                     //! true = thread 1 wurde gestartet
    bool _bShudtdownTargetSystem;           //! true = System wird heruntergefahren
    bool _bLockPackageBox;                  //! true = Paketbox ist inaktiv
    bool _bScannerButtonState;              //! true = Scanner Taster aktive
    bool _bErrorButtonState;                //! true = Fehlertaster aktive

    const unsigned int _cuiMaxCycleTimeMilliseconds = 1000;              //! wird ggf. automatisch korrigiert
    const std::string _cstWebsocketUrl = "ws://localhost:8080/foo";     //! URL: Websocketserver

    unsigned int _WebsocketConnectionId;                                //! Beinhaltet die ID der Websocketverbindung

    GPIOControl GPIOPort;
    TextToSpeech TTS;
    Systemlog Logger;
    UserManagement WebsiteHandler;
    MyNodeJsHandler NodeJsHandler;
    WebsocketClient Client;
};
#endif // MAINHANDLER_H_INCLUDED
