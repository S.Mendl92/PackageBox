/**
 * @file   DatasetWrapper.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse ZbarCam
 *
 * @section Definierte Methoden:
 * @li DatabaseWrapper()
 * @li ~DatabaseWrapper()
 * @li string get_DatabaseName() const
 * @li string get_DatabasePath() const
 * @li string get_WholeDatabasePath() const
 * @li bool ReadInformationFromDatabase(const unsigned int cuiDatasetNumber, Settings& Parameters)
 * @li void WriteInformationToDatabase(const unsigned int cuiDatasetNumber,const unsigned int cuiDatabaseValue)
 */

#include <string>
#include <sstream>
#include <sqlite3.h>
#include <cassert>

#include "../Libary/StructSettings.h"
#include "../Libary/Systemlog.h"

#ifndef DATABASEWRAPPER_H_INCLUDED
#define DATABASEWRAPPER_H_INCLUDED

/**
* @class DatabaseWrapper
* @brief Empfängt Information
*
* @section Aufgaben
* @li Empfängt Informationen aus der Einstellungsdatenbank (anhand der Datensatznummer) und gibt diese gebündelt
*     als Struktur zurück
* @li Name, Odnerpfad und komplette Adressierung aller verwendeten (externen) Datei
*     sollen aufrufbar sein
* @li <b>Eine Instanzierung macht keine Sinn, da von dieser Klasse nur geerbt werden kann<\b>
*/
class DatabaseWrapper
{
public:
    /*****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/

    DatabaseWrapper();
    ~DatabaseWrapper();

    /****************************************************************
    *                      Get-Methoden                             *
    *****************************************************************/

    std::string get_DatabaseName() const;
    std::string get_DatabasePath() const;
    std::string get_WholeDatabasePath() const;

    /*****************************************************************
    *                      enum - Deklaration                        *
    *****************************************************************/

    //! @enum Symbolisiert die einzelnen Datensatznummern (Primär Schlüssel) der Settingsdatenbank
    enum INTERFACE
    {
        HANDSHAKE = 1,                 //!< Dient zur Signalisierung des Prozesses der Paketbox (nicht implementiert)
        USERLOGINSTATE = 2,            //!< 1 = Benutzer ist angemeldet | 0 = Benutzer ist nicht angemledet
        HANDLECODE = 3,                //!< Reserver
        SPARE4 = 4,                    //!< Reserver
        SPARE5 = 5,                    //!< Reserver
        SPARE6 = 6,                    //!< Reserver
        SPARE7 = 7,                    //!< Reserver
        SPARE8 = 8,                    //!< Reserver
        SPARE9 = 9,                    //!< Reserver
        SCANNERBUTTONSTATE = 10,       //!< 1 = Scanner Taster wurde von VM betätigt | 0 = Scanner Taster wurde nicht von der VM betätigt
        ERRORBUTTONSTATE = 11,         //!< 1 = Fehler Taster wurde von VM betätigt | 0 = Fehler Taster wurde nicht von der VM betätigt
        HANDLEDOORBYWEBSITE = 12,      //!< 1 = Fehler Taster wurde von Website "betätigt" | 0 = Fehler Taster wurde nicht von der Website "betätigt"
        HANDLEERRORLEDBYWEBSITE = 13,  //!< 1 = Fehler Taster wurde von Website "Website" | 0 = Fehler Taster wurde nicht von der Website "betätigt"
        SPARE14 = 14,                  //!< Reserver
        SPARE15 = 15,                  //!< Reserver
        SPARE16 = 16,                  //!< Reserver
        SPARE17 = 17,                  //!< Reserver
        SPARE18 = 18,                  //!< Reserver
        SPARE19 = 19,                  //!< Reserver
        STATESUNDAY = 20,              //!< 1 = Paketbox ist am Sonntag aktiv | 0 = Paketbox ist nicht am Sonntag aktiv
        STATEMONDAY = 21,              //!< 1 = Paketbox ist am Montag aktiv | 0 = Paketbox ist nicht am Montag aktiv
        STATETUESDAY = 22,             //!< 1 = Paketbox ist am Dienstag aktiv | 0 = Paketbox ist nicht am Dienstag aktiv
        STATEWEDNESDAY = 23,           //!< 1 = Paketbox ist am Mittwoch aktiv | 0 = Paketbox ist nicht am Mittwoch aktiv
        STATETHUERSDAY = 24,           //!< 1 = Paketbox ist am Donnerstag aktiv | 0 = Paketbox ist nicht am Donnerstag aktiv
        STATEFRIDAY = 25,              //!< 1 = Paketbox ist am Freitag aktiv | 0 = Paketbox ist nicht am Freitag aktiv
        STATESATURDAY = 26,            //!< 1 = Paketbox ist am Samstag aktiv | 0 = Paketbox ist nicht am Samstag aktiv
        STARTHOUR = 27,                //!< [0..24] Beinhaltet die Startzeit (Ab wann aktiv)
        ENDHOUR = 28,                  //!< [0..24] Beinhaltet die Endzeit (Ab wann nicht mehr aktiv)
        HOURS24 = 29,                  //!< 1 = Paketbox ist 24 Stunden aktiv | 0 = Paketbox ist nicht aktiv
        SPARE30 = 30,                  //!< Reserver
        SPARE31 = 31,                  //!< Reserver
        SPARE32 = 32,                  //!< Reserver
        SPARE33 = 33,                  //!< Reserver
        SPARE34 = 34,                  //!< Reserver
        SPARE35 = 35,                  //!< Reserver
        TEXTTOTPEECH1 = 36,            //!< Beinhaltet den Text für das Soundfile1.wav
        TEXTTOTPEECH2 = 37,            //!< Beinhaltet den Text für das Soundfile2.wav
        TEXTTOTPEECH3 = 38,            //!< Beinhaltet den Text für das Soundfile3.wav
        TEXTTOTPEECH4 = 39,            //!< Beinhaltet den Text für das Soundfile4.wav
        TEXTTOTPEECH5 = 40,            //!< Beinhaltet den Text für das Soundfile5.wav
        TEXTTOTPEECH6 = 41,            //!< Beinhaltet den Text für das Soundfile6.wav
        TEXTTOTPEECH7 = 42,            //!< Beinhaltet den Text für das Soundfile7.wav
        TEXTTOTPEECH8 = 43,            //!< Beinhaltet den Text für das Soundfile8.wav
        TEXTTOTPEECH9 = 44,            //!< Beinhaltet den absoluten Pfad zur Textdatei zum generieren des Soundfile9.wav
        TEXTTOTPEECH10 =45,            //!< Beinhaltet den absoluten Pfad zur Textdatei zum generieren des Soundfile10.wav
        MAILRECEIVER1 = 46,            //!< 1. Mailempfänger | NULL kein Mailempfänger ist hinterlegt
        MAILRECEIVER2 = 47,            //!< 2. Mailempfänger | NULL kein Mailempfänger ist hinterlegt
        MAILRECEIVER3 = 48,            //!< 3. Mailempfänger | NULL kein Mailempfänger ist hinterlegt
        MAILRECEIVER4 = 49,            //!< 4. Mailempfänger | NULL kein Mailempfänger ist hinterlegt
        MAILRECEIVER5 =50             //!< 5. Mailempfänger | NULL kein Mailempfänger ist hinterlegt
    };

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_DatabaseWrapper;

protected:
    /*****************************************************************
    *                    Protected-Methoden                          *
    ****************************************************************/

    bool ReadInformationFromDatabase(const unsigned int cuiDatasetNumber, Settings& Parameters);
    void WriteInformationToDatabase(const unsigned int cuiDatasetNumber,const unsigned int cuiDatabaseValue);

private:
    /*****************************************************************
    *                  Privat-Methoden                              *
    ****************************************************************/

    void initialize();

    /*****************************************************************
    *                  Private Klassenvariablen                     *
    ****************************************************************/

    const std::string _cstDatabaseName = "Settings.db";                             //! Datenbank Name
    const std::string _cstDatabasePath = "./Paketbox/00_Databases/";               //! Datenbank Ordner

    std::string _stWholeDatabasePath ="./Paketbox/00_Databases/Settings.db";//! Kompletter Datenbank Pfad

    Systemlog Logger;
};
#endif // DATABASEWRAPPER_H_INCLUDED
