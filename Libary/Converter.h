/**
 * @file   Converter.h
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Schnittstellenimplementierung der Klasse Converter
 *
 * @section Definierte Methoden:
 * @li Converter()
 * @li ~Converter()
 * @li string ConvertUnsingendIntToString(const unsigned int cuiUnsingendIntToString) const
 * @li string ConvertBoolToString(const bool cbBoolToString) const
 * @li unsigned int ConvertStringToUnsingendInt(const string cstStringToUnsingendInt) const
 * @li void DeleteUmlauteToRegularLetters(std::string& stText)
 * @li void DetecteInjectionSigns(std::string& stText)
 */

#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <cassert>

#ifndef CONVERTER_H_INCLUDED
#define CONVERTER_H_INCLUDED

/**
* @class Converter
* @brief Übernimmt die Convertierungsaufgaben von verschiedenen Datentypen
*/
class Converter
{
public:
    /****************************************************************
    *                      Public-Methoden                          *
    *****************************************************************/

    Converter();
    ~Converter();

    /****************************************************************
    *                      friend class                             *
    *****************************************************************/

    friend class AutoTest_Converter;

protected:
    /*****************************************************************
    *                    Protected-Methoden                          *
    ****************************************************************/

    std::string ConvertUnsingendIntToString(const unsigned int cuiUnsingendIntToString) const;
    std::string ConvertBoolToString(const bool cbBoolToString) const;

    void DeleteUmlauteToRegularLetters(std::string& stText);
    void DetecteInjectionSigns(std::string& stText);

    unsigned int ConvertStringToUnsingendInt(const std::string cstStringToUnsingendInt) const;
};
#endif // CONVERTER_H_INCLUDED
