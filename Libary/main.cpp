/**
 * @file   SoftwarePaketbox.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Januar, 2017
 * @version 2.0
 *
 * @brief Beinhaltet die Implementierung der Main
 */

#include "../Libary/MainHandler.h"

/**
* Hauptprogramm
* @return 0 = Kein Fehler | 0 <> Progamm wurde fehlerhaft beendet
*/
int main()
{
    MainHandler Main;
    Main.RunPackageBox();

    return 0;
}
