/**
 * @file   Metadata.cpp
 * @author Sebastian Mendl (Sebastian.Mendl92@gmx.de)
 * @date   Februar, 2017
 * @version 1.0
 *
 * @brief Beinhaltet die Implementierung der Klasse Metadata
 *
* @section Definierte Methoden:
 * @li ~Metadata()
 * @li void on_open(client * c, websocketpp::connection_hdl hdl)
 * @li void on_fail(client * c, websocketpp::connection_hdl hdl)
 * @li void on_close(client * c, websocketpp::connection_hdl hdl)
 * @li void on_message(websocketpp::connection_hdl, client::message_ptr msg)
 * @li websocketpp::connection_hdl get_hdl() const
 * @li int get_id() const
 * @li std::string get_status() const
 * @li void record_sent_message(std::string message)
 */

#include "../Libary/Metadata.h"

/**
* @brief Konstruktoraufruf
* @param[in] id: Verbindungsid
* @param[in] hdl: Pointer auf Verbindung
* @param[in] uri: Websocketadresse
*/
Metadata::Metadata(int id, websocketpp::connection_hdl hdl, std::string uri)
{
    m_id = id;
    m_hdl= hdl;
    m_status = "Connecting";
    m_uri = uri;
    m_server = "N/A";
}

/**
* @brief Destruktoraufruf
*/
Metadata::~Metadata()
{

}

/**
* @brief Stellt eine Verbindung her
* @param[in] c: Pointer auf Websocketclient
* @param[in] hdl: Pointer auf Verbindung
* @return Kein Rückgabewert
*/
void Metadata::on_open(client * c, websocketpp::connection_hdl hdl)
{
    m_status = "Open";

    client::connection_ptr con = c->get_con_from_hdl(hdl);
    m_server = con->get_response_header("Server");
}

/**
* @brief Eine Verbindung ist fehlgeschlagen
* @param[in] c: Pointer auf Websocketclient
* @param[in] hdl: Pointer auf Verbindung
* @return Kein Rückgabewert
*/
void Metadata::on_fail(client * c, websocketpp::connection_hdl hdl)
{
    m_status = "Failed";

    client::connection_ptr con = c->get_con_from_hdl(hdl);
    m_server = con->get_response_header("Server");
    m_error_reason = con->get_ec().message();
}

/**
* @brief Eine Verbindung wird geschlossen
* @param[in] c: Pointer auf Websocketclient
* @param[in] hdl: Pointer auf Verbindung
* @return Kein Rückgabewert
*/
void Metadata::on_close(client * c, websocketpp::connection_hdl hdl)
{
    m_status = "Closed";
    client::connection_ptr con = c->get_con_from_hdl(hdl);
    std::stringstream s;
    s << "close code: " << con->get_remote_close_code() << " ("
      << websocketpp::close::status::get_string(con->get_remote_close_code())
      << "), close reason: " << con->get_remote_close_reason();
    m_error_reason = s.str();
}

/**
* @brief Eingehende Nachricht
* @param[in] hdl: Pointer auf Verbindung
* @param[in] msg: Pointer auf Nachricht
* @return Kein Rückgabewert
*/
void Metadata::on_message(websocketpp::connection_hdl, client::message_ptr msg)
{
    if (msg->get_opcode() == websocketpp::frame::opcode::text)
    {
        m_messages.push_back("<< " + msg->get_payload());
    }
    else
    {
        m_messages.push_back("<< " + websocketpp::utility::to_hex(msg->get_payload()));
    }
}

/**
* @brief Gibt den Verbindungspointer zurück
* @return Verbindungspointer
*/
websocketpp::connection_hdl Metadata::get_hdl() const
{
    return m_hdl;
}

/**
* @brief Gibt die Verbindungsid zurück
* @return Verbindungsid
*/
int Metadata::get_id() const
{
    return m_id;
}

/**
* @brief Gibt den Verbindungsstatus zurück
* @return Verbindungsstatus
*/
std::string Metadata::get_status() const
{
    return m_status;
}

/**
* @brief Speichert die gesendete Nachricht
* @param[in] message: eine Nachricht
* @return Kein Rückgabewert
*/
void Metadata::record_sent_message(std::string message)
{
    m_messages.push_back(">> " + message);
}



