/**
 * @fileOverview Webserver
 * @author PACtec
 * @version 1.0.0
 */

/**
	* Anlegen aller benötigten Variablen
	*/
var express = require('express');
var app = express();
var http = require('http');
var helmet = require('helmet');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var WebSocketServer = require('ws');
var path = require("path");
var server = http.createServer(app);
var io = require('socket.io')(server);
var async = require('async');
var functions = require('./modules/functions/index.js');
var mysocket = require('./modules/mysocket/index.js');
var session;
var user;

/**
	* Grundlegende Einstellungen des Servers
	*/
app.set('view engine', 'ejs');
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/node_modules'));
app.use(express.static(path.join(__dirname, '../Paketbox/02_Sound/')));
console.log(path.join(__dirname, '../Paketbox/02_Sound/'));
/**
	* Ruft die Login-Seite auf
	*/
app.get('/', function(req,res)
{
	res.render('Login.ejs');
});

/**
	* Ruft die Übersicht-Seite auf
	*/
app.get('/Uebersicht.ejs', function(req,res)
{
	var packets = [];
	async.series([
		function(callback)
    {
			functions.loggedIn(callback, session, res);
		},
		function(callback)
    {
			functions.actPacketlist(callback, packets);
		},
		function(callback)
    {
			res.render('Uebersicht.ejs',
      {
				packets: packets
			});
      callback(null);
		}
	]);
});

/**
	* Ruft die Bedienung-Seite auf
	*/
app.get('/Bedienung.ejs', function(req,res)
{
	var syslog = [];
	async.series([
		function(callback)
    {
			functions.loggedIn(callback, session, res);
		},
		function(callback)
    {
			functions.systemlog(callback, syslog);
		},
		function(callback)
    {
			res.render('Bedienung.ejs',
      {
				syslog: syslog
			});
      callback(null);
		}
	]);
});

/**
	* Ruft die Paket-Seite auf
	*/
app.get('/Pakete.ejs', function(req,res)
{
	var packets = [];
	async.series([
		function(callback)
    {
			functions.loggedIn(callback, session, res);
		},
		function(callback)
    {
			functions.packetlist(callback, packets);
		},
		function(callback)
    {
			res.render('Pakete.ejs',
      {
				packets: packets
			});
		}
	]);
});

/**
	* Ruft die Einstellungen-Seite auf
	*/
app.get('/Einstellungen.ejs', function(req,res)
{
	var mails = [];
	async.series([
		function(callback)
    {
			functions.loggedIn(callback, session, res);
		},
		function(callback)
    {
			functions.mailreceiver(callback, mails);
		},
		function(callback)
    {
			res.render('Einstellungen.ejs',
      {
				mails: mails
			});
		}
	]);
});

/**
	* Ruft die Paket-Hinzufügen-Seite auf
	*/
app.get('/PaketHinzufuegen.ejs', function(req,res)
{
	async.series([
		function(callback)
    {
			functions.loggedIn(callback, session, res);
		},
		function(callback)
    {
			res.render('PaketHinzufuegen.ejs');
		}
	]);
});

/**
	* Ruft die Email-Ändern-Seite auf
	*/
app.get('/EmailAendern.ejs', function(req,res)
{
	async.series([
		function(callback)
    {
			functions.loggedIn(callback, session, res);
		},
		function(callback)
    {
			res.render('EmailAendern.ejs');
		}
	]);
});

/**
	* Ruft die Passwort-Ändern-Seite auf
	*/
app.get('/PasswortAendern.ejs', function(req,res)
{
	async.series([
		function(callback)
    {
			functions.loggedIn(callback, session, res);
		},
		function(callback)
    {
			res.render('PasswortAendern.ejs');
		}
	]);
});

/**
	* Ruft die Nutzungszeit-Ändern-Seite auf
	*/
app.get('/NutzungszeitAendern.ejs', function(req,res)
{
	async.series([
		function(callback)
    {
			functions.loggedIn(callback, session, res);
		},
		function(callback)
    {
			res.render('NutzungszeitAendern.ejs');
		}
	]);
});

/**
	* Ruft die Logout-Seite auf
	*/
app.get('/Logout.ejs', function(req,res)
{
	async.series([
		function(callback)
    {
			functions.logout(callback);
		},
		function(callback)
    {
      res.render('Logout.ejs');
    	callback(null);
		}
	]);
});

/**
	* Führt die Login-Routine durch
	*/
app.post('/login' , function(req, res)
{
	user = req.body.userInput;
	var password = req.body.passwordInput;
	var dbuser = [];
	var dbpassword = [];
	var packets = [];

	async.series([
		function(callback)
    {
			functions.actPacketlist(callback, packets);
		},
		function(callback)
    {
			functions.userquery(callback, dbuser);
		},
		function(callback)
    {
			functions.passwordquery(callback, dbpassword);
		},
		function(callback)
    {
			functions.forwarding(callback, password, dbpassword, user, dbuser, packets, res, session);
			session = 1;
		}
	]);
});

/**
	* Führt die Paket-Hinzufügen-Routine durch
	*/
app.post('/addPacket', function(req,res)
{
	var packets = [];
	var barcode = req.body.barcode;
	var supplier = req.body.supplier;
	var content = req.body.content;
	var date = req.body.date;
	var receiverFirstname = req.body.firstname;
	var receiverLastname = req.body.lastname;
	var receiverEmail = req.body.email;
	async.series([
		function(callback)
    {
			functions.addPacket(callback, barcode, supplier, content, date, receiverFirstname, receiverLastname, receiverEmail);
		},
		function(callback)
    {
			functions.packetlist(callback, packets);
		},
		function(callback)
    {
			functions.renderPacket(callback, packets, res);
		}
	]);
});

/**
	* Führt die Paket-Löschen-Routine durch
	*/
app.post('/deletePacket', function(req, res)
{
	var packet = req.body.deletePacket;
	var packets = [];
	async.series([
		function(callback)
    {
			functions.deletePacket(callback, packet);
		},
		function(callback)
    {
			functions.packetlist(callback, packets);
		},
		function(callback)
    {
			functions.renderPacket(callback, packets, res);
		}
	]);
});

/**
	* Führt die Neustart-Routine durch
	*/
app.post('/programmRestart', function(req, res)
{
	var syslog = [];
	async.series([
		function(callback)
    {
			functions.systemlog(callback, syslog);
		},
		function(callback)
    {
			functions.restart(callback);
		},
		function(callback)
    {
			res.render('Bedienung.ejs',
      {
				syslog: syslog
			});
      callback(null);
		}
	]);
});

/**
	* Führt die Herunterfahren-Routine durch
	*/
app.post('/programmShutdown', function(req, res)
{
	var syslog = [];
	async.series([
		function(callback)
    {
			functions.systemlog(callback, syslog);
		},
		function(callback)
    {
			functions.shutdown(callback);
		},
		function(callback)
    {
			res.render('Bedienung.ejs',
      {
				syslog: syslog
			});
      callback(null);
		}
	]);
});

/**
	* Führt die Passwort-Ändern-Routine durch
	*/
app.post('/changePassword',function(req, res)
{
		var password1 = req.body.password1;
		var password2 = req.body.password2;
		var mails = [];
		var hash = new String(user+password1).hashCode();
		async.series([
			function(callback)
      {
				functions.mailreceiver(callback, mails);
			},
			function(callback)
      {
				functions.passwordchange(password1,password2,hash,user,mails,res);
			}
	]);
});

/**
	* Führt die Email-Ändern-Routine durch
	*/
app.post('/changeEmail',function(req, res)
{
		var email = req.body.email;
		var mails = [];
		async.series([
			function(callback)
      {
				functions.mailreceiver(callback, mails);
			},
			function(callback)
      {
				functions.mailchange(email,user,mails,res);
			}
		]);
});

/**
	* Führt die Mail-Empfänger-Hinzufügen-Routine durch
	*/
app.post('/hinzufuegen',function(req, res)
{
		var email = req.body.empfhinzufuegen;
		var number = [];
		var mails = [];
		async.series([
			function(callback)
      {
				functions.mailreceiver(callback, mails);
			},
			function(callback)
      {
				functions.dbnumber(callback, number);
			},
			function(callback)
      {
				functions.addReceiver(callback, number, mails, email, res);
			}
		]);
});

/**
	* Führt die Mail-Empfänger-Löschen-Routine durch
	*/
app.post('/loeschen',function(req, res)
{
		var email = req.body.empfloeschen;
		var mails = [];
		async.series([
			function(callback)
      {
				functions.mailreceiver(callback, mails);
			},
			function(callback)
      {
				functions.deleteReceiver(callback, mails, email, res);
			}
		]);
});

/**
	* Führt die Nutzungszeit-Ändern-Routine durch
	*/
app.post('/usingtime',function(req, res)
{
	var monday = req.body.monday;
	var tuesday = req.body.tuesday;
	var wednesday = req.body.wednesday;
	var thursday = req.body.thursday;
	var friday = req.body.friday;
	var saturday = req.body.saturday;
	var sunday = req.body.sunday;
	var duration = req.body.duration;
	var start = req.body.start;
	var end = req.body.end;
	var mails = [];
	async.series([
		function(callback)
    {
			functions.mailreceiver(callback, mails);
		},
		function(callback)
    {
			res.render('Einstellungen.ejs',
      {
				mails:mails
			});
			callback(null);
		},
		function(callback)
    {
	var monday = req.body.monday;
	var tuesday = req.body.tuesday;
	var wednesday = req.body.wednesday;
	var thursday = req.body.thursday;
	var friday = req.body.friday;
	var saturday = req.body.saturday;
	var sunday = req.body.sunday;
	var duration = req.body.duration;
	var start = req.body.start;
	var end = req.body.end;
	var mails = [];
			functions.timechange(monday,tuesday,wednesday,thursday,friday,saturday,sunday,duration,start,end);
		}
	]);
});
server.listen(3000);
console.log('Webserver listening on port 3000!');

/**
  * Websocket-Server C++ <> Webserver
  */
const wss = new WebSocketServer.Server({ port: 8080 });
console.log('Websocketserver listening on port 8080...');

mysocket.socket(wss, io);


