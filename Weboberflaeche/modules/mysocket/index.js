
/**
	* Websocket-Routine Server <> Weboberfläche
	*/
exports.socket = function(wss,io)
{
    wss.on('connection', function(ws)
    {
      io.on('connection', function (socket)
      {
        socket.setMaxListeners(15);
          ws.on('message', function(data, flags)
          {
          if (flags.binary)
          {
            return;
          }
      		if (data == '0001_0')
          {
            socket.emit('door', '0');
          }
      		if (data == '0001_1')
          {
            socket.emit('door', '1');
          }
      		if (data == '0010_0')
          {
            socket.emit('led', '0');
          }
      		if (data == '0010_1')
          {
            socket.emit('led', '1');
          }
      		if (data == '0011_0')
          {
            socket.emit('scanner', '0');
          }
      		if (data == '0011_1')
          {
            socket.emit('scanner', '1');
          }
      		if (data == '0100_0')
          {
            socket.emit('fehler', '0');
          }
      		if (data == '0100_1')
          {
            socket.emit('fehler', '1');
          }
         	if (data == '0101_0')
          {
            socket.emit('activeState', '0');
          }
          if (data == '0101_1')
          {
            socket.emit('activeState', '1');
          }
          });
          ws.on('close', function()
          {
          });
          ws.on('error', function(err)
          {
          });
      		socket.on('fehltaster', function(msg, err)
          {
      			var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
      			if (msg == '1')
            {
      				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'HandleErrorLedByWebsite'")
              setTimeout(function ()
              {
                db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'HandleErrorLedByWebsite'", function(err)
                {
                  db.close();
                });
              db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'HandleErrorLedByWebsite'")
              }, 5000)
      			}
            else
            {
      				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'HandleErrorLedByWebsite'", function(err)
              {
                db.close();
              });
      			}
      		});
      		socket.on('akttaster', function(msg)
          {
      			var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
      			if (msg == '1')
            {
      				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'HandleDoorByWebsite'")
              setTimeout(function ()
              {
                db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'HandleDoorByWebsite'", function(err)
                {
                  db.close();
                });
              }, 5000)
      			}
            else
            {
      				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'HandleDoorByWebsite'", function(err)
              {
                db.close();
              });
      			}
      		});
          socket.on('disconnect', function ()
          {
          });
      });
    });
}
