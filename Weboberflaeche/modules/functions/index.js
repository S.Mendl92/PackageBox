var async = require('async');
var sqlite3 = require('sqlite3').verbose();

/**
  * Hash-Funktion für Benutzername + Passwort
  * @return {number} Hash-Rückgabe
  */
String.prototype.hashCode = function()
{
    if (Array.prototype.reduce)
    {
        return this.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);
    }
    var hash = 0;
    if (this.length === 0) return hash;
    for (var i = 0; i < this.length; i++)
    {
        var character  = this.charCodeAt(i);
        hash  = ((hash<<5)-hash)+character;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

/**
	* Erstellt die Übersicht über die letzten 5 Lieferungen
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {object[]} packets Liste von Paketen
	*/
exports.actPacketlist = function(callback, packets)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/PaketDB.db');
			db.all("SELECT * FROM Pakete LIMIT 5" , function(err, rows)
      {
        	rows.forEach(function (row)
          {
							var singleObj = {};
							singleObj['number'] = row.Barcode;
							singleObj['supplier'] = row.Lieferant;
							singleObj['content'] = row.Inhalt;
							singleObj['status'] = row.Lieferstatus;
							packets.push(singleObj);
					});
					callback(null, packets);
			});
}

/**
	* Erstellt die komplette Paketliste
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {object[]} packets Liste von Paketen
	*/
exports.packetlist = function(callback, packets)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/PaketDB.db');
			db.all("SELECT * FROM Pakete p, Empfaenger e WHERE p.Barcode = e.Barcode" , function(err, rows)
      {
        	rows.forEach(function (row)
          {
							var singleObj = {};
							singleObj['number'] = row.Barcode;
							singleObj['supplier'] = row.Lieferant;
							singleObj['content'] = row.Inhalt;
							singleObj['firstname'] = row.Vorname;
							singleObj['lastname'] = row.Nachname;
							singleObj['status'] = row.Lieferstatus;
							packets.push(singleObj);
					});
					callback(null, packets);
			});
}

/**
	* Fragt die vorhandenen Benutzer von der Datenbank ab
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {string[]} dbuser Feld der gefundenen User
	*/
exports.userquery = function(callback, dbuser)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/User_Management.db');
	db.all("SELECT * FROM Users" , function(err, rows)
  {
		rows.forEach(function (row)
    {
			dbuser.push(row.Username);
		});
		callback(null, dbuser);
	});
}

/**
	* Fragt das Passwort aus der Datenbank ab
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {string[]} dbpassword Feld des gefundenen Passworts
	*/
exports.passwordquery = function(callback, dbpassword)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/User_Management.db');
	db.all("SELECT * FROM Users" , function(err, rows)
  {
		rows.forEach(function (row)
    {
			dbpassword.push(row.Password);
		});
		callback(null, dbpassword);
	});
}

/**
	* Erstellt die Systemlog Liste
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {object[]} syslog Liste von Daten
	*/
exports.systemlog = function(callback, syslog)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/Systemlog.db');
			db.all("SELECT * FROM Systemlog ORDER BY ID DESC LIMIT 10" , function(err, rows)
      {
        	rows.forEach(function (row)
          {
							var singleObj = {};
							singleObj['timestamp'] = row.Wochentag+','+row.Monatstag+'.'+row.Monat+'.'+row.Jahr+' '+row.Uhrzeit;
							singleObj['class'] = row.Fehlerklasse;
							singleObj['comment'] = row.Kommentar;
							syslog.push(singleObj);
					});
					callback(null, syslog);
			});
}

/**
	* Fügt ein neues Paket der DB hinzu
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {string} barcode Eingegebener Barcode
	* @param {string} supplier Eingegebener Lieferant
	* @param {string} content Eingegebener Inhalt
	*/
exports.addPacket = function(callback, barcode, supplier, content, date, receiverFirstname, receiverLastname, receiverEmail)
{
		var db = new sqlite3.Database('../Paketbox/00_Databases/PaketDB.db');
    db.serialize(function()
    {
  		var stmt = db.prepare("INSERT OR IGNORE INTO Empfaenger VALUES (?,?,?,?)");
      stmt.run(receiverFirstname,receiverLastname,receiverEmail,barcode);
  		stmt.finalize();
      stmt = db.prepare("INSERT OR IGNORE INTO Pakete VALUES (?,?,?,?)");
      stmt.run(supplier,barcode,content,date);
      stmt.finalize();
      callback(null);
    });
}

/**
	* Löscht ein Paket aus der DB
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {string} packet Eingegebenes Paket
	*/
exports.deletePacket = function(callback, packet)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/PaketDB.db');
  db.serialize(function()
  {
		var stmt = db.prepare("DELETE FROM Empfaenger WHERE Barcode = (?)");
    stmt.run(packet);
    stmt.finalize();
    stmt = db.prepare("DELETE FROM Pakete WHERE Barcode = (?)");
    stmt.run(packet);
    stmt.finalize();
    callback(null);
  });
}

/**
	* Fügt den neuen Mail-Empfänger der DB hinzu
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {int[]} number Feld von Datensatzpositionen
	* @param {object[]} mails Liste von Mail-Empfängern
	* @param {string} email Eingabe des neuen Mail-Empfängers
	* @param {} res Antwort des Servers
	*/
exports.addReceiver = function(callback, number, mails, email, res)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
  db.serialize(function()
  {
	   var stmt = db.prepare("UPDATE Settings SET DatasetValue = (?) WHERE DatasetNumber = '"+number[0]+"'");
     stmt.run(email);
     stmt.finalize();
  });
	res.render('Einstellungen.ejs',
  {
		mails: mails
	});
	callback(null);
}

/**
	* Löscht den gewählten Mail-Empfänger aus der DB
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {object[]} mails Liste von Mail-Empfängern
	* @param {string} email Eingabe des zu löschenden Mail-Empfängers
	* @param {} res Antwort des Servers
	*/
exports.deleteReceiver = function(callback, mails, email, res)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
  db.serialize(function()
  {
	   var stmt = db.prepare("UPDATE Settings SET DatasetValue = '' WHERE DatasetValue = (?)");
     stmt.run(email);
     stmt.finalize();
  });
  res.render('Einstellungen.ejs',
  {
		mails: mails
	});
	callback(null);
}

/**
	* Ändert den Handler für den Restart
	* @param {} callback Gibt das Ende der Funktion zurück
	*/
exports.restart = function(callback)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
	db.run("UPDATE Settings SET DatasetValue = '2' WHERE DatasetNumber = '3'", function(err)
  {
		callback(null);
	});
}

/**
	* Ändert den Handler für den Shutdown
	* @param {} callback Gibt das Ende der Funktion zurück
	*/
exports.shutdown = function(callback)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
	db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetNumber = '3'", function(err)
  {
		callback(null);
	});
}

/**
	* Erstellt die Liste der Mail-Empfänger
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {object[]} mails Liste von Mail-Empfängern
	*/
exports.mailreceiver = function(callback, mails)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
	db.all("SELECT * FROM Settings WHERE DatasetValue != '' AND DatasetComment = 'Mailreceiver'" , function(err, rows)
  {
		rows.forEach(function (row)
    {
			var singleObj = {};
			singleObj['address'] = row.DatasetValue;
			mails.push(singleObj);
	});
	callback(null, mails);
	});
}

/**
	* Überprüft die Login Eingaben
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {string} password Eingegebenes Passwort
	* @param {string[]} dbpassword Feld des gefundenen Passworts
	* @param {string} user Eingegebener Benutzer
	* @param {string[]} dbuser Feld der gefundenen User
	* @param {object[]} packets Liste von Paketen
	* @param {} res Antwort des Servers
	*/
exports.forwarding = function(callback, password, dbpassword, user, dbuser, packets, res, session)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
	var hash = new String(user+password).hashCode();
	if(dbpassword == hash)
  {
		session = 1;
		db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'UserLoginState'");
		res.render('Uebersicht.ejs', {
			packets: packets
		});
	}
  else
  {
		db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'UserLoginState'");
		res.render('Login.ejs');
	}
	callback(null);
}

/**
	* Ermittelt die freien Reihen in der DB für die Mail-Empfänger
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {int[]} number Feld von Datensatzpositionen
	*/
exports.dbnumber = function(callback, number)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
	var help;
	db.all("SELECT DatasetNumber FROM Settings WHERE DatasetComment = 'Mailreceiver' AND DatasetValue = ''", function(err, rows)
  {
		rows.forEach(function(row)
    {
			help = row.DatasetNumber;
			number.push(help);
		});
		callback(null, number);
	});
}

/**
	* Ruft die Paket-Seite auf
	* @param {} callback Gibt das Ende der Funktion zurück
	* @param {object[]} packets Liste von Paketen
	* @param {} res Antwort des Servers
	*/
exports.renderPacket = function(callback, packets, res)
{
	res.render('Pakete.ejs',
  {
		packets: packets
	});
	callback(null);
}

/**
	* Logt den Benutzer aus
	* @param {} callback Gibt das Ende der Funktion zurück
	*/
exports.logout = function(callback)
{
	var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
	db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'UserLoginState'", function(err)
  {
		callback(null);
	});
	session = 0;
}

/**
  * Überprüft, ob der Benutzer eingeloggt ist
  * @param {} callback Gibt das Ende der Funktion zurück
  * @param {int} session Gibt den aktuellen Login-Status an
  */
exports.loggedIn = function(callback,session,res)
{
  if(session == '1')
  {
    callback(null);
  }
  else
  {
    res.render('Login.ejs');
  }
}

exports.timechange = function(monday,tuesday,wednesday,thursday,friday,saturday,sunday,duration,start,end)
{
var db = new sqlite3.Database('../Paketbox/00_Databases/Settings.db');
			if(monday == 'true')
      {
				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'StateMonday'");
			}
      else
      {
				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'StateMonday'");
			}
			if(tuesday == 'true')
      {
				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'StateTuesday'");
			}
      else
      {
				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'StateTuesday'");
			}
			if(wednesday == 'true')
      {
				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'StateWednesday'");
			}
      else
      {
				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'StateWednesday'");
			}
			if(thursday == 'true')
      {
				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'StateThursday'");
			}
      else
      {
				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'StateThursday'");
			}
			if(friday == 'true')
      {
				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'StateFriday'");
			}
      else
      {
				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'StateFriday'");
			}
			if(saturday == 'true')
      {
				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'StateSaturday'");
			}
      else
      {
				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'StateSaturday'");
			}
			if(sunday == 'true')
      {
				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = 'StateSunday'");
			}
      else
      {
				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = 'StateSunday'");
			}
			if(duration == 'true')
      {
				db.run("UPDATE Settings SET DatasetValue = '1' WHERE DatasetComment = '24-Hours'");
			}
      else
      {
				db.run("UPDATE Settings SET DatasetValue = '0' WHERE DatasetComment = '24-Hours'");
			}
      db.serialize(function()
      {
			var stmt = db.prepare("UPDATE Settings SET DatasetValue = (?) WHERE DatasetComment = 'StartHour'");
      stmt.run(start);
      stmt.finalize();
			stmt = db.prepare("UPDATE Settings SET DatasetValue = (?) WHERE DatasetComment = 'EndHour'");
      stmt.run(end);
      stmt.finalize();
      });
}
