var socket = io();

$(document).ready(function(){
	var datum = new Date();
	var heute = datum.getDate()+"."+ (datum.getMonth()+ 1)+"." + datum.getFullYear();
	$('#datum').text(heute);
});

socket.on('activeState', function(msg){
	if(msg == '1'){
		$('#activeState').attr("class","label label-success");
		$('#activeState').text('Paketbox bereit');
	}else{
		$('#activeState').attr("class","label label-danger");
		$('#activeState').text('Paketbox nicht bereit');
		}
});
