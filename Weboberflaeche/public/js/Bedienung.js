
	$(document).ready(function(){
		var datum = new Date();
		var heute = datum.getDate()+"."+ (datum.getMonth()+ 1)+"." + datum.getFullYear();
		$('#datum').text(heute);

		$('#table_sort').DataTable();

		var socket = io();

			socket.on('door', function(msg){
				window.console.log("Door: "+msg)
				if(msg == '1'){
					$('#door').attr("class","label label-success");
				}else{
					$('#door').attr("class","label label-danger");
		    	}
			});
			socket.on('led', function(msg){
				if(msg == '1'){
					$('#led').attr("class","label label-success");
				}else{
					$('#led').attr("class","label label-danger");
		    	}
			});

			socket.on('scanner', function(msg){
				if(msg == '1'){
					$('#akttaster').attr("class","btn btn-block btn-primary btn-lg btn-success");
				}else{
					$('#akttaster').attr("class","btn btn-block btn-primary btn-lg btn-danger");
					}
			});

			socket.on('fehler', function(msg){
				if(msg == '1'){
					$('#fehltaster').attr("class","btn btn-block btn-primary btn-lg btn-success");
				}else{
					$('#fehltaster').attr("class","btn btn-block btn-primary btn-lg btn-danger");
					}
			});

			socket.on('scanner', function(msg){
				if(msg == '1'){
					$('#fehltaster').attr("class","btn btn-block btn-primary btn-lg btn-success");
				}else{
					$('#fehltaster').attr("class","btn btn-block btn-primary btn-lg btn-danger");
					}
			});

			$('#fehltaster').click(function(){
				socket.emit('fehltaster', $('#fehltaster').val());
			});

			$('#akttaster').click(function(){
				socket.emit('akttaster', $('#akttaster').val());

			});

	});
