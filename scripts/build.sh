#!/bin/bash
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/Database.cpp -o ~/Packagebox/obj/Database.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/Converter.cpp -o ~/Packagebox/obj/Converter.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/DatabaseWrapper.cpp -o  ~/Packagebox/obj//DatabaseWrapper.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/GPIOControl.cpp -o  ~/Packagebox/obj/GPIOControl.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/MailClient.cpp -o  ~/Packagebox/obj/MailClient.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/MainHandler.cpp -o  ~/Packagebox/obj/MainHandler.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/Metadata.cpp -o  ~/Packagebox/obj/Metadata.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/MyNodeJsHandler.cpp -o  ~/Packagebox/obj/MyNodeJsHandler.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/MyWebsocketProtocol.cpp -o  ~/Packagebox/obj/MyWebsocketProtocol.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/ReceiveTime.cpp -o  ~/Packagebox/obj/ReceiveTime.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/Systemlog.cpp -o  ~/Packagebox/obj/Systemlog.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/UserManagement.cpp -o ~/Packagebox/obj/UserManagement.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/UserManagement.cpp -o  ~/Packagebox/obj/UserManagement.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/WebsocketClient.cpp -o  ~/Packagebox/obj/WebsocketClient.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/ZbarCam.cpp -o  ~/Packagebox/obj/ZbarCam.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/main.cpp -o  ~/Packagebox/obj/main.o;
#
g++ -O3 -DNDEBUG -pg -g -std=c++11 -std=gnu++14 -pthread -I/usr/include/uWebSockets/src -I/usr/include/boost/ -c ~/Packagebox/Libary/TextToSpeech.cpp -o  ~/Packagebox/obj/TextToSpeech.o;
#
g++  -o ~/Packagebox/Weboberflaeche/bin/Release/SoftwarePackagebox  ~/Packagebox/obj/Converter.o  ~/Packagebox/obj/Database.o  ~/Packagebox/obj/DatabaseWrapper.o  ~/Packagebox/obj/GPIOControl.o  ~/Packagebox/obj/MailClient.o  ~/Packagebox/obj/MainHandler.o  ~/Packagebox/obj/Metadata.o  ~/Packagebox/obj/MyNodeJsHandler.o  ~/Packagebox/obj/MyWebsocketProtocol.o  ~/Packagebox/obj/ReceiveTime.o  ~/Packagebox/obj/Systemlog.o  ~/Packagebox/obj/TextToSpeech.o  ~/Packagebox/obj/UserManagement.o  ~/Packagebox/obj/WebsocketClient.o ~/Packagebox/obj/ZbarCam.o ~/Packagebox/obj/main.o  -s -pg -fexceptions -pthread -lsqlite3 -I/usr/include/boost/ -lboost_system;

